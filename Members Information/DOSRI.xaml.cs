﻿using Members_Information.Models.DOSRI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Members_Information
{
    /// <summary>
    /// Interaction logic for DOSRI.xaml
    /// </summary>
    public partial class DOSRI : Window
    {
        MainWindow mww;
        private ICollectionView MyData;
        String SearchText = string.Empty;
        String DefaultColumn = "";
        int currentRow = 0, currentColumn = 1;

        public DOSRI(MainWindow mw, List<Models.DosRi> dosRis, Int16 dosRiID = 0)
        {
            InitializeComponent();
            this.mww = mw;
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            DG_DOSRI.ItemsSource = dosRis;

            if (dosRis.Count != 0)
            {
                DG_DOSRI.SelectedItem = dosRis.FirstOrDefault();
            }
            MyData = CollectionViewSource.GetDefaultView(dosRis);
            searchTextbox.Focus();
        }

        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;
            if (cell.Column.DisplayIndex != this.currentColumn)
            {
                cell.IsSelected = false;
            }


            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }

        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Up)
                {
                    if (currentRow > 0)
                    {
                        try
                        {
                            int previousIndex = DG_DOSRI.SelectedIndex - 1;
                            if (previousIndex < 0) return;
                            DG_DOSRI.SelectedIndex = previousIndex;
                            DG_DOSRI.ScrollIntoView(DG_DOSRI.Items[currentRow]);

                        }
                        catch (Exception ex)
                        {
                            e.Handled = true;
                        }
                    }

                }
                else if (e.Key == Key.Down)
                {
                    if (currentRow < DG_DOSRI.Items.Count - 1)
                    {
                        try
                        {
                            int nextIndex = DG_DOSRI.SelectedIndex + 1;
                            if (nextIndex > DG_DOSRI.Items.Count - 1) return;
                            DG_DOSRI.SelectedIndex = nextIndex;
                            DG_DOSRI.ScrollIntoView(DG_DOSRI.Items[currentRow]);
                        }
                        catch (Exception ex)
                        {
                            e.Handled = true;
                        }

                    } // end if (this.SelectedOverride > 0)

                } // end else if (e.Key == Key.Down)

                else if (e.Key == Key.Escape)
                {
                    this.Close();
                    e.Handled = true;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void DG_DOSRI_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    SelectDOSRI();
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void DG_DOSRI_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                SelectDOSRI();
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Selected Item
        /// </summary>
        private void SelectDOSRI()
        {
            if (DG_DOSRI.SelectedItem == null) return;
            var selectedDOSRI = DG_DOSRI.SelectedItem as Models.DosRi;

            if (this.mww != null)
            {
                InsertDOSRI selected = (InsertDOSRI)this.mww.DG_DOSRI.SelectedItem;
                selected.DosriID = selectedDOSRI.DosRiID;
                selected.DosriDesc = selectedDOSRI.DosRiDesc;

                this.Close();
                Keyboard.Focus(this.mww.GetDataGridCell(this.mww.DG_DOSRI.SelectedCells[1]));
            }
        }

        private void searchTextbox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (DG_DOSRI.Items != null)
                    {
                        SelectDOSRI();
                        e.Handled = true;
                    }
                    
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SelectDOSRI();
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }
    }
}

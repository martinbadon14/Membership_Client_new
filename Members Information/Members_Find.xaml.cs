﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Web.Script.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Members_Information.Models;
using System.Text.RegularExpressions;

namespace Members_Information
{
    /// <summary>
    /// Interaction logic for Members_Find.xaml
    /// </summary>
    public partial class Members_Find : Window
    {
        Members_List newsub;
        public MainWindow mw;
        public Members_New mws;
        public int CHOICE;

        public Members_Find(int choice, MainWindow mww)
        {
            InitializeComponent();
            this.mw = mww;
            CHOICE = choice;
            Options(choice);
            string clientID = txt_batchnum.Text;

            //txt_venue.Text = this.mw.searchName;

            txt_venue.Text = App.CRUXAPI.getUserParameter("ClientName");
            //else if (!String.IsNullOrWhiteSpace(txt_venue.Text))
            //{
            //	txt_batchnum.Focus();
            //}

            InitializeDefaults();

            if (!String.IsNullOrWhiteSpace(txt_venue.Text))
            {
                txt_venue.Focus();
                txt_venue.SelectAll();
            }
            else
            {
                txt_batchnum.Focus();
            }
            if (!String.IsNullOrWhiteSpace(this.mw.checkAll))
            {
                checkBox_showAll.IsChecked = true;
            }
            else if (!String.IsNullOrWhiteSpace(this.mw.checkClose))
            {
                checkBox_showClosed.IsChecked = true;
            }

        }

        /// <summary>
        /// Default Values
        /// </summary>
        private void InitializeDefaults()
        {
            CRUXLib.Response Response;

            Response = App.CRUXAPI.request("client/allbranches", "");
            if (Response.Status == "SUCCESS")
            {
                List<Branches> listSuffix = new JavaScriptSerializer().Deserialize<List<Branches>>(Response.Content);
                cmb_branchID.ItemsSource = listSuffix;
            }
            else
            {
                MessageBox.Show(Response.Content);
            }

            Response = App.CRUXAPI.request("CurrentBranch/loanapplication", "");
            if (Response.Status == "SUCCESS")
            {
                CurrentBranch BranchID = new JavaScriptSerializer().Deserialize<CurrentBranch>(Response.Content);
                cmb_branchID.DataContext = BranchID;
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        private void SetBranch()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("CurrentBranch/loanapplication", "");
            if (Response.Status == "SUCCESS")
            {
                CurrentBranch BranchID = new JavaScriptSerializer().Deserialize<CurrentBranch>(Response.Content);
                cmb_branchID.DataContext = BranchID;
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        private void Options(int choice)
        {
            switch (choice)
            {
                case 1:
                case 11:
                    CompanyOption();
                    break;
                case 2:
                case 21:
                case 22:
                case 23:
                    MemberOption();
                    break;
                    //case 3:
                    //	KCASMemberOption();
                    //	break;
            }
        }

        private void CompanyOption()
        {
            Title = "Find Company";

            labelID.Content = "Company ID";
            labelDescription.Content = "Company Name:";

        }

        private void MemberOption()
        {
            Title = "Find Member";
            labelID.Content = "Client ID";

            labelDescription.Content = "Client Name:";
        }

        private void KCASMemberOption()
        {
            Title = "Find KCAS Member";
            labelID.Content = "Client ID";

            labelDescription.Content = "Client Name:";
        }

        private void btn_ok_Click(object sender, RoutedEventArgs e)
        {
            if (txt_batchnum.Text != "" || txt_venue.Text != "")
            {

                if (checkBox_showAll.IsChecked.Value == true)
                {
                    MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("WARNING!! You are accessing ALL Accounts, CLOSED Accounts will be shown!", "Posting Confirmation", System.Windows.MessageBoxButton.YesNo); if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        newsub = new Members_List(CHOICE, this.mw, mws, this);
                        newsub.Owner = this.mw;
                        this.Close();
                        newsub.ShowDialog();
                        this.mw.checkAll = "Yes";
                    }
                }
                else if (checkBox_showClosed.IsChecked.Value == true)
                {
                    MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("WARNING!! You are accessing CLOSED Accounts Only!", "Posting Confirmation", System.Windows.MessageBoxButton.YesNo); if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        newsub = new Members_List(CHOICE, this.mw, mws, this);
                        newsub.Owner = this.mw;
                        this.Close();
                        newsub.ShowDialog();
                        this.mw.checkClose = "Yes";
                    }

                }
                else
                {
                    newsub = new Members_List(CHOICE, this.mw, mws, this);
                    newsub.Owner = this.mw;
                    this.Close();
                    newsub.ShowDialog();
                    this.mw.checkClose = "";
                    this.mw.checkAll = "";
                }
            }
            else
            {
                MessageBox.Show("Please input some character", "Warning");
            }
            //this.mw.searchName = txt_venue.Text;

        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
          //  this.mw.searchName = txt_venue.Text;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txt_batchnum.Text != "" || txt_venue.Text != "")
                {
                    if (checkBox_showAll.IsChecked.Value == true)
                    {
                        MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("WARNING!! You are accessing ALL Accounts, CLOSED Accounts will be shown!", "Posting Confirmation", System.Windows.MessageBoxButton.YesNo); if (messageBoxResult == MessageBoxResult.Yes)
                        {
                            newsub = new Members_List(CHOICE, this.mw, mws, this);
                            newsub.Owner = this.mw;
                            this.Close();
                            newsub.ShowDialog();
                            this.mw.checkAll = "Yes";

                        }
                    }
                    else if (checkBox_showClosed.IsChecked.Value == true)
                    {
                        MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("WARNING!! You are accessing CLOSED Accounts Only!", "Posting Confirmation", System.Windows.MessageBoxButton.YesNo); if (messageBoxResult == MessageBoxResult.Yes)
                        {
                            newsub = new Members_List(CHOICE, this.mw, mws, this);
                            newsub.Owner = this.mw;
                            this.Close();
                            newsub.ShowDialog();
                            this.mw.checkClose = "Yes";
                        }

                    }
                    else
                    {

                        newsub = new Members_List(CHOICE, this.mw, mws, this);
                        newsub.Owner = this.mw;
                        this.Close();
                        newsub.ShowDialog();

                        this.mw.checkClose = "";
                        this.mw.checkAll = "";
                    }

                }
                else
                {
                    MessageBox.Show("Please input some character", "Warning");
                }
                //this.mw.searchName = txt_venue.Text;
            }
            if (e.Key == Key.Escape)
            {
                this.Close();
                //this.mw.searchName = txt_venue.Text;
            }

        }

        private void txt_batchnum_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!Regex.IsMatch(e.Text, @"^[0-9]+$"))
            {
                e.Handled = true;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            App.CRUXAPI.setUserParameter("ClientName", txt_venue.Text);
            //this.mw.searchName = txt_venue.Text;
        }

        private void txt_batchnum_GotFocus(object sender, RoutedEventArgs e)
        {
            //this.mw.searchName = "";
            txt_venue.Text = "";
        }

        private void txt_venue_GotFocus(object sender, RoutedEventArgs e)
        {
            txt_batchnum.Text = "";
        }

        private void checkBox_showClosed_Checked(object sender, RoutedEventArgs e)
        {
            this.checkBox_showAll.IsChecked = false;
            this.mw.checkAll = "";
        }

        private void checkBox_showAll_Checked(object sender, RoutedEventArgs e)
        {
            checkBox_showClosed.IsChecked = false;
            this.mw.checkClose = "";
        }
    }
}

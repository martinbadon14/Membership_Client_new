﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Members_Information.Models;
using System.Web.Script.Serialization;

namespace Members_Information
{
    /// <summary>
    /// Interaction logic for ActivityLog.xaml
    /// </summary>
    public partial class ActivityLog : Window
    {
        public ActivityLog(Int64 ClientID, Int64 BranchID)
        {
            InitializeComponent();

            if (ClientID != null && BranchID != null)
            {
                ClientSearch o2 = new ClientSearch();
                o2.ClientID = ClientID;
               
                o2.BatchNum = BranchID;
                String parsed = new JavaScriptSerializer().Serialize(o2);
                CRUXLib.Response Response = App.CRUXAPI.request("client/activityLog", parsed);
                if (Response.Status == "SUCCESS")
                {
                    List<ActivityLogs> clientBeneficiariesList = new JavaScriptSerializer().Deserialize<List<ActivityLogs>>(Response.Content);
                    //    Console.WriteLine(Response.Content);

                    dataGrid.ItemsSource = clientBeneficiariesList;
                    dataGrid.Focus();
                //    // DG_List.Focus();


                    }
                    else
                    {
                        MessageBox.Show(Response.Content);
                    }
                }



        }



    }
}

﻿using Members_Information.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Globalization;
using System.Collections.ObjectModel;

namespace Members_Information
{
	/// <summary>
	/// Interaction logic for Members_Form.xaml
	/// </summary>
	
	public partial class Members_Form : Window
	{
		MainWindow main;
		public Int64 clientID = 0;
		public int optionstatus = 0;

		public Members_Form(int option, Int64 ClientID, Int64 BranchID, MainWindow mw)
		{
			InitializeComponent();
			this.main = mw;
			if (main.BasicInfoGrid.IsEnabled == true)
			{
				//DG_Form.IsReadOnly = false;
				DG_Form.IsEnabled = true;
				bdateofbirth.IsReadOnly = false;

			}
			OptionList(option);
			DG_Form.ItemsSource = null;
			if (option == 1 && ClientID != 0 && BranchID != 0)
			{
				optionstatus = 1;
				this.Title = "ITF List";
				ClientSearch o2 = new ClientSearch();
				if (Convert.ToInt64(main.clientTypeCombo.SelectedValue)==1)
				{
					
					o2.ClientID = ClientID;
				}
				else
				{
					o2.ClientID = Convert.ToInt64( main.principalText.Text);
				}
				o2.ClientID = ClientID;
				clientID = ClientID;
				o2.BatchNum = Convert.ToInt64(main.clientTypeCombo.SelectedValue);
				o2.BranchID = BranchID;
				String parsed = new JavaScriptSerializer().Serialize(o2);
				CRUXLib.Response Response = App.CRUXAPI.request("client/loadITF", parsed);
				//main.clientBeneficiariesList.Clear();
				if (Response.Status == "SUCCESS")
				{
					List<InsertClientBeneficiaries> clientBeneficiariesList = new JavaScriptSerializer().Deserialize<List<InsertClientBeneficiaries>>(Response.Content);
					if (clientBeneficiariesList.Count < main.clientBeneficiariesList.Count)
					{
						DG_Form.ItemsSource = main.clientBeneficiariesList;
					}
					else
					{
						main.loadinsert = new JavaScriptSerializer().Deserialize<ObservableCollection<InsertClientBeneficiaries>>(Response.Content);
						DG_Form.ItemsSource = clientBeneficiariesList;

					}


					// DG_List.Focus();

				}
				else
				{
					MessageBox.Show(Response.Content);
				}
			}
			else if (option == 2 && ClientID != 0 && BranchID != 0)
			{
				optionstatus = 2;
				this.Title = "Member's Beneficiaries";
				ClientSearch o2 = new ClientSearch();
				o2.ClientID = ClientID;
				clientID = ClientID;
				o2.BatchNum = BranchID;
				String parsed = new JavaScriptSerializer().Serialize(o2);
				CRUXLib.Response Response = App.CRUXAPI.request("client/beneficiaries", parsed);
				main.clientBeneficiariesList.Clear();
				if (Response.Status == "SUCCESS")
				{
					List<InsertClientBeneficiaries> clientBeneficiariesList = new JavaScriptSerializer().Deserialize<List<InsertClientBeneficiaries>>(Response.Content);
					
					//if (clientBeneficiariesList.Count < main.clientBeneficiariesList.Count)
					//{
					//	MessageBox.Show("Hahay" + main.clientBeneficiariesList.Count);
					//	DG_Form.ItemsSource = main.clientBeneficiariesList;
					//}
					//else
					//{
					//	if (main.clientBeneficiariesList.Count == 0)
					//	{
							//MessageBox.Show("Lowcost" + main.clientBeneficiariesList.Count);
							
							main.loadinsert = new JavaScriptSerializer().Deserialize<ObservableCollection<InsertClientBeneficiaries>>(Response.Content);
							DG_Form.ItemsSource = clientBeneficiariesList;

					//	}
					//	else
					//	{
					//		DG_Form.ItemsSource =  main.clientBeneficiariesList;
					//		MessageBox.Show("Main" + main.clientBeneficiariesList.Count);
					//	}


					//}


					// DG_List.Focus();

				}
				else
				{
					MessageBox.Show(Response.Content);
				}
			}


		}

		private void DG_Form_PreviewKeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
			{
				this.Close();
				e.Handled = true;
			}
			if (e.Key == Key.Enter)
			{
				if (optionstatus == 1)
				{
					selectClient();
				}
				else
				{
					DG_Form.CommitEdit();
				}
				

				e.Handled = true;
			}
			if (e.Key == Key.Tab)
			{

				if (DG_Form.CurrentColumn.DisplayIndex == 3)
				{
					DG_Form.Focus();
					DG_Form.CurrentCell = new DataGridCellInfo(DG_Form.SelectedItem, DG_Form.Columns[4]);
					DG_Form.BeginEdit();
					return;

				}


			}
			if (e.Key == Key.Insert)
			{
				main.loadinsert.Add(new InsertClientBeneficiaries { });
				DG_Form.ItemsSource = main.loadinsert;
				DG_Form.SelectedIndex = main.loadinsert.Count - 1;
				DG_Form.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
				DG_Form.Focus();
				//DG_Form.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
				//DG_Attendance.CurrentCell = new DataGridCellInfo(DG_Attendance.SelectedItem, DG_Attendance.Columns[1]);
				DG_Form.CurrentCell = new DataGridCellInfo(DG_Form.SelectedItem, DG_Form.Columns[0]);
				DG_Form.BeginEdit();
				e.Handled = true;
			}
			if (e.Key == Key.F11) {
				CloseWindow();
				e.Handled = true;
			}

		}

		private void OptionList(int option)
		{
			switch (option)
			{
				case 1:
					ITFAccounts();
					break;
				case 2:

					CRUXLib.Response Response = App.CRUXAPI.request("client/loadRelations", "");
					List<Relation> IncomeTypeList = new JavaScriptSerializer().Deserialize<List<Relation>>(Response.Content);

					// this.main.clientBeneficiariesList = new List<InsertClientBeneficiaries>(); 
					//if (this.main.clientBeneficiariesList.Count > 0)
					//{
					brelation.ItemsSource = IncomeTypeList;

					//}

					//Beneficiaries();
					//loadBeneficiaries();
					break;
			}
		}

		private void ITFAccounts()
		{
			//DG_Form.IsReadOnly = true;
			Title = "ITF List";
			DG_Form.IsReadOnly = true;
			DG_Form.IsEnabled = true;
			
			DataGridTextColumn dataGridBranchName = new DataGridTextColumn();
			dataGridBranchName.Header = "Client ID";
			dataGridBranchName.Binding = new Binding("ClientID");
			dataGridBranchName.Width = new DataGridLength(3, DataGridLengthUnitType.Star);
			dataGridBranchName.DisplayIndex = 0;
			DG_Form.Columns.Add(dataGridBranchName);

			boccupation.Header = "Age";
			boccupation.Binding = new Binding("RelationID");
			brelation.Visibility = Visibility.Hidden;

			DG_Form.PreviewKeyDown += new KeyEventHandler(DG_Form_PreviewKeyDown);
			//DataGridTextColumn dataGridClientType = new DataGridTextColumn();
			//dataGridClientType.Header = "Client Type";
			//dataGridClientType.Binding = new Binding("ClientType");
			//dataGridClientType.Width = new DataGridLength(30, DataGridLengthUnitType.Star);
			//DG_Form.Columns.Add(dataGridClientType);
		}
		
		private void selectClient()
		{
			
			if (DG_Form.Items.Count>0)
			{
				Object client = DG_Form.SelectedItem;
				InsertClientBeneficiaries o3 = (InsertClientBeneficiaries)client;
				ClientSearch o2 = new ClientSearch();
				o2.ClientID = o3.ClientID;

				o2.BranchID = o3.BranchID;

				String parsed = new JavaScriptSerializer().Serialize(o2);

				CRUXLib.Response Response = App.CRUXAPI.request("client/selectedClient", parsed);
				if (Response.Status == "SUCCESS")
				{
					List<EditClient> listCompany = new JavaScriptSerializer().Deserialize<List<EditClient>>(Response.Content);
					Console.WriteLine(Response.Content);
					this.Close();
					this.main.DisplayEditClient(listCompany);
					this.main.btn_edit.IsEnabled = true;
					this.main.btn_edit.Opacity = 100;
					//main.clientBeneficiariesList.Clear();
					main.btn_find.Focus();
				}
				else
				{
					MessageBox.Show(Response.Content);
				}

			}
			
		}
		


		private void btn_edit_Click(object sender, ExecutedRoutedEventArgs e)
		{
			//DG_Form.IsReadOnly = false;
			//btn_add.IsEnabled = true;
		}

		

		private void btn_cancel_Click(object sender, ExecutedRoutedEventArgs e)
		{
			CloseWindow();
		}

		private void Window_Closed(object sender, EventArgs e)
		{

			CloseWindow();
		}
		private void CloseWindow()
		{
			if (main.btn_edit.IsEnabled == false)
			{
				if (optionstatus == 2)
				{
					if (clientID != 0)
					{
						DG_Form.Focus();
						DG_Form.CurrentCell = new DataGridCellInfo(DG_Form.SelectedItem, DG_Form.Columns[0]);
						DG_Form.BeginEdit();
						var selected = DG_Form.SelectedItem as InsertClientBeneficiaries;
						//var selects = DG_Form.ItemsSource;
						this.main.clientBeneficiariesList.Clear();
                        //DG_Form.Items.Refresh();

                        MessageBox.Show(DG_Form.Items.Count.ToString());

                        foreach (var otherID in DG_Form.ItemsSource)
						{

							InsertClientBeneficiaries insertBeneficiaries = (InsertClientBeneficiaries)otherID;


							if (!String.IsNullOrWhiteSpace(insertBeneficiaries.LastName))
							{

								if (!String.IsNullOrWhiteSpace(insertBeneficiaries.DateOfBirth))
								{
									DateTime results1;

									if (DateTime.TryParseExact(insertBeneficiaries.DateOfBirth, "MMddyyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out results1))
									{

										DateTime bdate = DateTime.ParseExact(insertBeneficiaries.DateOfBirth, "MMddyyyy", CultureInfo.InvariantCulture);
										this.main.clientBeneficiariesList.Add(new InsertClientBeneficiaries
										{
											BeneficiaryID = insertBeneficiaries.BeneficiaryID,
											DateOfBirth = bdate.ToString("MM/dd/yyyy"),
											LastName = insertBeneficiaries.LastName,
											FirstName = insertBeneficiaries.FirstName,
											MiddleName = insertBeneficiaries.MiddleName,
											RelationID = insertBeneficiaries.RelationID,
											Occupation = String.IsNullOrEmpty(insertBeneficiaries.Occupation) ? "" : insertBeneficiaries.Occupation,
											ClientID = clientID,
											BranchID = insertBeneficiaries.BranchID,
											datetimeAdded = insertBeneficiaries.datetimeAdded,
											datetimeModified = insertBeneficiaries.datetimeModified,
										});

									}
									else
									{
										MessageBox.Show("Birthdate cannot be empty");
									}

								}


							}

						}
						main.InsertBeneficiaries(clientID.ToString(), this.main.clientBeneficiariesList);
					}
				}
			}
			this.Close();
		}
		private void DG_Form_Loaded(object sender, RoutedEventArgs e)
		{
			
				if (main.BasicInfoGrid.IsEnabled == true)
				{
					if (DG_Form.Items.Count == 0)
					{

						main.loadinsert.Add(new InsertClientBeneficiaries { BeneficiaryID = 1 });
						DG_Form.ItemsSource = main.loadinsert;
						DG_Form.SelectedIndex = main.loadinsert.Count - 1;
						DG_Form.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
						DG_Form.Focus();
						DG_Form.CurrentCell = new DataGridCellInfo(DG_Form.SelectedItem, DG_Form.Columns[0]);
						DG_Form.BeginEdit();
					}
					else
					{
						DG_Form.Focus();
						DG_Form.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
						DG_Form.CurrentCell = new DataGridCellInfo(DG_Form.Items[0], DG_Form.Columns[0]);

						DG_Form.BeginEdit();
					}
				}

			
			
		}

        private void bdateofbirth_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            MessageBox.Show("test");
        }
    }
}

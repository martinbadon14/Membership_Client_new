﻿using Members_Information.Models.DOSRI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Members_Information
{
    /// <summary>
    /// Interaction logic for DOSRIAcctType.xaml
    /// </summary>
    public partial class DOSRIAcctType : Window
    {
        MainWindow mww;
        private ICollectionView MyData;
        String SearchText = string.Empty;
        String DefaultColumn = "";
        int currentRow = 0, currentColumn = 1;

        public DOSRIAcctType(MainWindow mw, List<Models.DOSRI.DosRiAcctType> dosriAcctTypes, Int16 dosRiID = 0)
        {
            InitializeComponent();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.mww = mw;
            PopulateGrid(dosriAcctTypes, dosRiID);
            searchTextbox.Focus();
        }

        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Up)
                {
                    if (currentRow > 0)
                    {
                        try
                        {
                            int previousIndex = DG_DOSRIAcctType.SelectedIndex - 1;
                            if (previousIndex < 0) return;
                            DG_DOSRIAcctType.SelectedIndex = previousIndex;
                            DG_DOSRIAcctType.ScrollIntoView(DG_DOSRIAcctType.Items[currentRow]);

                        }
                        catch (Exception ex)
                        {
                            e.Handled = true;
                        }
                    }

                }
                else if (e.Key == Key.Down)
                {
                    if (currentRow < DG_DOSRIAcctType.Items.Count - 1)
                    {
                        try
                        {
                            int nextIndex = DG_DOSRIAcctType.SelectedIndex + 1;
                            if (nextIndex > DG_DOSRIAcctType.Items.Count - 1) return;
                            DG_DOSRIAcctType.SelectedIndex = nextIndex;
                            DG_DOSRIAcctType.ScrollIntoView(DG_DOSRIAcctType.Items[currentRow]);
                        }
                        catch (Exception ex)
                        {
                            e.Handled = true;
                        }

                    } // end if (this.SelectedOverride > 0)

                } // end else if (e.Key == Key.Down)

                else if (e.Key == Key.Escape)
                {
                    this.Close();
                    e.Handled = true;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void PopulateGrid(List<Models.DOSRI.DosRiAcctType> dosriAcctTypes, Int16 dosRiID = 0)
        {
            if (dosRiID == 1 || dosRiID == 2)
            {
                DG_DOSRIAcctType.ItemsSource = dosriAcctTypes.FindAll(i => i.AcctTypesID == 1).ToList();
            }
            else
            {
                DG_DOSRIAcctType.ItemsSource = dosriAcctTypes;
            }

            if (dosriAcctTypes.Count != 0)
            {
                DG_DOSRIAcctType.SelectedItem = dosriAcctTypes.FirstOrDefault();
            }

            MyData = CollectionViewSource.GetDefaultView(dosriAcctTypes);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void DG_DOSRI_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    SelectDOSRIAcctType();
                }
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void SelectDOSRIAcctType()
        {
            if (DG_DOSRIAcctType.SelectedItem == null) return;
            var selectedDOSRIAcctType = DG_DOSRIAcctType.SelectedItem as Models.DOSRI.DosRiAcctType;

            if (this.mww != null)
            {
                InsertDOSRI selected = (InsertDOSRI)this.mww.DG_DOSRI.SelectedItem;
                selected.AcctTypeID = selectedDOSRIAcctType.AcctTypesID;
                selected.AcctTypeDesc = selectedDOSRIAcctType.AcctTypeDesc;

                this.Close();
                Keyboard.Focus(this.mww.GetDataGridCell(this.mww.DG_DOSRI.SelectedCells[2]));
            }
        }

        private void DG_DOSRIAcctType_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                SelectDOSRIAcctType();
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SelectDOSRIAcctType();
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;
            if (cell.Column.DisplayIndex != this.currentColumn)
            {
                cell.IsSelected = false;
            }


            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }

        private void searchTextbox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (DG_DOSRIAcctType.Items != null)
                    {
                        SelectDOSRIAcctType();
                        e.Handled = true;
                    }

                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

    }
}

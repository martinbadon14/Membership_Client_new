﻿using Members_Information.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Members_Information
{
    /// <summary>
    /// Interaction logic for Benficiaries.xaml
    /// </summary>
    public partial class Benficiaries : Window
    {
        Boolean IsEscape = false;
        public BeneficiaryContext Data = new BeneficiaryContext();
        public Benficiaries(Int64 ClientID, Int64 BranchID, Boolean IsEditable)
        {
            InitializeComponent();
            this.Data.ClientID = ClientID;
            this.Data.BranchID = BranchID;
            this.Data.EnableEdit = IsEditable;


            LoadBeneficiaries();

            this.DataContext = this.Data;
        }


        void LoadBeneficiaries()
        {

            CRUXLib.Response Response = App.CRUXAPI.request("client/loadRelations", "");
            List<Relation> RelationsList = new JavaScriptSerializer().Deserialize<List<Relation>>(Response.Content);

            ComboBoxColumn.ItemsSource = RelationsList;

            this.Data.Beneficiaries = new List<ClientBeneficiaries>();
            ClientSearch o2 = new ClientSearch();
            o2.ClientID = this.Data.ClientID;
            o2.BatchNum = this.Data.BranchID;
            String parsed = new JavaScriptSerializer().Serialize(o2);
            Response = App.CRUXAPI.request("client/beneficiaries", parsed);
            if (Response.Status == "SUCCESS")
            {
                this.Data.Beneficiaries = new JavaScriptSerializer().Deserialize<List<ClientBeneficiaries>>(Response.Content);

                foreach(ClientBeneficiaries cb in this.Data.Beneficiaries)
                {
                    cb.Relations = new List<Relation>();
                    cb.Relations = RelationsList;
                }

            }
            else
            {
                MessageBox.Show(Response.Content);
            }

        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (!this.Data.EnableEdit)
            {
                if (Key.Insert == e.Key)
                {
                    this.Data.Beneficiaries.Add(new ClientBeneficiaries() { ClientID = this.Data.ClientID });
                    this.BeneficiaryGrid.CancelEdit();
                    this.BeneficiaryGrid.IsReadOnly = true;
                    this.BeneficiaryGrid.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
                    this.BeneficiaryGrid.Items.Refresh();

                    this.BeneficiaryGrid.IsReadOnly = false;
                    this.BeneficiaryGrid.Focus();
                    this.BeneficiaryGrid.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
                    this.BeneficiaryGrid.SelectedIndex = this.Data.Beneficiaries.Count - 1;
                    this.BeneficiaryGrid.CurrentCell = new DataGridCellInfo(this.BeneficiaryGrid.SelectedItem, this.BeneficiaryGrid.Columns[0]);
                    this.BeneficiaryGrid.BeginEdit();
                }
                else if (Key.Delete == e.Key)
                {
                    int index = this.BeneficiaryGrid.SelectedIndex;

                    ClientBeneficiaries cb = this.Data.Beneficiaries.ElementAt(index);

                    MessageBoxResult v =  MessageBox.Show("Are you sure you want to delete " + cb.LastName + ", " + cb.FirstName + " from the List of Beneficiaries?","CONFIRM!!!",MessageBoxButton.YesNo,MessageBoxImage.Warning);

                    if (v == MessageBoxResult.Yes)
                    {
                        this.BeneficiaryGrid.IsReadOnly = true;
                        this.Data.Beneficiaries.RemoveAt(index);
                        this.BeneficiaryGrid.CancelEdit();
                        this.BeneficiaryGrid.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));

                        this.BeneficiaryGrid.IsReadOnly = false;
                        this.BeneficiaryGrid.Items.Refresh();
                        this.BeneficiaryGrid.Focus();
                        this.BeneficiaryGrid.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
                        //this.BeneficiaryGrid.SelectedIndex = this.Data.Beneficiaries.Count - 1;
                        this.BeneficiaryGrid.CurrentCell = new DataGridCellInfo(this.BeneficiaryGrid.SelectedItem, this.BeneficiaryGrid.Columns[0]);
                    }

                }
                else if(Key.F11 == e.Key)
                {
                    e.Handled = true;
                    this.Close();
                }
               
            }
            if (Key.Escape == e.Key)
            {
                e.Handled = true;
                this.IsEscape = true;
                this.Close();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Boolean Continue = true;

            Boolean ContinueEscape = true;

            if (!this.Data.EnableEdit)
            {
                if(IsEscape)
                {
                    MessageBoxResult v = MessageBox.Show("Do you wish to submit your changes if there are any?", "CONFIRM!!!", MessageBoxButton.YesNo, MessageBoxImage.Hand);
                    if (MessageBoxResult.No == v)
                    {
                        ContinueEscape = false;
                    }
                }
                if (ContinueEscape)
                {
                    List<InsertClientBeneficiaries> beneficiaries = new List<InsertClientBeneficiaries>();

                    foreach (ClientBeneficiaries CurrentData in this.Data.Beneficiaries)
                    {
                        InsertClientBeneficiaries beneficiary = new InsertClientBeneficiaries();

                        //beneficiary.BeneficiaryID = CurrentData.BeneficiaryID;

                        beneficiary.BeneficiaryID = CurrentData.BeneficiaryID;
                        beneficiary.DateOfBirth = CurrentData.DateOfBirth;

                        beneficiary.LastName = CurrentData.LastName;

                        beneficiary.FirstName = CurrentData.FirstName;
                        beneficiary.MiddleName = CurrentData.MiddleName;
                        beneficiary.RelationID = CurrentData.RelationID;
                        beneficiary.Occupation = String.IsNullOrEmpty(CurrentData.Occupation) ? "" : CurrentData.Occupation;
                        beneficiary.ClientID = CurrentData.ClientID;
                        beneficiary.BranchID = CurrentData.BranchID;
                        beneficiary.datetimeAdded = CurrentData.datetimeAdded;
                        beneficiary.datetimeModified = CurrentData.datetimeModified;


                        beneficiaries.Add(beneficiary);

                        if (beneficiary.RelationID == 0)
                        {
                            Continue = false;
                        }
                    }

                    if (Continue)
                    {

                        ClientBenefParams b = new ClientBenefParams();

                        b.ClientID = this.Data.ClientID;
                        b.beneficiaries = beneficiaries;

                        string parsedIDs = new JavaScriptSerializer().Serialize(b);

                        CRUXLib.Response ResponseIDs = App.CRUXAPI.request("client/insertBeneficiaries", parsedIDs);
                        if (ResponseIDs.Status == "SUCCESS")
                        {
                            MessageBox.Show("Client Beneficiaries successfully updated!", "SUCCESS", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            Boolean closeWindow = true;
                            if (ResponseIDs.Content == "Conversion failed when converting date and/or time from character string.")
                            {
                                MessageBoxResult v = MessageBox.Show("There is an error on the encoded birthdate...\n Do you wish to edit your entry?", "ERROR!", MessageBoxButton.YesNo, MessageBoxImage.Hand);

                                if (MessageBoxResult.Yes == v)
                                {
                                    closeWindow = false;
                                    e.Cancel = true;
                                }
                            }


                            if (closeWindow)
                            {
                                MessageBox.Show("Beneficiaries Not Successfully Updated", "Warning!", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            return;
                        }
                    }
                    else
                    {
                        MessageBoxResult v = MessageBox.Show("Unable to save entry due to one or more  beneficiaries had no ralation set to the Member...\n\nDo you wish to edit your entry?", "ERROR!", MessageBoxButton.YesNo, MessageBoxImage.Hand);

                        if (MessageBoxResult.Yes == v)
                        {
                            e.Cancel = true;
                        }
                        else
                        {
                            MessageBox.Show("Beneficiaries Not Successfully Updated", "Warning!", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                }
            }
        }

        private void BeneficiaryGrid_Loaded(object sender, RoutedEventArgs e)
        {
            this.BeneficiaryGrid.Focus();
            this.BeneficiaryGrid.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
            this.BeneficiaryGrid.CurrentCell = new DataGridCellInfo(this.BeneficiaryGrid.SelectedItem, this.BeneficiaryGrid.Columns[0]);
        }
    }
}

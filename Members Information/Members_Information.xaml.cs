﻿using Members_Information.Models;
using CRUXLib;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Web.Script.Serialization;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Text.RegularExpressions;
using System.Globalization;
using Microsoft.Win32;
using System.Drawing;
using System.ComponentModel;
using System.Dynamic;
using System.Windows.Interop;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using Members_Information.Models.Client_History;
using Members_Information.Models.DOSRI;

namespace Members_Information
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        private bool option = true;
        public Int64 provinceChange = 0;
        public Int64 cityChange = 0;
        public Int64 CityID;
        public Int64 ClientID;
        public Int64 AttendanceID = 0;
        public Int64 PMESBatchNumber = 0;
        public Int64 PMESBranchID = 0;
        public SystemDateClass system = new SystemDateClass();
        public ProvinceAndCityCombo provinceAndCityComboIDs;
        public InsertClient client;
        public Stream stream;
        enum clientTypes { RegularMember = 1, ProbitionaryMember, JuniorDepositor, SpecialDepositor, TemporaryAccount, Customer, Supplier };
        public ObservableCollection<Notification> loadNotifications = new ObservableCollection<Notification>();
        public ObservableCollection<InsertClientBeneficiaries> loadinsert = new ObservableCollection<InsertClientBeneficiaries>();
        List<Provinces> listSProvinces;
        List<Cities> listCities;
        public Members_New mws;
        public List<EditClient> getbarangay = new List<EditClient>();
        private Int64 displayClientID;
        private Int64 displayBranchID;
        //List<Barangay> listBarangays;
        public string userid;
        public BitmapImage imgsrc = new BitmapImage();
        public String searchName = "";
        public String checkAll = "";
        public String checkClose = "";
        public Int32 firstLoad = 0;

        public List<InsertClientBeneficiaries> clientBeneficiariesList = new List<InsertClientBeneficiaries>();

        //added by Martin: 07-05-2018
        public List<InsertDOSRI> insertDOSRIs;
        List<DosRi> dosRis;
        List<DosRiAcctType> dosriAcctTypes;
        Int64 defaultBranchID = 0;

        public MainWindow()
        {
            //String args
            InitializeComponent();
            //App.CRUXAPI = new CRUXLib.API();
            //string[] aaa = args.Split(' ');
            //List<String> ll = aaa.ToList<String>();
            //App.CRUXAPI.loadParams(ll);
            EventManager.RegisterClassHandler(typeof(TextBox), TextBox.GotFocusEvent, new RoutedEventHandler(TextBox_GotFocus));
            EventManager.RegisterClassHandler(typeof(TextBox), TextBox.PreviewMouseDownEvent, new MouseButtonEventHandler(TextBox_PreviewMouseDown));
            btn_find.Focus();
            Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
            client = new InsertClient();


            listSProvinces = new List<Provinces>();
            listCities = new List<Cities>();
            provinceAndCityComboIDs = new ProvinceAndCityCombo();

            Populate();
            this.DataContext = client;
            tabControl.SelectedIndex = 0;

            //userid = App.CRUXAPI.Arguments;

            //if (!String.IsNullOrEmpty(userid))
            //{
            //	ClientParams user = new JavaScriptSerializer().Deserialize<ClientParams>(userid);
            //	loadclientName(user);
            //}

            setDate();
            dateOpenedPicker.SelectedDate = system.SystemDate;
            Disable();
            otherid.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Categories_CollectionChanged);
            this.DataContext = otherid;
            DG_OtherID.ItemsSource = otherid;
        }

        void Categories_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                //MessageBox.Show("New Row Added");
            }
        }

        void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        void TextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var TextBox = (TextBox)sender;
            if (!TextBox.IsKeyboardFocusWithin)
            {
                TextBox.Focus();
                e.Handled = true;
            }
        }

        private void setDate()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("frontoffice/SystemDate", "");
            if (Response.Status == "SUCCESS")
            {
                system = new JavaScriptSerializer().Deserialize<SystemDateClass>(Response.Content);

            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        private void refreshMe()
        {
            InitializeComponent();
            EventManager.RegisterClassHandler(typeof(TextBox), TextBox.GotFocusEvent, new RoutedEventHandler(TextBox_GotFocus));
            EventManager.RegisterClassHandler(typeof(TextBox), TextBox.PreviewMouseDownEvent, new MouseButtonEventHandler(TextBox_PreviewMouseDown));
            //btn_find.Focus();
            Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
            client = new InsertClient();
            getbarangay = new List<EditClient>();

            if (getbarangay.Count > 0)
            {
                listSProvinces = new List<Provinces>();
                listCities = new List<Cities>();
                provinceAndCityComboIDs = new ProvinceAndCityCombo();
            }

            tabControl.SelectedIndex = 0;
            Populate();
            this.DataContext = client;
            DG_AddressHistory.ItemsSource = null;
            dg_emphistory.ItemsSource = null;
            dataGridNotification.ItemsSource = null;

            //added by Martin : 07-27-2018
            DG_DOSRI.ItemsSource = null;

            /*userid = App.CRUXAPI.Arguments;
			if (!String.IsNullOrEmpty(userid))
			{
				ClientParams user = new JavaScriptSerializer().Deserialize<ClientParams>(userid);
				loadclientName(user);
			}*/

            setDate();
            Disable();
            btn_find.Focus();
            clientBeneficiariesList.Clear();
            loadinsert.Clear();
            BeneficiariesButton.IsEnabled = false;
            ITFAccountButton.IsEnabled = false;
            dateOpenedPicker.SelectedDate = system.SystemDate;
        }

        /// <summary>
        /// Load Refresher ClientID :By Martin 05/04/2018
        /// </summary>
        /// <param name="user"></param>
        private void LoadRefresherID(ClientParams user)
        {
            String parsed = new JavaScriptSerializer().Serialize(user);

            CRUXLib.Response Response = App.CRUXAPI.request("history/SelectedClientHistory", parsed);
            if (Response.Status == "SUCCESS")
            {
                DG_Refresher.ItemsSource = new JavaScriptSerializer().Deserialize<List<ClientHistoryDetails>>(Response.Content);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        private void LoadDOSRI(ClientParams user)
        {
            String parsed = new JavaScriptSerializer().Serialize(user);

            CRUXLib.Response Response = App.CRUXAPI.request("client/SelectedClientDOSRI", parsed);
            if (Response.Status == "SUCCESS")
            {
                DG_DOSRI.ItemsSource = new JavaScriptSerializer().Deserialize<List<InsertDOSRI>>(Response.Content);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        private void loadclientName(ClientParams user)
        {
            String parsed = new JavaScriptSerializer().Serialize(user);

            CRUXLib.Response Response = App.CRUXAPI.request("client/selectedClient", parsed);
            if (Response.Status == "SUCCESS")
            {
                List<EditClient> listCompany = new JavaScriptSerializer().Deserialize<List<EditClient>>(Response.Content);


                DisplayEditClient(listCompany);


                //this.btn_edit.IsEnabled = true;
                //this.btn_edit.Opacity = 100;

            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public MainWindow(int displayEdit)
        {
            client = new InsertClient();

            listSProvinces = new List<Provinces>();
            listCities = new List<Cities>();
            provinceAndCityComboIDs = new ProvinceAndCityCombo();
            //this.DataContext = client;
        }

        public void NextClient(object sender, RoutedEventArgs e)
        {
            ClientSearch o2 = new ClientSearch();
            o2.ClientID = displayClientID;
            o2.BranchID = displayBranchID;
            String parsed = new JavaScriptSerializer().Serialize(o2);

            CRUXLib.Response Response = App.CRUXAPI.request("client/selectedNextClient", parsed);
            if (Response.Status == "SUCCESS")
            {
                List<EditClient> listCompany = new JavaScriptSerializer().Deserialize<List<EditClient>>(Response.Content);
                DisplayEditClient(listCompany);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        public void PreviousClient(object sender, RoutedEventArgs e)
        {
            ClientSearch o2 = new ClientSearch();
            o2.ClientID = displayClientID;
            o2.BranchID = displayBranchID;
            String parsed = new JavaScriptSerializer().Serialize(o2);

            CRUXLib.Response Response = App.CRUXAPI.request("client/selectedPreviousClient", parsed);
            if (Response.Status == "SUCCESS")
            {
                List<EditClient> listCompany = new JavaScriptSerializer().Deserialize<List<EditClient>>(Response.Content);
                DisplayEditClient(listCompany);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        private void cellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            //Only handles cases where the cell contains a TextBox
            var editedTextbox = e.EditingElement as TextBox;


            if (editedTextbox != null)
                MessageBox.Show("Value after edit: " + editedTextbox.Text, "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void DisplayClient(List<InsertPMESAttendant> clientList)
        {
            refreshMe();
            clientIDText.Text = "";
            txtAgeUponEntry.Text = "";
            recruitedByTextBox.Text = "";
            getbarangay = new List<EditClient>();
            displayClientID = 0;
            OtherButtonsGrid.IsEnabled = true;
            String convertToSmallLName = clientList.First().LastName.ToLower();
            String convertToSmallFName = clientList.First().FirstName.ToLower();
            String convertToSmallMName = clientList.First().MiddleName.ToLower();
            lastNameText.Text = FirstCharToUpper(convertToSmallLName);
            firstNameText.Text = FirstCharToUpper(convertToSmallFName);
            middleNameText.Text = FirstCharToUpper(convertToSmallMName);
            batchNoText.Text = clientList.First().Batch;
            dateofPMES.Text = clientList.First().DateOfPMES;
            //principalText.Text= clientList.First()
            dateOfBirthPicker.Text = clientList.First().BirthDate;
            dateOpenedPicker.SelectedDate = Convert.ToDateTime(clientList.First().SystemDate);
            suffixComboBox.SelectedValue = clientList.First().SuffixID;
            AttendanceID = Convert.ToInt64(clientList.First().AttendanceID);
            PMESBatchNumber = Convert.ToInt64(clientList.First().PMESBatch);
            PMESBranchID = clientList.First().BranchID;
            //oldPSBNTextBox.Text = clientList.First().
            tabControl.SelectedIndex = 0;
            MainGrid.IsEnabled = true;
            btn_save.IsEnabled = true;
            btn_save.Opacity = 100;
            btn_cancel.IsEnabled = true;
            Enable();
            titleCombo.Focus();

            //btn_find.Focus();
            btn_find.IsEnabled = false;
            BeneficiariesButton.IsEnabled = true;
        }

        public void DisplayEditClient(List<EditClient> clientList)
        {
            //*******PARA SA ACCESS LEVEL?********//
            //if (App.CRUXAPI.isAllowed("client_access"))
            //{
            if (clientList.Count > 0)
            {
                //}
                MainWindow mw = new MainWindow(1);
                //ObservableCollection<EditClient> clientList = new ObservableCollection<EditClient>(clientinformation);
                clientTypeCombo.Items.Refresh();
                getbarangay = clientList;

                //lastNameText.Text = FirstCharToUpper(clientList.First().LastName);
                //firstNameText.Text = FirstCharToUpper(clientList.First().FirstName);
                //middleNameText.Text = FirstCharToUpper(clientList.First().MiddleName);
                //batchNoText.Text = clientList.First().BatchNumber;
                //dateofPMES.Text = clientList.First().DateOfPMES;
                //dateOfBirthPicker.Text = clientList.First().DateOfBirth;
                //dateOpenedPicker.SelectedDate = Convert.ToDateTime(clientList.First().DateOpened);
                //suffixComboBox.SelectedValue = clientList.First().Suffix;

                displayClientID = clientList.First().ClientID;
                displayBranchID = clientList.First().BranchID;
                suffixComboBox.SelectedValue = clientList.FirstOrDefault().SuffixID;

                provinceAndCityComboIDs.ProvinceID = clientList.FirstOrDefault().ProvinceID;
                provinceAndCityComboIDs.PProvinceID = clientList.FirstOrDefault().PProvinceID;

                provinceAndCityComboIDs.HomeProvinceID = clientList.FirstOrDefault().HomeProvinceID;
                provinceAndCityComboIDs.MotherProvinceID = clientList.FirstOrDefault().MotherProvinceID;
                provinceAndCityComboIDs.FatherProvinceID = clientList.FirstOrDefault().FatherProvinceID;
                provinceAndCityComboIDs.EmpProvinceID = clientList.FirstOrDefault().EmpProvinceID;
                provinceAndCityComboIDs.EmpSelfProvinceID = clientList.FirstOrDefault().SelfEmpProvinceID;

                provinceAndCityComboIDs.CityID = clientList.FirstOrDefault().PMunicipalityID;
                provinceAndCityComboIDs.HomeCityID = clientList.FirstOrDefault().HomeMunicipalityID;
                provinceAndCityComboIDs.MotherCityID = clientList.FirstOrDefault().MotherCityID;
                provinceAndCityComboIDs.FatherCityID = clientList.FirstOrDefault().FatherCityID;
                provinceAndCityComboIDs.EmpCityID = clientList.FirstOrDefault().EmpMunicipalityID;
                provinceAndCityComboIDs.EmpSelfCityID = clientList.FirstOrDefault().SelfEmpMunicipalityID;

                //Data Grid
                DG_ContactType.ItemsSource = clientList.FirstOrDefault().ClientContactList;
                DG_PrimaryID.ItemsSource = clientList.FirstOrDefault().ClientIDList;
                DG_OtherID.ItemsSource = clientList.FirstOrDefault().ClientOtherIDList;
                DG_Asset.ItemsSource = clientList.FirstOrDefault().ClientAssetsList;
                DG_Dependent.ItemsSource = clientList.FirstOrDefault().ClientDependentsList;
                DG_AddressHistory.ItemsSource = clientList.FirstOrDefault().ClientAddressList;
                dg_emphistory.ItemsSource = clientList.FirstOrDefault().ClientCompanyHistoryList;
                DG_Bank.ItemsSource = clientList.FirstOrDefault().ClientBanksList;

                //dateOfBirthPicker.SelectedDate = Convert.ToDateTime(clientList.FirstOrDefault().DateOfBirth);
                dateOpenedPicker.SelectedDate = Convert.ToDateTime(clientList.FirstOrDefault().DateOpened);
                clientIDText.Text = (clientList.FirstOrDefault().BranchID.ToString().PadLeft(2, '0')) + "-" + (clientList.FirstOrDefault().ClientID.ToString().PadLeft(7, '0')) + "-" + clientList.FirstOrDefault().ClientChkID.ToString();
                if (clientList.FirstOrDefault().Nationality == 0)
                {
                    clientList.FirstOrDefault().Nationality = client.Nationality;
                    //nationalityCombo.SelectedValue = client.Nationality;
                }
                loadBranches();
                this.DataContext = clientList;



                OtherButtonsGrid.IsEnabled = true;
                //dosRiComboBox.SelectedValue = clientList.FirstOrDefault().DosriID;
                if (!String.IsNullOrEmpty(relatedToTxt.Text))
                {
                    relatedToTxt.Visibility = Visibility.Visible;
                    relatedlabel.Visibility = Visibility.Visible;
                }


                tabControl.SelectedIndex = 0;
                this.btn_edit.IsEnabled = true;
                this.btn_edit.Opacity = 100;

                string brID = string.Format("{0:D2}", clientList.FirstOrDefault().BranchID);

                // ... Create a new BitmapImage.
                BitmapImage b = new BitmapImage();
                b.BeginInit();

                String crux = App.CRUXAPI.Server.Host;
                b.UriSource = new Uri(@"\\10.15.40.211\e\Image\" + brID + "_" + clientList.FirstOrDefault().ClientID + ".jpg");

                if (File.Exists(@"\\10.15.40.211\e\Image\" + brID + "_" + clientList.FirstOrDefault().ClientID + ".jpg"))
                {
                    b.EndInit();
                    clientImage.Source = b;
                }
                else
                {
                    clientImage.Source = null;
                }




                // ... Create a new Signature.
                BitmapImage signature = new BitmapImage();
                signature.BeginInit();
                signature.CreateOptions |= BitmapCreateOptions.IgnoreColorProfile;

                signature.UriSource = new Uri(@"\\10.15.40.211\e\Signature\" + brID + "_" + clientList.FirstOrDefault().ClientID + ".jpg");
                //signature.UriSource = new Uri(@"\\" + crux + "\\e\\Signature\\" + brID + "_" + clientList.FirstOrDefault().ClientID + ".jpg");

                //if (File.Exists(@"\\" + crux + " \\e\\Signature\\" + brID + "_" + clientList.FirstOrDefault().ClientID + ".jpg"))
                if (File.Exists(@"\\10.15.40.211\e\Signature\" + brID + "_" + clientList.FirstOrDefault().ClientID + ".jpg"))
                {
                    signature.EndInit();
                    clientSignature.Source = signature;
                }
                else
                {
                    clientSignature.Source = null;
                }

                oldPSBNTextBox.Text = clientList.FirstOrDefault().OldPSBN;
                dateOfBirthPicker.Text = clientList.FirstOrDefault().DateOfBirth;

                DateTime selecteddate;
                string SelectedDate;
                string gender = "her";
                string gender2 = "her";
                if (clientList.FirstOrDefault().GenderID == 1)
                {
                    gender = "his";
                    gender2 = "him";
                }
                if (DateTime.TryParseExact(dateOfBirthPicker.Text, "MM/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out selecteddate))
                {
                    SelectedDate = selecteddate.ToString("yyyyMMdd");
                    String DateOpened = Convert.ToString(dateOpenedPicker.SelectedDate.Value.ToString("yyyyMMdd"));
                    Int64 getAgeUponEntry = (Convert.ToInt64(DateOpened) - Convert.ToInt64(SelectedDate)) / 10000;
                    txtAgeUponEntry.Text = getAgeUponEntry.ToString();
                }
                else
                {
                    return;
                }

                Boolean TriggerPop = false;

                String DateToday = Convert.ToString(DateTime.Now.ToString("MMdd"));
                String BirthdayDate = selecteddate.ToString("MMdd");
                Int64 getAge = (Convert.ToInt64(Convert.ToString(DateTime.Now.ToString("yyyyMMdd"))) - Convert.ToInt64(SelectedDate)) / 10000;

                loadNotification();
                if (loadNotifications.Count != 0 || BirthdayDate == DateToday)
                {
                    TriggerPop = true;
                }

                titleCombo.Focus();

                if (TriggerPop)
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() =>
                    {
                        //String DateToday = Convert.ToString(DateTime.Now.ToString("MMdd"));
                        //String BirthdayDate = selecteddate.ToString("MMdd");

                        if (BirthdayDate == DateToday)
                        {
                            MessageBox.Show("This MEMBER is CELEBRATING " + gender + " BIRTHDAY TODAY!!! \n\n" + clientList.FirstOrDefault().LastName + "," + clientList.FirstOrDefault().FirstName + " " + clientList.FirstOrDefault().MiddleName + " is turning " + getAge + " years old today.\n\nHave time to greet " + gender2 + " with a smile!!! ", "Happy Birthday!", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        if (this.IsActive == true)
                        {

                            //// members_list.Close();
                            foreach (var jefrey in loadNotifications)
                            {
                                if (jefrey.IsPrompt)
                                {
                                    MessageBox.Show("Description: " + jefrey.NotificationStatus + "\nEncoded by: " + jefrey.EncodedBy + "\nRemarks: " + jefrey.Remarks + "\nDate: " + jefrey.TR_DATE, "Alert!", MessageBoxButton.OK, MessageBoxImage.Information);
                                    lastNameText.Focus();
                                }
                            }
                        }

                    }));
                }


                ////Loading refresher ID by Martin 05/04/2018//////
                ClientParams _client = new ClientParams();
                _client.BranchID = clientList.First().BranchID.ToString();
                _client.ClientID = clientList.First().ClientID.ToString();
                LoadRefresherID(_client);
                LoadDOSRI(_client);
                /////////end //////////////////////////////////////


                BeneficiariesButton.IsEnabled = true;
                ITFAccountButton.IsEnabled = true;
                btn_new.IsEnabled = false;
            }
        }

        public void loadNotification()
        {
            ClientSearch o2 = new ClientSearch();
            o2.ClientID = displayClientID;
            o2.BatchNum = displayBranchID;
            String parsed = new JavaScriptSerializer().Serialize(o2);

            CRUXLib.Response Response = App.CRUXAPI.request("client/notification", parsed);

            if (Response.Status == "SUCCESS")
            {
                loadNotifications = new JavaScriptSerializer().Deserialize<ObservableCollection<Notification>>(Response.Content);

                dataGridNotification.ItemsSource = loadNotifications;
            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void SetBatchNo(String batchNO)
        {
            batchNoText.Text = batchNO;
        }

        public void SetPrincipalID(Int64 principalID)
        {
            principalText.Text = principalID.ToString();
        }

        public void SetSpouseCompanyIdName(Int64 companyID, String companyName)
        {
            spouseCompanyIDText.Text = companyID.ToString();
            spouseCompanyNameText.Text = companyName;
        }

        public void SetCompanyIdName(Int64 companyID, String companyName)
        {
            companyEmpCodeText.Text = companyID.ToString();
            companyEmpNameText.Text = companyName;
        }

        public void Populate()
        {
            loadCities();
            loadProvinces();
            loadCountry();
            loadCivilStatus();
            loadGender();
            loadTitle();
            loadBloodType();
            loadAccountType();
            loadClientType();
            loadClientStatus();
            loadReligion();
            loadEducation();
            loadNationality();
            loadYear();
            loadHouseOwnership();
            loadSector();
            loadPosition();
            loadOccupationStatus();
            loadBankAccoutType();
            loadAssetType();
            loadIncomeType();
            loadReasons(); //added - 12-1-2016
            loadRestrictions();
            loadDosRi();
            LoadSuffix();
            loadBranches();
            loadNotificationStatus();
            loadAlertLevel();
            loadBanks();
            loadSchools();
            loadSections();
            List<InsertClientContact> contactTypes = new List<InsertClientContact>();
            DG_ContactType.ItemsSource = contactTypes;
            loadContactType();

            List<InsertClientID> clientprimaryids = new List<InsertClientID>();
            DG_PrimaryID.ItemsSource = clientprimaryids;
            loadIDPrimary();

            List<InsertClientOtherID> clientOthersIDs = new List<InsertClientOtherID>();

            DG_OtherID.ItemsSource = clientOthersIDs;
            loadIDSecondary();

            List<InsertClientAssets> clientAssets = new List<InsertClientAssets>();
            DG_Asset.ItemsSource = clientAssets;

            List<InsertClientDependent> clientDependents = new List<InsertClientDependent>();
            DG_Dependent.ItemsSource = clientDependents;
            setDate();
            dateOpenedPicker.SelectedDate = system.SystemDate;
            List<InsertClientBank> clientBanks = new List<InsertClientBank>();
            DG_Bank.ItemsSource = clientBanks;

            //added by Martin : 07-05-2018
            loadDosriAcctTypes();
            insertDOSRIs = new List<InsertDOSRI>();
            DefaultBranchID();

            btn_find.Focus();
        }

        private void DefaultBranchID()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("client/selectBranch", "");
            if (Response.Status == "SUCCESS")
            {
                defaultBranchID = new JavaScriptSerializer().Deserialize<Int64>(Response.Content);
            }
        }

        public void loadDosriAcctTypes()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("client/DOSRIAcctTypes", "");
            if (Response.Status == "SUCCESS")
            {
                dosriAcctTypes = new List<DosRiAcctType>();

                dosriAcctTypes = new JavaScriptSerializer().Deserialize<List<DosRiAcctType>>(Response.Content);

            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        public void LoadSuffix()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("client/suffix", "");
            if (Response.Status == "SUCCESS")
            {
                List<Suffix> listSuffix = new JavaScriptSerializer().Deserialize<List<Suffix>>(Response.Content);
                suffixComboBox.ItemsSource = listSuffix;
                suffixSpouseComboBox.ItemsSource = listSuffix;
            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void loadBanks()
        {

            CRUXLib.Response Response = App.CRUXAPI.request("frontoffice/bankcode", "");

            if (Response.Status == "SUCCESS")
            {
                List<BankCodeClass> listBanks = new JavaScriptSerializer().Deserialize<List<BankCodeClass>>(Response.Content);
                BankNameText.ItemsSource = listBanks;
            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void loadBranches()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("client/branches", "");
            if (Response.Status == "SUCCESS")
            {
                List<Branches> listSuffix = new JavaScriptSerializer().Deserialize<List<Branches>>(Response.Content);
                branchDestCombo.ItemsSource = listSuffix;
            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void loadReasons()
        {
            try
            {
                List<Reasons> listReasons = new JavaScriptSerializer().ConvertToType<List<Reasons>>(App.CRUXAPI.Attributes("Reason"));

                reasonsComboBox.ItemsSource = listReasons;
            }
            catch (Exception ex)
            {

            }
        }

        public void loadRestrictions()
        {
            try
            {
                List<Restrictions> listRestrictions = new JavaScriptSerializer().ConvertToType<List<Restrictions>>(App.CRUXAPI.Attributes("Restriction"));
                restrictionsComboBox.ItemsSource = listRestrictions;
            }
            catch (Exception ex)
            {

            }
        }

        public void loadDosRi()
        {
            try
            {
                List<DosRi> listDosRis = new JavaScriptSerializer().ConvertToType<List<DosRi>>(App.CRUXAPI.Attributes("DosRi"));
                dosRiComboBox.ItemsSource = listDosRis;

                dosRis = new List<DosRi>();
                dosRis = listDosRis;
            }
            catch (Exception ex)
            { }
        }

        public void loadCities()
        {
            try
            {
                listCities = new JavaScriptSerializer().ConvertToType<List<Cities>>(App.CRUXAPI.Attributes("Cities"));
            }
            catch (Exception ex)
            {

            }
        }

        public void loadProvinces()
        {
            try
            {
                listSProvinces = new JavaScriptSerializer().ConvertToType<List<Provinces>>(App.CRUXAPI.Attributes("Provinces", null, "Province"));
            }
            catch (Exception ex)
            {

            }
            //provincesCombo.ItemsSource = listSProvinces;
            //addressProvinceCombo.ItemsSource = listSProvinces;
            //homeProvinceCombo.ItemsSource = listSProvinces;
            //motherProvinceCombo.ItemsSource = listSProvinces;
            //fatherProvinceCombo.ItemsSource = listSProvinces;
            //companyProvinceCombo.ItemsSource = listSProvinces;
            //selfEmployedProvinceCombo.ItemsSource = listSProvinces;
        }

        public void loadCountry()
        {

            try
            {
                List<Countries> listCountry = new JavaScriptSerializer().ConvertToType<List<Countries>>(App.CRUXAPI.Attributes("Countries"));

                countriesCombo.ItemsSource = listCountry;
                addressCountryCombo.ItemsSource = listCountry;
                homeCountryCombo.ItemsSource = listCountry;
                motherCountryCombo.ItemsSource = listCountry;
                fatherCountryCombo.ItemsSource = listCountry;
                companyCountryCombo.ItemsSource = listCountry;
                selfEmployedCountryCombo.ItemsSource = listCountry;

                countriesCombo.SelectedIndex = 141;
                addressCountryCombo.SelectedIndex = 141;
                homeCountryCombo.SelectedIndex = 141;
                motherCountryCombo.SelectedIndex = 141;
                fatherCountryCombo.SelectedIndex = 141;
                companyCountryCombo.SelectedIndex = 141;
                selfEmployedCountryCombo.SelectedIndex = 141;

                if (option)
                {
                    loadProvince();
                }
                else
                {

                }

            }
            catch (Exception ex)
            {

            }
        }

        public void loadProvince()
        {
            var selectedCountry = countriesCombo.SelectedValue;
            Countries o2 = new Countries();
            o2.CountryID = Convert.ToInt64(selectedCountry);

            String parsed = new JavaScriptSerializer().Serialize(o2);

            var provinces = listSProvinces.FindAll(p => p.CountryID == o2.CountryID).ToList();
            if (!(provinces == null))
            {
                provincesCombo.ItemsSource = provinces;
                addressProvinceCombo.ItemsSource = provinces;
                homeProvinceCombo.ItemsSource = provinces;
                motherProvinceCombo.ItemsSource = provinces;
                fatherProvinceCombo.ItemsSource = provinces;
                companyProvinceCombo.ItemsSource = provinces;
                selfEmployedProvinceCombo.ItemsSource = provinces;

                //combobo.ItemsSource = provinces;
            }
            else
            {
                MessageBox.Show("Failed", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        public void loadCivilStatus()
        {
            try
            {
                List<CivilStatuses> listCivilStatus = new JavaScriptSerializer().ConvertToType<List<CivilStatuses>>(App.CRUXAPI.Attributes("CivilStatus"));
                civilStatusCombo.ItemsSource = listCivilStatus;
            }
            catch (Exception ex)
            {

            }
        }

        public void loadGender()
        {
            try
            {
                List<Genders> listGender = new JavaScriptSerializer().ConvertToType<List<Genders>>(App.CRUXAPI.Attributes("Gender"));
                genderCombo.ItemsSource = listGender;
            }
            catch (Exception ex)
            {

            }
            //CRUXLib.Response Response = App.CRUXAPI.request("client/gender", "");
            //if (Response.Status == "SUCCESS")
            //{
            //    List<Gender> listGender = new JavaScriptSerializer().Deserialize<List<Gender>>(Response.Content);
            //    Console.WriteLine(Response.Content);

            //    genderCombo.ItemsSource = listGender;
            //}
            //else
            //{
            //    MessageBox.Show(Response.Content);
            //}

        }

        public void loadTitle()
        {
            //List<Titles> listTitle = new JavaScriptSerializer().ConvertToType<List<Titles>>(App.CRUXAPI.Attributes("Title"));
            CRUXLib.Response Response = App.CRUXAPI.request("client/title", "");
            if (Response.Status == "SUCCESS")
            {
                List<Titles> listTitle = new JavaScriptSerializer().Deserialize<List<Titles>>(Response.Content);
                titleCombo.ItemsSource = listTitle;
            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        public void loadBloodType()
        {
            try
            {
                List<BloodTypes> listBloodType = new JavaScriptSerializer().ConvertToType<List<BloodTypes>>(App.CRUXAPI.Attributes("BloodType"));
                bloodtypeCombo.ItemsSource = listBloodType;
            }
            catch (Exception ex)
            {

            }
            //CRUXLib.Response Response = App.CRUXAPI.request("client/bloodType", "");
            //if (Response.Status == "SUCCESS")
            //{
            //    List<BloodType> listBloodType = new JavaScriptSerializer().Deserialize<List<BloodType>>(Response.Content);
            //    Console.WriteLine(Response.Content);

            //    bloodtypeCombo.ItemsSource = listBloodType;
            //}
            //else
            //{
            //    MessageBox.Show(Response.Content);
            //}

        }

        public void loadClientStatus()
        {
            try
            {
                List<ClientStatuses> listClientStatus = new JavaScriptSerializer().ConvertToType<List<ClientStatuses>>(App.CRUXAPI.Attributes("ClientStatus"));
                clientStatusCombo.ItemsSource = listClientStatus;
            }
            catch (Exception ex)
            {

            }
            //CRUXLib.Response Response = App.CRUXAPI.request("client/clientstatus", "");
            //if (Response.Status == "SUCCESS")
            //{
            //    List<ClientStatus> listClientStatus = new JavaScriptSerializer().Deserialize<List<ClientStatus>>(Response.Content);
            //    Console.WriteLine(Response.Content);
            //    clientStatusCombo.ItemsSource = listClientStatus;
            //}
            //else
            //{
            //    MessageBox.Show(Response.Content);
            //}

        }

        public void loadReligion()
        {
            try
            {


                List<Religions> listReligion = new JavaScriptSerializer().ConvertToType<List<Religions>>(App.CRUXAPI.Attributes("Religion", null, "Religion"));
                religionCombo.ItemsSource = listReligion;
            }
            catch (Exception ex)
            {

            }
        }

        public void loadAccountType()
        {
            try
            {
                List<AccountTypes> accountTypeList = new JavaScriptSerializer().ConvertToType<List<AccountTypes>>(App.CRUXAPI.Attributes("AccountType"));
                accountTypeCombo.ItemsSource = accountTypeList;
            }
            catch (Exception ex)
            {

            }
            //CRUXLib.Response Response = App.CRUXAPI.request("client/accounttype", "");
            //if (Response.Status == "SUCCESS")
            //{
            //    List<AccountType> accountTypeList = new JavaScriptSerializer().Deserialize<List<AccountType>>(Response.Content);
            //    Console.WriteLine(Response.Content);

            //    accountTypeCombo.ItemsSource = accountTypeList;
            //}
            //else
            //{
            //    MessageBox.Show(Response.Content);
            //}

        }

        public void loadClientType()
        {
            try
            {

                List<ClientTypes> clientTypeList = new JavaScriptSerializer().ConvertToType<List<ClientTypes>>(App.CRUXAPI.Attributes("ClientType"));
                clientTypeCombo.ItemsSource = clientTypeList;
            }
            catch (Exception ex)
            {

            }
            //CRUXLib.Response Response = App.CRUXAPI.request("client/clienttype", "");
            //if (Response.Status == "SUCCESS")
            //{
            //    List<ClientType> clientTypeList = new JavaScriptSerializer().Deserialize<List<ClientType>>(Response.Content);
            //    Console.WriteLine(Response.Content);

            //    clientTypeCombo.ItemsSource = clientTypeList;
            //}
            //else
            //{
            //    MessageBox.Show(Response.Content);
            //}

        }

        public void loadEducation()
        {
            try
            {
                List<Education> educationList = new JavaScriptSerializer().ConvertToType<List<Education>>(App.CRUXAPI.Attributes("EducationalAttainment"));
                highestEducationCombo.ItemsSource = educationList;
            }
            catch (Exception ex)
            {

            }
        }

        public void loadNationality()
        {
            try
            {
                List<Nationalities> nationalityList = new JavaScriptSerializer().ConvertToType<List<Nationalities>>(App.CRUXAPI.Attributes("Nationality"));
                nationalityCombo.ItemsSource = nationalityList;
                nationalityCombo.SelectedValue = 66;

                try
                {
                    client.Nationality = Convert.ToInt32(nationalityList.FirstOrDefault(c => c.NationalityID == 66).NationalityID);
                }
                catch (Exception ex)
                {

                }
                //CRUXLib.Response Response = App.CRUXAPI.request("client/nationality", "");
                //if (Response.Status == "SUCCESS")
                //{
                //    List<Nationality> nationalityList = new JavaScriptSerializer().Deserialize<List<Nationality>>(Response.Content);
                //    Console.WriteLine(Response.Content);

                //    nationalityCombo.ItemsSource = nationalityList;
                //    client.Nationality = Convert.ToInt32(nationalityList.FirstOrDefault(c => c.NationalityID == 66).NationalityID);
                //}
                //else
                //{
                //    MessageBox.Show(Response.Content);
                //}
            }
            catch (Exception ex)
            {

            }
        }

        public void loadHouseOwnership()
        {
            try
            {
                List<HouseOwnerTypes> houseOwnerTypeList = new JavaScriptSerializer().ConvertToType<List<HouseOwnerTypes>>(App.CRUXAPI.Attributes("HouseOwnerType"));

                addressOwnershipCombo.ItemsSource = houseOwnerTypeList;
                homeOwnershipCombo.ItemsSource = houseOwnerTypeList;
            }
            catch (Exception ex)
            {

            }
        }

        public void loadYear()
        {
            var currentYear = DateTime.Now.Year;
            for (int year = 1950; year <= currentYear; year++)
            {
                addressSinceDateCombo.Items.Add(year);
                homeSinceDateCombo.Items.Add(year);
                motherSinceYearCombo.Items.Add(year);
                fatherSinceYearCombo.Items.Add(year);
            }
        }

        public void loadContactType()
        {
            try
            {
                List<ContactTypes> contactTypeList = new JavaScriptSerializer().ConvertToType<List<ContactTypes>>(App.CRUXAPI.Attributes("ContactType"));
                contactTypeDataGridComboBox.ItemsSource = contactTypeList;
            }
            catch (Exception ex)
            {

            }
        }

        public void loadIDPrimary()
        {
            try
            {
                List<IDPrimary> IDPrimaryList = new JavaScriptSerializer().ConvertToType<List<IDPrimary>>(App.CRUXAPI.Attributes("IDPrimary"));
                PrimaryIDDataGridComboBox.ItemsSource = IDPrimaryList;
            }
            catch (Exception ex)
            {

            }
        }

        public void loadIDSecondary()
        {
            try
            {
                List<IDSecondary> IDSecondaryList = new JavaScriptSerializer().ConvertToType<List<IDSecondary>>(App.CRUXAPI.Attributes("IDSecondary"));
                dataGridSecondaryID.ItemsSource = IDSecondaryList;
            }
            catch (Exception ex)
            {

            }
            //CRUXLib.Response Response = App.CRUXAPI.request("client/secondaryIDType", "");
            //if (Response.Status == "SUCCESS")
            //{
            //    List<IDSecondary> IDSecondaryList = new JavaScriptSerializer().Deserialize<List<IDSecondary>>(Response.Content);
            //    Console.WriteLine(Response.Content);

            //    dataGridSecondaryID.ItemsSource = IDSecondaryList;
            //}
            //else
            //{
            //    MessageBox.Show(Response.Content);
            //}

        }

        public void loadPosition()
        {
            try
            {
                List<Position> positionList = new JavaScriptSerializer().ConvertToType<List<Position>>(App.CRUXAPI.Attributes("StandardOccupation"));
                companyPositionCombo.ItemsSource = positionList;
            }
            catch (Exception ex)
            {

            }
        }

        public void loadOccupationStatus()
        {
            //List<OccupationStatuses> occupationStatusList = new JavaScriptSerializer().ConvertToType<List<OccupationStatuses>>(App.CRUXAPI.Attributes("OccupationStatus"));
            //companyStatusCombo.ItemsSource = occupationStatusList;


            CRUXLib.Response Response = App.CRUXAPI.request("client/occupationStatus", "");
            if (Response.Status == "SUCCESS")
            {
                List<OccupationStatuses> occupationStatusList = new JavaScriptSerializer().Deserialize<List<OccupationStatuses>>(Response.Content);
                companyStatusCombo.ItemsSource = occupationStatusList;

            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        public void loadSchools()
        {
            //List<OccupationStatuses> occupationStatusList = new JavaScriptSerializer().ConvertToType<List<OccupationStatuses>>(App.CRUXAPI.Attributes("OccupationStatus"));
            //companyStatusCombo.ItemsSource = occupationStatusList;


            CRUXLib.Response Response = App.CRUXAPI.request("client/school", "");
            if (Response.Status == "SUCCESS")
            {
                List<Schools> schoolsList = new JavaScriptSerializer().Deserialize<List<Schools>>(Response.Content);
                CB_Schools.ItemsSource = schoolsList;

            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        public void loadSections()
        {
            //List<OccupationStatuses> occupationStatusList = new JavaScriptSerializer().ConvertToType<List<OccupationStatuses>>(App.CRUXAPI.Attributes("OccupationStatus"));
            //companyStatusCombo.ItemsSource = occupationStatusList;


            CRUXLib.Response Response = App.CRUXAPI.request("client/sections", "");
            if (Response.Status == "SUCCESS")
            {
                List<Sections> schoolsList = new JavaScriptSerializer().Deserialize<List<Sections>>(Response.Content);
                CB_Sections.ItemsSource = schoolsList;

            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        public void loadBankAccoutType()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("client/bankAccountType", "");
            if (Response.Status == "SUCCESS")
            {
                List<BankAccountType> bankAccountTypeList = new JavaScriptSerializer().Deserialize<List<BankAccountType>>(Response.Content);
                accountTypeDataGridComboBox.ItemsSource = bankAccountTypeList;

            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        public void loadAssetType()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("client/assetType", "");
            if (Response.Status == "SUCCESS")
            {
                List<AssetType> AssetTypeList = new JavaScriptSerializer().Deserialize<List<AssetType>>(Response.Content);

                assetTypeDataGridComboBox.ItemsSource = AssetTypeList;

            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        public void loadNotificationStatus()
        {

            CRUXLib.Response Response = App.CRUXAPI.request("client/notificationStatus", "");
            if (Response.Status == "SUCCESS")
            {
                List<notificationStatus> NotificationStatus = new JavaScriptSerializer().Deserialize<List<notificationStatus>>(Response.Content);

                notificationCombo.ItemsSource = NotificationStatus;

            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void loadIncomeType()
        {

            CRUXLib.Response Response = App.CRUXAPI.request("client/incomeType", "");
            if (Response.Status == "SUCCESS")
            {
                List<IncomeType> IncomeTypeList = new JavaScriptSerializer().Deserialize<List<IncomeType>>(Response.Content);

                salaryIcomeCombo.ItemsSource = IncomeTypeList;
                businessIncomeCombo.ItemsSource = IncomeTypeList;
                spouseSalaryCombo.ItemsSource = IncomeTypeList;
                spouseBusinessIncomeCombo.ItemsSource = IncomeTypeList;
                otherIncomeCombo.ItemsSource = IncomeTypeList;
            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void loadSector()
        {
            try
            {
                List<Sectors> sectorList = new JavaScriptSerializer().ConvertToType<List<Sectors>>(App.CRUXAPI.Attributes("Sector"));

                companySectorCombo.ItemsSource = sectorList;
                selfEmployedSector.ItemsSource = sectorList;
                selfEmployedLineBusiness.ItemsSource = sectorList;
            }
            catch (Exception ex) { }
            //CRUXLib.Response Response = App.CRUXAPI.request("client/sector", "");
            //if (Response.Status == "SUCCESS")
            //{
            //    List<Sector> sectorList = new JavaScriptSerializer().Deserialize<List<Sector>>(Response.Content);
            //    Console.WriteLine(Response.Content);

            //    companySectorCombo.ItemsSource = sectorList;
            //    selfEmployedSector.ItemsSource = sectorList;
            //    selfEmployedLineBusiness.ItemsSource = sectorList;
            //}
            //else
            //{
            //    MessageBox.Show(Response.Content);
            //}

        }

        public void loadSubSector(Object selectedSector)
        {
            Sectors o2 = new Sectors();
            o2.SectorID = Convert.ToInt64(selectedSector);

            String parsed = new JavaScriptSerializer().Serialize(o2);

            try
            {
                List<SubSectors> listSubSectors = new JavaScriptSerializer().ConvertToType<List<SubSectors>>(App.CRUXAPI.Attributes("SubSector"));
                selfEmployedSubSector.ItemsSource = listSubSectors;
            }
            catch (Exception ex)
            { }
        }

        public void loadAlertLevel()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("client/loadAlertLevel", "");
            if (Response.Status == "SUCCESS")
            {
                List<AlertLevel> AlertLevel = new JavaScriptSerializer().Deserialize<List<AlertLevel>>(Response.Content);

                cmb_AlertLevel.ItemsSource = AlertLevel;

            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btn_find_Click(object sender, RoutedEventArgs e)
        {
            Disable();
            var members_find = new Members_Find(2, this);
            members_find.Owner = this;
            members_find.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Members_Documents members_list = new Members_Documents();
            members_list.Owner = this;
            members_list.ShowDialog();
        }

        private void provinceComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Object selectedProvince = new Object();

            if (provincesCombo.Focus())
            {
                provinceChange = 1;
                selectedProvince = provincesCombo.SelectedValue;
            }
            else if (addressProvinceCombo.Focus())
            {
                provinceChange = 2;
                selectedProvince = addressProvinceCombo.SelectedValue;
            }
            else if (homeProvinceCombo.Focus())
            {
                provinceChange = 3;
                selectedProvince = homeProvinceCombo.SelectedValue;
            }
            else if (motherProvinceCombo.Focus())
            {
                provinceChange = 4;
                selectedProvince = motherProvinceCombo.SelectedValue;

            }
            else if (fatherProvinceCombo.Focus())
            {
                provinceChange = 5;
                selectedProvince = fatherProvinceCombo.SelectedValue;
            }
            else if (companyProvinceCombo.Focus())
            {
                provinceChange = 6;
                selectedProvince = companyProvinceCombo.SelectedValue;
            }
            else if (selfEmployedProvinceCombo.Focus())
            {
                provinceChange = 7;
                selectedProvince = selfEmployedProvinceCombo.SelectedValue;
            }
            string sp = "";
            if (selectedProvince != null)
            {
                sp = selectedProvince.ToString();
            }


            if (sp != "System.Object")
            {
                loadCity(selectedProvince);
            }
            else
            {
                string name = (sender as ComboBox).Name.ToString();

                if (provincesCombo.Name == name)
                {
                    loadSelectedCity(provinceAndCityComboIDs.ProvinceID, name);
                    // checkComboBoxProvince(provincesCombo.SelectedValue, provinceAndCityComboIDs.ProvinceID, name);
                }

                else if (addressProvinceCombo.Name == name)
                {
                    loadSelectedCity(provinceAndCityComboIDs.PProvinceID, name);
                    //checkComboBoxProvince(addressProvinceCombo.SelectedValue, provinceAndCityComboIDs.PProvinceID, name);
                }

                else if (homeProvinceCombo.Name == name)
                {
                    loadSelectedCity(provinceAndCityComboIDs.HomeProvinceID, name);
                    // checkComboBoxProvince(homeProvinceCombo.SelectedValue, provinceAndCityComboIDs.HomeProvinceID, name);
                }

                else if (motherProvinceCombo.Name == name)
                {
                    loadSelectedCity(provinceAndCityComboIDs.MotherProvinceID, name);
                    //  checkComboBoxProvince(motherProvinceCombo.SelectedValue, provinceAndCityComboIDs.MotherProvinceID, name);
                }

                else if (fatherProvinceCombo.Name == name)
                {
                    loadSelectedCity(provinceAndCityComboIDs.FatherProvinceID, name);
                    //  checkComboBoxProvince(fatherProvinceCombo.SelectedValue, provinceAndCityComboIDs.FatherProvinceID, name);
                }

                else if (companyProvinceCombo.Name == name)
                {
                    loadSelectedCity(provinceAndCityComboIDs.EmpProvinceID, name);
                    //   checkComboBoxProvince(companyProvinceCombo.SelectedValue, provinceAndCityComboIDs.EmpProvinceID, name);
                }

                else if (selfEmployedProvinceCombo.Name == name)
                {
                    loadSelectedCity(provinceAndCityComboIDs.EmpSelfProvinceID, name);
                    //  checkComboBoxProvince(selfEmployedProvinceCombo.SelectedValue, provinceAndCityComboIDs.EmpSelfProvinceID, name);
                }
            }

        }

        public void checkComboBoxProvince(Object provinceID, Int64 provinceSelectedID, String name)
        {
            if (provinceID == null)
            {
                loadSelectedCity(provinceSelectedID, name);
            }
            else
            {
                if (Convert.ToInt64(provinceID) != provinceSelectedID)
                {
                    loadSelectedCity(provinceSelectedID, name);
                }
                else
                    loadSelectedCity(provinceID, name);
            }
        }

        public void loadSelectedCity(Object selectedProvince, String comboName)
        {
            Provinces o2 = new Provinces();
            o2.ProvinceID = Convert.ToInt64(selectedProvince);

            var cities = listCities.FindAll(c => c.ProvinceID == o2.ProvinceID).ToList();

            if (cities != null)
            {
                if (provincesCombo.Name == comboName)
                {
                    cityCombo.ItemsSource = cities;
                    if (getbarangay.Count > 0)
                    {
                        cityCombo.SelectedValue = getbarangay.FirstOrDefault().MunicipalityBirthID;
                    }

                }

                else if (addressProvinceCombo.Name == comboName)
                {
                    addressCityCombo.ItemsSource = cities;
                    if (getbarangay.Count > 0)
                    {
                        addressCityCombo.SelectedValue = getbarangay.FirstOrDefault().PMunicipalityID;
                    }
                }

                else if (homeProvinceCombo.Name == comboName)
                {
                    homeCityCombo.ItemsSource = cities;
                    if (getbarangay.Count > 0)
                    {
                        homeCityCombo.SelectedValue = getbarangay.FirstOrDefault().HomeMunicipalityID;
                    }
                }

                else if (motherProvinceCombo.Name == comboName)
                {
                    motherCityCombo.ItemsSource = cities;
                }

                else if (fatherProvinceCombo.Name == comboName)
                {
                    fatherCityCombo.ItemsSource = cities;
                }

                else if (companyProvinceCombo.Name == comboName)
                {
                    companyCityCombo.ItemsSource = cities;
                    if (getbarangay.Count > 0)
                    {
                        companyCityCombo.SelectedValue = getbarangay.FirstOrDefault().EmpMunicipalityID;
                    }
                }

                else if (selfEmployedProvinceCombo.Name == comboName)
                {
                    selfEmployedCityCombo.ItemsSource = cities;
                }
            }

            else
            {
                MessageBox.Show("Failed to load Cities", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void loadCity(Object selectedProvince)
        {
            Provinces o2 = new Provinces();
            o2.ProvinceID = Convert.ToInt64(selectedProvince);// (Provinces)

            var cities = listCities.FindAll(c => c.ProvinceID == o2.ProvinceID).ToList();

            if (cities != null)
            {
                if (provinceChange == 1)
                {
                    cityCombo.ItemsSource = cities;

                }

                else if (provinceChange == 2)
                {
                    // addressBrgyCombo.ItemsSource = null;
                    addressCityCombo.ItemsSource = cities;

                }

                else if (provinceChange == 3)
                {
                    homeCityCombo.ItemsSource = cities;

                }

                else if (provinceChange == 4)
                {
                    motherCityCombo.ItemsSource = cities;
                }

                else if (provinceChange == 5)
                {
                    fatherCityCombo.ItemsSource = cities;
                }

                else if (provinceChange == 6)
                {
                    companyCityCombo.ItemsSource = cities;
                }

                else if (provinceChange == 7)
                {
                    selfEmployedCityCombo.ItemsSource = cities;
                }
            }

            else
            {
                MessageBox.Show("Failed to load Cities", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cityComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Object selectedCity = new Object();

            if (addressCityCombo.Focus())
            {
                cityChange = 2;
                selectedCity = addressCityCombo.SelectedValue;
            }
            else if (homeCityCombo.Focus())
            {
                cityChange = 3;
                selectedCity = homeCityCombo.SelectedValue;
            }
            else if (motherCityCombo.Focus())
            {
                cityChange = 4;
                selectedCity = motherCityCombo.SelectedValue;
            }
            else if (fatherCountryCombo.Focus())
            {
                cityChange = 5;
                selectedCity = fatherCityCombo.SelectedValue;

            }
            else if (companyCityCombo.Focus())
            {
                cityChange = 6;
                selectedCity = companyCityCombo.SelectedValue;
            }
            else if (selfEmployedCityCombo.Focus())
            {
                cityChange = 7;
                selectedCity = selfEmployedCityCombo.SelectedValue;
            }

            try
            {
                string sCity = selectedCity.ToString();
                if (sCity != "System.Object")
                {
                    loadBarangay(selectedCity);
                    loadPostalCode(selectedCity);
                }

                else
                {
                    string name = (sender as ComboBox).Name.ToString();

                    if (addressCityCombo.Name == name)
                    {
                        loadSelectedBarangay(provinceAndCityComboIDs.CityID, name);
                        // checkComboBoxCity(addressCityCombo.SelectedValue, provinceAndCityComboIDs.CityID, name);
                    }
                    else if (homeCityCombo.Name == name)
                    {
                        loadSelectedBarangay(provinceAndCityComboIDs.HomeCityID, name);
                        // checkComboBoxCity(homeCityCombo.SelectedValue, provinceAndCityComboIDs.HomeCityID, name);
                    }
                    else if (motherCityCombo.Name == name)
                    {
                        loadSelectedBarangay(provinceAndCityComboIDs.MotherCityID, name);
                        //checkComboBoxCity(motherCityCombo.SelectedValue, provinceAndCityComboIDs.MotherCityID, name);
                    }
                    else if (fatherCityCombo.Name == name)
                    {
                        loadSelectedBarangay(provinceAndCityComboIDs.FatherCityID, name);
                        // checkComboBoxCity(fatherCityCombo.SelectedValue, provinceAndCityComboIDs.FatherCityID, name);
                    }
                    else if (companyCityCombo.Name == name)
                    {
                        loadSelectedBarangay(provinceAndCityComboIDs.EmpCityID, name);
                        //checkComboBoxCity(companyCityCombo.SelectedValue, provinceAndCityComboIDs.EmpCityID, name);
                    }
                    else if (selfEmployedCityCombo.Name == name)
                    {
                        loadSelectedBarangay(provinceAndCityComboIDs.EmpSelfCityID, name);
                        //loadSelectedBarangay(provinceAndCityComboIDs.EmpSelfCityID, name);
                        //checkComboBoxCity(selfEmployedCityCombo.SelectedValue, provinceAndCityComboIDs.EmpSelfCityID, name);
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        public void checkComboBoxCity(Object cityID, Int64 citySelectedID, String name)
        {
            if (cityID == null)
            {
                loadSelectedBarangay(citySelectedID, name);
            }
            else
            {
                loadSelectedBarangay(cityID, name);
            }
        }

        public void loadSelectedBarangay(Object selectedCity, String comboName)
        {

            Cities o2 = new Cities();
            o2.CityID = Convert.ToInt64(selectedCity);

            String parsed = new JavaScriptSerializer().Serialize(o2.CityID);

            CRUXLib.Response Response = App.CRUXAPI.request("client/barangay", parsed);
            if (Response.Status == "SUCCESS")
            {
                List<Barangay> listBarangay = new JavaScriptSerializer().Deserialize<List<Barangay>>(Response.Content);


                if (addressCityCombo.Name == comboName)
                {
                    addressBrgyCombo.ItemsSource = listBarangay;
                    if (getbarangay.Count > 0)
                    {
                        addressBrgyCombo.SelectedValue = getbarangay.FirstOrDefault().PBarangayID;
                    }

                }
                else if (homeCityCombo.Name == comboName)
                {
                    homeBrgyCombo.ItemsSource = listBarangay;
                    if (getbarangay.Count > 0)
                    {
                        homeBrgyCombo.SelectedValue = getbarangay.FirstOrDefault().HomeBarangayID;
                    }
                }
                else if (motherCityCombo.Name == comboName)
                {
                    motherBrgyCombo.ItemsSource = listBarangay;
                    if (getbarangay.Count > 0)
                    {
                        motherBrgyCombo.SelectedValue = getbarangay.FirstOrDefault().MotherBarangayID;
                    }
                }
                else if (fatherCityCombo.Name == comboName)
                {
                    fatherBrgyCombo.ItemsSource = listBarangay;
                }
                else if (companyCityCombo.Name == comboName)
                {
                    companyBrgyCombo.ItemsSource = listBarangay;
                    if (getbarangay.Count > 0)
                    {
                        companyBrgyCombo.SelectedValue = getbarangay.FirstOrDefault().EmpBarangayID;
                    }
                }
                else if (selfEmployedCityCombo.Name == comboName)
                {
                    selfEmployedBrgyCombo.ItemsSource = listBarangay;
                }
            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void loadBarangay(Object selectedCity)
        {
            Cities o2 = new Cities();
            o2.CityID = Convert.ToInt64(selectedCity);

            String parsed = new JavaScriptSerializer().Serialize(o2.CityID);

            CRUXLib.Response Response = App.CRUXAPI.request("client/barangay", parsed);
            if (Response.Status == "SUCCESS")
            {
                List<Barangay> listBarangay = new JavaScriptSerializer().Deserialize<List<Barangay>>(Response.Content);


                if (cityChange == 2)
                {
                    addressBrgyCombo.ItemsSource = listBarangay;
                }
                else if (cityChange == 3)
                {
                    homeBrgyCombo.ItemsSource = listBarangay;
                }
                else if (cityChange == 4)
                {
                    motherBrgyCombo.ItemsSource = listBarangay;
                }
                else if (cityChange == 5)
                {
                    fatherBrgyCombo.ItemsSource = listBarangay;
                }
                else if (cityChange == 6)
                {
                    companyBrgyCombo.ItemsSource = listBarangay;
                }

                else if (cityChange == 7)
                {
                    selfEmployedBrgyCombo.ItemsSource = listBarangay;
                }

            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        public void loadPostalCode(Object selectedCity)
        {
            Cities o2 = new Cities();
            o2.CityID = Convert.ToInt64(selectedCity);

            String parsed = new JavaScriptSerializer().Serialize(o2.CityID);

            CRUXLib.Response Response = App.CRUXAPI.request("client/zipcode", parsed);
            if (Response.Status == "SUCCESS")
            {
                List<Cities> listZipcode = new JavaScriptSerializer().Deserialize<List<Cities>>(Response.Content);

                if (cityChange == 2)
                {
                    addressPostalCodeText.Text = listZipcode.First().ZipCode.ToString();
                }
                else if (cityChange == 3)
                {
                    homePostalCodeText.Text = listZipcode.First().ZipCode.ToString();
                }
                else if (cityChange == 4)
                {
                    motherPostalCodeText.Text = listZipcode.First().ZipCode.ToString();
                }
                else if (cityChange == 5)
                {
                    fatherPostalCodeText.Text = listZipcode.First().ZipCode.ToString();
                }
                else if (cityChange == 6)
                {
                    companyPostalCodeText.Text = listZipcode.First().ZipCode.ToString();
                }

                else if (cityChange == 7)
                {
                    selfEmployedPostalCodeText.Text = listZipcode.First().ZipCode.ToString();
                }

            }
            else
            {
                MessageBox.Show(Response.Content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        public void ImageToBase64()
        {
            string brID = string.Format("{0:D2}", getbarangay.FirstOrDefault().BranchID);
            string sourcePath = client.ImageFileName;
            string targetPath = @"\\10.15.40.211\e\Image\";

            string sourceFileName = brID + "_" + getbarangay.FirstOrDefault().ClientID + ".jpg";

            string fileName = sourceFileName;
            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.GetFullPath(sourcePath);
            string destFile = System.IO.Path.Combine(targetPath, fileName);

            // To copy a folder's contents to a new location:
            // Create a new target folder, if necessary.
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }



            // To copy a file to another location and 
            // overwrite the destination file if it already exists.
            stream.Close();
            System.IO.File.Copy(sourceFile, destFile, true);

            // To copy all the files in one directory to another directory.
            // Get the files in the source folder. (To recursively iterate through
            // all subfolders under the current directory, see
            // "How to: Iterate Through a Directory Tree.")
            // Note: Check for target path was performed previously
            //       in this code example.
            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);

                // Copy the files and overwrite destination files if they already exist.
                foreach (string s in files)
                {
                    // Use static Path methods to extract only the file name from the path.
                    fileName = System.IO.Path.GetFileName(s);
                    destFile = System.IO.Path.Combine(targetPath, fileName);
                    System.IO.File.Copy(s, destFile, true);
                }
            }
            else
            {
                MessageBox.Show("Source path does not exist", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);

            }
            return;
        }

        public void SaveSignature()
        {




            string brID = string.Format("{0:D2}", getbarangay.FirstOrDefault().BranchID);
            string sourcePath = client.ImageFileName;
            if (sourcePath != null)
            {
                string targetPath = @"\\10.15.40.211\e\Signature\";

                string sourceFileName = brID + "_" + getbarangay.FirstOrDefault().ClientID + ".jpg";

                string fileName = sourceFileName;
                // Use Path class to manipulate file and directory paths.
                string sourceFile = System.IO.Path.GetFullPath(sourcePath);
                string destFile = System.IO.Path.Combine(targetPath, fileName);

                // To copy a folder's contents to a new location:
                // Create a new target folder, if necessary.
                if (!System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }

                // To copy a file to another location and 
                // overwrite the destination file if it already exists.
                stream.Close();
                System.IO.File.Copy(sourceFile, destFile, true);

                // To copy all the files in one directory to another directory.
                // Get the files in the source folder. (To recursively iterate through
                // all subfolders under the current directory, see
                // "How to: Iterate Through a Directory Tree.")
                // Note: Check for target path was performed previously
                //       in this code example.
                if (System.IO.Directory.Exists(sourcePath))
                {
                    string[] files = System.IO.Directory.GetFiles(sourcePath);

                    // Copy the files and overwrite destination files if they already exist.
                    foreach (string s in files)
                    {
                        // Use static Path methods to extract only the file name from the path.
                        fileName = System.IO.Path.GetFileName(s);
                        destFile = System.IO.Path.Combine(targetPath, fileName);
                        System.IO.File.Copy(s, destFile, true);
                    }
                }




                else
                {
                    MessageBox.Show("Source path does not exist", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }

            return;
        }
        private Boolean EmploymentVerification()
        {
            Boolean result = false;




            return result;
        }
        private void btn_save_Click(object sender, RoutedEventArgs e)
        {

            btn_save.Focus();
            Unlock();
            if (btn_save.IsEnabled == true)
            {
                if (EmploymentVerification())
                {

                }

                if (titleCombo.SelectedItem == null)
                {
                    MessageBox.Show("Title cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    titleCombo.Focus();
                    return;
                }
                if (String.IsNullOrWhiteSpace(lastNameText.Text) || String.IsNullOrWhiteSpace(firstNameText.Text))
                {
                    MessageBox.Show("Client name cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    if (String.IsNullOrWhiteSpace(firstNameText.Text))
                    {
                        firstNameText.Focus();
                    }
                    else
                    {
                        lastNameText.Focus();
                    }

                    return;
                }
                if (accountTypeCombo.SelectedItem == null)
                {
                    MessageBox.Show("Account Type cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    accountTypeCombo.Focus();
                    return;
                }
                if (clientTypeCombo.SelectedItem == null)
                {
                    MessageBox.Show("Client Type cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    clientTypeCombo.Focus();
                    return;
                }
                var selectedClientStatus = clientStatusCombo.SelectedItem as ClientStatuses;
                if (clientStatusCombo.SelectedItem == null)
                {
                    MessageBox.Show("Client Status cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    clientStatusCombo.Focus();
                    return;
                }
                else if (selectedClientStatus.ClientStatusID == 3)
                {
                    if (reasonsComboBox.SelectedItem == null)
                    {
                        tabControl.SelectedIndex = 11;
                        othersTabControl.SelectedIndex = 1;
                        MessageBox.Show("Please provide reason for closing the account!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                        reasonsComboBox.Focus();

                        return;
                    }
                }
                if (genderCombo.SelectedItem == null)
                {
                    tabControl.SelectedIndex = 0;
                    MessageBox.Show("Gender Status cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    genderCombo.Focus();
                    return;
                }
                if (civilStatusCombo.SelectedItem == null)
                {
                    tabControl.SelectedIndex = 0;
                    MessageBox.Show("Civil Status cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    civilStatusCombo.Focus();
                    return;
                }
                if (provincesCombo.SelectedItem == null)
                {
                    tabControl.SelectedIndex = 0;
                    MessageBox.Show("Province birthplace cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    provincesCombo.Focus();
                    return;
                }
                if (cityCombo.SelectedItem == null)
                {
                    tabControl.SelectedIndex = 0;
                    MessageBox.Show("City birthplace cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    cityCombo.Focus();
                    return;
                }

                if (highestEducationCombo.SelectedItem == null)
                {
                    tabControl.SelectedIndex = 0;
                    MessageBox.Show("Highest Education cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    highestEducationCombo.Focus();
                    return;
                }
                if (spouseLastNameText.Text != "")
                {
                    if (String.IsNullOrWhiteSpace(spouseDateOfBirthPicker.Text))
                    {
                        MessageBox.Show("Spouse Birthdate cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                        spouseDateOfBirthPicker.Focus();
                        return;
                    }
                }
                if (restrictionsComboBox.SelectedItem == null)
                {


                    tabControl.SelectedIndex = 11;
                    othersTabControl.SelectedIndex = 2;
                    MessageBox.Show("Restriction cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    restrictionsComboBox.Focus();
                    return;
                }

                if (String.IsNullOrWhiteSpace(ageText.Text))
                {
                    tabControl.SelectedIndex = 0;
                    MessageBox.Show("Age cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    dateOfBirthPicker.Focus();
                    return;
                }
                DateTime resulta;
                if (!DateTime.TryParseExact(dateOfBirthPicker.Text, "MM/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out resulta))
                {
                    tabControl.SelectedIndex = 0;
                    MessageBox.Show("Birth Date cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    dateOfBirthPicker.Focus();
                    return;
                }

                if (addressProvinceCombo.SelectedItem == null)
                {
                    tabControl.SelectedIndex = 1;
                    MessageBox.Show("Province Address cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    addressProvinceCombo.Focus();
                    return;
                }

                if (addressCityCombo.SelectedItem == null)
                {
                    tabControl.SelectedIndex = 1;
                    MessageBox.Show("City/Municipality Address cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    addressCityCombo.Focus();
                    return;
                }

                if (addressBrgyCombo.SelectedItem == null)
                {
                    tabControl.SelectedIndex = 1;
                    MessageBox.Show("Barangay Address cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    addressBrgyCombo.Focus();
                    return;
                }
                var selectedClientType = clientTypeCombo.SelectedItem as ClientTypes;
                if (selectedClientType.ClientTypeID == 1 || selectedClientType.ClientTypeID == 2 || selectedClientType.ClientTypeID == 5)
                {
                    if (Convert.ToInt64(ageText.Text) < 18)
                    {
                        tabControl.SelectedIndex = 0;
                        MessageBox.Show("Client is below 18 years old!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                        dateOfBirthPicker.Focus();
                        return;
                    }
                    if (String.IsNullOrWhiteSpace(batchNoText.Text))
                    {
                        MessageBox.Show("PMES Batch cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                        batchNoText.Focus();
                        return;
                    }
                    DateTime pmesDate;
                    if (!String.IsNullOrEmpty(dateofPMES.Text))
                    {
                        try
                        {
                            Convert.ToDateTime(dateofPMES.Text).ToString("MM/dd/yyyy");
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("PMES Date cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                            dateofPMES.Focus();
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("PMES Date cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                        dateofPMES.Focus();
                        return;
                    }

                }
                else if (selectedClientType.ClientTypeID == 3)

                {
                    if (String.IsNullOrWhiteSpace(principalText.Text))
                    {
                        MessageBox.Show("Junior Depositor Should have Principal Account!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                        principalButton.Focus();
                        return;
                    }
                }
                else if (selectedClientType.ClientTypeID == 8)

                {
                    if (CB_Schools.SelectedItem == null)
                    {
                        tabControl.SelectedIndex = 11;
                        othersTabControl.SelectedIndex = 3;
                        MessageBox.Show("School cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                        CB_Schools.Focus();
                        return;
                    }
                    if (CB_Sections.SelectedItem == null)
                    {
                        tabControl.SelectedIndex = 11;
                        othersTabControl.SelectedIndex = 3;
                        MessageBox.Show("Section cannot be empty!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                        CB_Sections.Focus();
                        return;
                    }
                }

                if (clientImage.Source != null)
                {

                    var mem = new MemoryStream();
                    if (mem.Length > 0)
                    {
                        // If using .NET 4 or later:
                        stream.CopyTo(mem);

                        // getting the internal buffer (no additional copying)
                        byte[] buffer = mem.GetBuffer();
                        long length = mem.Length; // the actual length of the data 
                                                  // (the array may be longer)

                        // if you need the array to be exactly as long as the data
                        byte[] truncated = mem.ToArray(); // makes another copy



                        var output = new Bitmap(500, 500);
                        var rect = new System.Drawing.Rectangle(0, 0, 500, 500);
                        var bmpData = output.LockBits(rect,
                            ImageLockMode.ReadWrite, output.PixelFormat);
                        var ptr = bmpData.Scan0;
                        Marshal.Copy(truncated, 0, ptr, truncated.Length);
                        output.UnlockBits(bmpData);



                        //var filePath = System.IO.Path.Combine(@"\\D\image\", "yourImageName.jpg");
                        //var streams = new FileStream(filePath, FileMode.Create);


                        //output.save(@"C:/img.jpg", ImageFormat.Jpeg);
                        output.Save(@"\\D\image\img.jpg", ImageFormat.Jpeg);
                        //  streams.Close();
                        // System.Drawing.Image img = System.Drawing.Image.FromFile(@"\\D\Image\");
                        // MemoryStream imgms = new MemoryStream();
                        //img.Save(imgms, ImageFormat.Jpeg);
                        //img = new System.Drawing.Bitmap(imgms);
                        //img.Save(outFile);


                        //string brID = string.Format("{0:D2}", getbarangay.FirstOrDefault().BranchID);
                        //string sourcePath = client.ImageFileName;
                        //string targetPath = @"\\D\Image\";

                        //string sourceFileName = brID + "_" + getbarangay.FirstOrDefault().ClientID + ".jpg";

                        //string fileName = sourceFileName;
                        //// Use Path class to manipulate file and directory paths.
                        //string sourceFile = System.IO.Path.GetFullPath(sourcePath);
                        //string destFile = System.IO.Path.Combine(targetPath, fileName);

                        //// To copy a folder's contents to a new location:
                        //// Create a new target folder, if necessary.
                        //if (!System.IO.Directory.Exists(targetPath))
                        //{
                        //    System.IO.Directory.CreateDirectory(targetPath);
                        //}



                        //// To copy a file to another location and 
                        //// overwrite the destination file if it already exists.
                        //stream.Close();
                        //System.IO.File.Copy(sourceFile, destFile, true);

                        //// To copy all the files in one directory to another directory.
                        //// Get the files in the source folder. (To recursively iterate through
                        //// all subfolders under the current directory, see
                        //// "How to: Iterate Through a Directory Tree.")
                        //// Note: Check for target path was performed previously
                        ////       in this code example.
                        //if (System.IO.Directory.Exists(sourcePath))
                        //{
                        //    string[] files = System.IO.Directory.GetFiles(sourcePath);

                        //    // Copy the files and overwrite destination files if they already exist.
                        //    foreach (string s in files)
                        //    {
                        //        // Use static Path methods to extract only the file name from the path.
                        //        fileName = System.IO.Path.GetFileName(s);
                        //        destFile = System.IO.Path.Combine(targetPath, fileName);
                        //        System.IO.File.Copy(s, destFile, true);
                        //    }
                        //}
                        //else
                        //{
                        //    Console.WriteLine("Source path does not exist!");
                        //}
                        //return;

                        //ImageToBase64();
                    }

                }
                if (clientSignature.Source != null)
                {
                    SaveSignature();
                }

                List<InsertClient> InsertClientList = new List<InsertClient>();
                InsertClient InsertClientContainer = new InsertClient();
                //DatePicker
                string dateOpened = dateOpenedPicker.SelectedDate.Value.ToString("yyyyMMdd"); // Basic

                //string birthDate = dateOfBirthPicker.SelectedDate.Value.ToString("yyyyMMdd");
                DateTime selecteddate;
                string birthDate;
                if (DateTime.TryParseExact(dateOfBirthPicker.Text, "MM/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out selecteddate))
                {
                    birthDate = selecteddate.ToString("yyyyMMdd");
                }
                else
                {
                    birthDate = "";
                }


                DateTime PmesDate;
                string dateOfPMES = "";
                if (!String.IsNullOrEmpty(dateofPMES.Text))
                {
                    dateOfPMES = Convert.ToDateTime(dateofPMES.Text).ToString("yyyy/MM/dd");
                }
                //if (DateTime.TryParseExact(dateofPMES.Text, "MM/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out PmesDate))
                //{

                //	dateOfPMES = PmesDate.ToString("yyyy/MM/dd");
                //}




                // string spousebirthDate = (spouseDateOfBirthPicker.SelectedDate.Equals(null) ? "" : spouseDateOfBirthPicker.SelectedDate.Value.ToString("yyyyMMdd")); //Spouse
                DateTime spousebirthdateresult;
                string spousebirthDate;
                if (DateTime.TryParseExact(spouseDateOfBirthPicker.Text, "MM/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out spousebirthdateresult))
                {
                    spousebirthDate = spousebirthdateresult.ToString("yyyyMMdd");
                }
                else
                {
                    spousebirthDate = "";
                }



                foreach (var otherID in DG_OtherID.ItemsSource)
                {

                    InsertClientOtherID insertClientID = (InsertClientOtherID)otherID;
                    if (!String.IsNullOrWhiteSpace(insertClientID.OtherID.ToString()))
                    {
                        if (String.IsNullOrWhiteSpace(insertClientID.OtherIDNumber))
                        {
                            tabControl.SelectedIndex = 3;
                            MessageBox.Show("ID Number should not be empty!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                            DG_OtherID.Focus();
                            DG_OtherID.CurrentCell = new DataGridCellInfo(DG_OtherID.SelectedItem, DG_OtherID.Columns[1]);
                            DG_OtherID.BeginEdit();
                            return;
                        }

                    }

                }

                string empFromDate = (dateOfEmpFromPicker.SelectedDate.Equals(null) ? "" : dateOfEmpFromPicker.SelectedDate.Value.ToString("yyyyMMdd"));//Client Company
                string empUntilDate = (dateOfEmpUntilPicker.SelectedDate.Equals(null) ? "" : dateOfEmpUntilPicker.SelectedDate.Value.ToString("yyyyMMdd"));
                DateTime result;
                string dateClosed;
                if (DateTime.TryParseExact(dateClosedTxt.Text, "MM/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out result))
                {
                    dateClosed = result.ToString("yyyyMMdd");
                }
                else
                {
                    dateClosed = "";
                }

                DateTime results;
                string dateTransferred;
                if (DateTime.TryParseExact(dateTransferredTxt.Text, "MM/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out results))
                {
                    dateTransferred = results.ToString("yyyyMMdd");
                }
                else
                {
                    dateTransferred = "";
                }

                DateTime results1;
                string dateDeceased;
                if (DateTime.TryParseExact(dateDeceasedTxt.Text, "MM/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out results1))
                {
                    dateDeceased = results1.ToString("yyyyMMdd");
                }
                else
                {
                    dateDeceased = "";
                }

                //string closedDate = (dateClosedDatePicker.SelectedDate.Equals(null) ? "" : dateClosedDatePicker.SelectedDate.Value.ToString("yyyyMMdd"));

                Titles title = (Titles)titleCombo.SelectedItem;
                AccountTypes accounttype = (AccountTypes)accountTypeCombo.SelectedItem;
                ClientTypes clienttype = (ClientTypes)clientTypeCombo.SelectedItem;
                ClientStatuses clientstatus = (ClientStatuses)clientStatusCombo.SelectedItem;
                Genders gender = (Genders)genderCombo.SelectedItem;
                CivilStatuses civilstatus = (CivilStatuses)civilStatusCombo.SelectedItem;
                Education education = (Education)highestEducationCombo.SelectedItem;
                Cities city = (Cities)cityCombo.SelectedItem;
                Religions religion = (Religions)religionCombo.SelectedItem;
                BloodTypes blood = (BloodTypes)bloodtypeCombo.SelectedItem;
                Nationalities nationality = (Nationalities)nationalityCombo.SelectedItem;
                Suffix suffix = (Suffix)suffixComboBox.SelectedItem;

                Restrictions restriction1 = (Restrictions)restrictionsComboBox.SelectedItem;
                Reasons reasons1 = (Reasons)reasonsComboBox.SelectedItem;
                DosRi dosri1 = (DosRi)dosRiComboBox.SelectedItem;

                //Barangay Combo box
                Barangay barangay = (Barangay)addressBrgyCombo.SelectedItem; //Address
                Barangay barangayHome = (Barangay)homeBrgyCombo.SelectedItem;
                Barangay barangayMother = (Barangay)motherBrgyCombo.SelectedItem;//Insert ClientParents
                Barangay barangayFather = (Barangay)fatherBrgyCombo.SelectedItem;
                Barangay sectorBarangay = (Barangay)companyBrgyCombo.SelectedItem;//Insert ClientCompany
                Barangay selfEmpBarangay = (Barangay)selfEmployedBrgyCombo.SelectedItem;

                HouseOwnerTypes houseOwnerType = (HouseOwnerTypes)addressOwnershipCombo.SelectedItem;
                HouseOwnerTypes houseOwnerTypeHome = (HouseOwnerTypes)homeOwnershipCombo.SelectedItem;
                Sectors sectorCompany = (Sectors)companySectorCombo.SelectedItem;
                Sectors sectorSelfEmployed = (Sectors)selfEmployedSector.SelectedItem;
                Sectors lineOfBusiness = (Sectors)selfEmployedLineBusiness.SelectedItem;
                SubSectors subSectorSelfEmployed = (SubSectors)selfEmployedSubSector.SelectedItem;

                Position position = (Position)companyPositionCombo.SelectedItem;
                OccupationStatuses empClientStatus = (OccupationStatuses)companyStatusCombo.SelectedItem;

                IncomeType salaryIncomeType = (IncomeType)salaryIcomeCombo.SelectedItem;
                IncomeType businessIncomeType = (IncomeType)businessIncomeCombo.SelectedItem;
                IncomeType spouseIncomeType = (IncomeType)spouseSalaryCombo.SelectedItem;
                IncomeType spouseBusinessIncomeType = (IncomeType)spouseBusinessIncomeCombo.SelectedItem;
                IncomeType otherIncomeType = (IncomeType)otherIncomeCombo.SelectedItem;
                Branches closedBranch = (Branches)branchDestCombo.SelectedItem;
                Schools schoolslist = (Schools)CB_Schools.SelectedItem;
                Sections sectionlist = (Sections)CB_Sections.SelectedItem;

                List<InsertClientContact> InsertClientContactList = new List<InsertClientContact>();
                List<InsertClientID> InsertClientIDList = new List<InsertClientID>();
                List<InsertClientOtherID> InsertClientOtherIDList = new List<InsertClientOtherID>();
                List<InsertClientAssets> InsertClientAssetList = new List<InsertClientAssets>();
                List<InsertClientDependent> InsertClientDependentList = new List<InsertClientDependent>();//Insert ClientDependents
                List<InsertClientCompany> InsertClientCompanyList = new List<InsertClientCompany>();//Insert ClientCompany
                List<InsertClientBank> InsertClientBankList = new List<InsertClientBank>(); // Insert Client Bank 
                List<Notification> InsertClientNotifications = new List<Notification>();
                if (option)
                {
                    try
                    {
                        btn_save.IsEnabled = false;
                        //string[] cleanClientID = clientIDText.Text.Split('-');


                        InsertClientContainer.BranchID = PMESBranchID;
                        //ClientChkID = Convert.ToByte(cleanClientID[2]),
                        InsertClientContainer.TitleID = title.TitleID;
                        InsertClientContainer.FirstName = firstNameText.Text.Trim();
                        InsertClientContainer.LastName = lastNameText.Text.Trim();
                        InsertClientContainer.MiddleName = middleNameText.Text.Trim();
                        InsertClientContainer.SuffixID = (suffixComboBox.Text.Equals("") ? Convert.ToInt16(0) : suffix.SuffixID);
                        InsertClientContainer.NickName = nickNameText.Text.Trim();
                        InsertClientContainer.DateOpened = dateOpened;
                        InsertClientContainer.AccountTypeID = accounttype.AccountTypeID;
                        InsertClientContainer.ClientTypeID = clienttype.ClientTypeID;
                        InsertClientContainer.ClientStatusID = clientstatus.ClientStatusID;
                        InsertClientContainer.DateOfBirth = birthDate;
                        InsertClientContainer.GenderID = gender.GenderID;
                        InsertClientContainer.CivilStatusID = civilstatus.CivilStatusID;
                        InsertClientContainer.EducAttainedID = education.EducAttainmentID;
                        InsertClientContainer.MunicipalityBirthID = city.CityID;
                        InsertClientContainer.Nationality = Convert.ToInt16(nationality.NationalityID);
                        InsertClientContainer.ReligionID = (religionCombo.Text.Equals("") ? 0 : Convert.ToInt64(religion.ReligionID));
                        InsertClientContainer.BloodTypeID = (bloodtypeCombo.Text.Equals("") ? 0 : Convert.ToInt64(blood.BloodTypeID));
                        InsertClientContainer.Height = (heightFtText.Text) + '.' + heightIncText.Text;
                        InsertClientContainer.Weight = WeightText.Text;
                        InsertClientContainer.NoofChildren = (noOfChildrenText.Text.Equals("") ? 0 : Convert.ToInt64(noOfChildrenText.Text));
                        InsertClientContainer.NoofDependent = (noOfDependentText.Text.Equals("") ? 0 : Convert.ToInt64(noOfDependentText.Text));
                        InsertClientContainer.PrincipalID = (principalText.Text.Equals("") ? 0 : Convert.ToInt64(principalText.Text));
                        InsertClientContainer.BatchNo = batchNoText.Text;
                        InsertClientContainer.DateOfPMES = dateOfPMES;
                        InsertClientContainer.OptionInsertOrEdit = option;
                        InsertClientContainer.AttendanceID = AttendanceID.ToString();
                        InsertClientContainer.PMESBatchNumber = PMESBatchNumber;
                        InsertClientContainer.DateClosed = dateClosed;
                        InsertClientContainer.ReasonID = (reasonsComboBox.Text.Equals("") ? Convert.ToInt16(0) : (reasons1.ReasonID));
                        InsertClientContainer.ReasonExp = othersTextBox.Text;
                        InsertClientContainer.RecruitedBy = (recruitedByTextBox.Text.Equals("") ? 0 : Convert.ToInt64(recruitedByTextBox.Text));
                        InsertClientContainer.DosriID = (dosRiComboBox.Text.Equals("") ? Convert.ToInt16(0) : dosri1.DosRiID);
                        InsertClientContainer.RelatedTo = relatedToTxt.Text;
                        InsertClientContainer.RestrictionID = restriction1.RestrictionID;
                        InsertClientContainer.DateDeceased = dateDeceased;
                        InsertClientContainer.DateTransferred = dateTransferred;
                        InsertClientContainer.BranchCode = (branchDestCombo.Text.Equals("") ? 0 : Convert.ToInt64(closedBranch.BranchCode));
                        InsertClientContainer.SchoolID = (CB_Schools.Text.Equals("") ? 0 : Convert.ToInt64(schoolslist.SchoolID));
                        InsertClientContainer.SectionID = (CB_Sections.Text.Equals("") ? 0 : Convert.ToInt64(sectionlist.SectionID));

                        InsertClientContainer.Mother = Mother();
                        InsertClientContainer.Father = Father();
                        InsertClientContainer.Present = Present();
                        InsertClientContainer.Home = Home();
                        InsertClientContainer.Spouse = Spouse(spousebirthDate);
                        InsertClientContainer.Employer = Employer();
                        InsertClientContainer.Self = selfEmployed();
                        InsertClientContainer.Income = income();

                        InsertClientContainer.ClientAssets = InsertClientAssets();
                        InsertClientContainer.ClientContacts = InsertClientContacts();
                        InsertClientContainer.ClientIDs = InsertClientID();
                        InsertClientContainer.ClientOtherIDs = InsertClientOtherID();
                        InsertClientContainer.ClientBanks = InsertClientBanks();
                        InsertClientContainer.ClientNotifications = InsertClientNotification();
                        InsertClientContainer.ClientDependents = InsertClientDependents();

                        InsertClientContainer.ClientDOSRIs = InsertClientDOSRIs();
                        //

                    }
                    catch (Exception ex)
                    {
                        btn_save.IsEnabled = true;
                        MessageBox.Show("Client Not Save!", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                    String parsed = new JavaScriptSerializer().Serialize(InsertClientContainer);
                    //String parsed = new JavaScriptSerializer().Serialize(InsertClientList);
                    CRUXLib.Response Response = App.CRUXAPI.request("client/insertclient", parsed, int.MaxValue);

                    if (Response.Status == "SUCCESS" && !String.IsNullOrEmpty(Response.Content))
                    {
                        MessageBox.Show("Client Was Successfully Added!", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        refreshMe();
                        clientIDText.Text = "";
                        txtAgeUponEntry.Text = "";
                        dateOfBirthPicker.Text = "";
                        ageText.Text = "";
                        recruitedByTextBox.Text = "";
                        Disable();
                        displayClientID = 0;
                        btn_new.IsEnabled = true;
                        btn_find.IsEnabled = true;
                        clientSignature.Source = null;
                        clientImage.Source = null;
                        btn_find.Focus();
                        //Unlock();
                    }
                    else
                    {
                        btn_save.IsEnabled = true;
                        MessageBox.Show(Response.Content, "Client Not Save!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {

                    try
                    {
                        btn_save.IsEnabled = false;

                        InsertClientContainer.ClientID = displayClientID;
                        InsertClientContainer.TitleID = title.TitleID;
                        InsertClientContainer.FirstName = firstNameText.Text.Trim();
                        InsertClientContainer.LastName = lastNameText.Text.Trim();
                        InsertClientContainer.MiddleName = middleNameText.Text.Trim();
                        InsertClientContainer.SuffixID = (suffixComboBox.Text.Equals("") ? Convert.ToInt16(0) : suffix.SuffixID);
                        InsertClientContainer.NickName = nickNameText.Text.Trim();
                        InsertClientContainer.DateOpened = dateOpened;
                        InsertClientContainer.AccountTypeID = accounttype.AccountTypeID;
                        InsertClientContainer.ClientTypeID = clienttype.ClientTypeID;
                        InsertClientContainer.ClientStatusID = clientstatus.ClientStatusID;
                        InsertClientContainer.DateOfBirth = birthDate;
                        InsertClientContainer.GenderID = gender.GenderID;
                        InsertClientContainer.CivilStatusID = civilstatus.CivilStatusID;
                        InsertClientContainer.EducAttainedID = education.EducAttainmentID;
                        InsertClientContainer.MunicipalityBirthID = city.CityID;
                        InsertClientContainer.Nationality = Convert.ToInt16(nationality.NationalityID);
                        InsertClientContainer.ReligionID = (religionCombo.Text.Equals("") ? 0 : Convert.ToInt64(religion.ReligionID));
                        InsertClientContainer.BloodTypeID = (bloodtypeCombo.Text.Equals("") ? 0 : Convert.ToInt64(blood.BloodTypeID));
                        InsertClientContainer.Height = (heightFtText.Text) + '.' + heightIncText.Text;
                        InsertClientContainer.Weight = WeightText.Text;
                        InsertClientContainer.NoofChildren = (noOfChildrenText.Text.Equals("") ? 0 : Convert.ToInt64(noOfChildrenText.Text));
                        InsertClientContainer.NoofDependent = (noOfDependentText.Text.Equals("") ? 0 : Convert.ToInt64(noOfDependentText.Text));
                        InsertClientContainer.PrincipalID = (principalText.Text.Equals("") ? 0 : Convert.ToInt64(principalText.Text));
                        InsertClientContainer.BatchNo = batchNoText.Text;
                        InsertClientContainer.DateOfPMES = dateOfPMES;

                        InsertClientContainer.DateClosed = dateClosed;
                        InsertClientContainer.ReasonID = (reasonsComboBox.Text.Equals("") ? Convert.ToInt16(0) : (reasons1.ReasonID));
                        InsertClientContainer.ReasonExp = othersTextBox.Text;
                        InsertClientContainer.RecruitedBy = (recruitedByTextBox.Text.Equals("") ? 0 : Convert.ToInt64(recruitedByTextBox.Text));
                        InsertClientContainer.DosriID = (dosRiComboBox.Text.Equals("") ? Convert.ToInt16(0) : dosri1.DosRiID);
                        InsertClientContainer.RelatedTo = relatedToTxt.Text;
                        InsertClientContainer.RestrictionID = (restrictionsComboBox.Text.Equals("") ? Convert.ToInt16(0) : restriction1.RestrictionID);

                        InsertClientContainer.DateDeceased = dateDeceased;
                        InsertClientContainer.DateTransferred = dateTransferred;
                        InsertClientContainer.BranchCode = (branchDestCombo.Text.Equals("") ? 0 : Convert.ToInt64(closedBranch.BranchCode));
                        InsertClientContainer.SchoolID = (CB_Schools.Text.Equals("") ? 0 : Convert.ToInt64(schoolslist.SchoolID));
                        InsertClientContainer.SectionID = (CB_Sections.Text.Equals("") ? 0 : Convert.ToInt64(sectionlist.SectionID));
                        InsertClientContainer.Present = Present();
                        InsertClientContainer.Home = Home();
                        InsertClientContainer.Mother = Mother();
                        InsertClientContainer.Father = Father();

                        InsertClientContainer.Spouse = Spouse(spousebirthDate);
                        InsertClientContainer.Employer = Employer();
                        InsertClientContainer.Self = selfEmployed();
                        InsertClientContainer.Income = income();

                        InsertClientContainer.ClientAssets = InsertClientAssets(displayClientID);
                        InsertClientContainer.ClientContacts = InsertClientContacts(displayClientID);
                        InsertClientContainer.ClientIDs = InsertClientID(displayClientID);
                        InsertClientContainer.ClientOtherIDs = InsertClientOtherID(displayClientID);
                        InsertClientContainer.ClientBanks = InsertClientBanks(displayClientID);
                        InsertClientContainer.ClientNotifications = InsertClientNotification(displayClientID);
                        InsertClientContainer.ClientDependents = InsertClientDependents(displayClientID);

                        InsertClientContainer.ClientDOSRIs = InsertClientDOSRIs();

                    }
                    catch (Exception ex)
                    {
                        btn_save.IsEnabled = true;

                        MessageBox.Show("Client Not Save!", "Warning", MessageBoxButton.OK, MessageBoxImage.Error);
                        //Console.WriteLine("List " + ex.Message);
                        return;
                    }

                    //String parsed = new JavaScriptSerializer().Serialize(InsertClientList);
                    String parsed = new JavaScriptSerializer().Serialize(InsertClientContainer);
                    CRUXLib.Response Response = App.CRUXAPI.request("client/insertclient", parsed, int.MaxValue);

                    if (Response.Status == "SUCCESS")
                    {
                        MessageBox.Show("Client Was Successfully Updated!", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        refreshMe();
                        clientIDText.Text = "";
                        txtAgeUponEntry.Text = "";
                        dateOfBirthPicker.Text = "";
                        ageText.Text = "";
                        recruitedByTextBox.Text = "";
                        Disable();
                        displayClientID = 0;
                        btn_new.IsEnabled = true;
                        btn_find.IsEnabled = true;
                        clientSignature.Source = null;
                        clientImage.Source = null;
                        btn_find.Focus();
                        Unlock();
                    }
                    else if (Response.Status == "SL Not Zero")
                    {
                        btn_save.IsEnabled = true;
                        MessageBox.Show("Cannot close account, client has existing SL balance!", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        Unlock();
                    }
                    else
                    {
                        btn_save.IsEnabled = true;
                        MessageBox.Show(Response.Content, "Client Not Save!", MessageBoxButton.OK, MessageBoxImage.Error);
                        Unlock();
                    }
                }

            }


        }

        public void InsertBeneficiaries(String ClientID, List<InsertClientBeneficiaries> Insertclientbeneficiries)
        {

            string parsedIDs = new JavaScriptSerializer().Serialize(Insertclientbeneficiries);

            CRUXLib.Response ResponseIDs = App.CRUXAPI.request("client/insertBeneficiaries", parsedIDs);
            if (ResponseIDs.Status == "SUCCESS")
            {
                //MessageBox.Show("Beneficiaries Successfully Added");
            }
            else
            {
                MessageBox.Show("Beneficiaries Not Successfully Added", "Warning!", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private InsertPresent Present()
        {
            InsertPresent insertPresent = new InsertPresent();
            try
            {
                Barangay barangay = (Barangay)addressBrgyCombo.SelectedItem; //Address
                HouseOwnerTypes houseOwnerType = (HouseOwnerTypes)addressOwnershipCombo.SelectedItem;

                insertPresent.BarangayID = (addressBrgyCombo.Text.Equals("") ? 0 : barangay.BrgyID);
                insertPresent.HouseNo = addressHouseNoText.Text;
                insertPresent.PostalCode = (addressPostalCodeText.Text.Equals("") ? 0 : Convert.ToInt32(addressPostalCodeText.Text));
                insertPresent.Street = addressStreetText.Text;
                insertPresent.SinceDate = (addressSinceDateCombo.Text.Equals("") ? 0 : Convert.ToInt32(addressSinceDateCombo.Text));
                insertPresent.AddressType = "MI";
                insertPresent.LivingwithParents = addressLivingWithParentsCheckBox.IsChecked.Value;
                insertPresent.Owned = (addressOwnershipCombo.Text.Equals("") ? 0 : Convert.ToInt32(addressOwnershipCombo.SelectedValue));
                insertPresent.Owner = addressOwnerIfRentedText.Text;

                return insertPresent;
            }
            catch (Exception ex)
            {
                return insertPresent;
            }

        }

        private InsertHome Home()
        {
            InsertHome insertHome = new InsertHome();
            try
            {
                Barangay barangayHome = (Barangay)homeBrgyCombo.SelectedItem;
                HouseOwnerTypes houseOwnerTypeHome = (HouseOwnerTypes)homeOwnershipCombo.SelectedItem;

                insertHome.HomeBarangayID = (homeBrgyCombo.Text.Equals("") ? 0 : barangayHome.BrgyID);
                insertHome.HomeHouseNo = homeHouseNoText.Text;
                insertHome.HomePostalCode = (homePostalCodeText.Text.Equals("") ? 0 : Convert.ToInt32(homePostalCodeText.Text));
                insertHome.HomeStreet = homeStreetText.Text;
                insertHome.HomeSinceDate = (homeSinceDateCombo.Text.Equals("") ? 0 : Convert.ToInt32(homeSinceDateCombo.Text));
                insertHome.HomeAddressType = "AI";
                insertHome.HomeLivingwithParents = homeLivingWithParentsCheckBox.IsChecked.Value;
                insertHome.HomeOwned = (homeOwnershipCombo.Text.Equals("") ? Convert.ToInt32(0) : Convert.ToInt32(homeOwnershipCombo.SelectedValue));
                insertHome.HomeOwner = homeOwnerIfRentedText.Text;


                return insertHome;
            }
            catch (Exception ex)
            {
                return insertHome;
            }

        }

        private InsertSpouse Spouse(String spousebirthDate)
        {
            Suffix suffix = (Suffix)suffixSpouseComboBox.SelectedItem;
            InsertSpouse insSpouse = new InsertSpouse();
            try
            {
                insSpouse.SpouseLastName = spouseLastNameText.Text;
                insSpouse.SpouseFirstName = spouseFirstNameText.Text;
                insSpouse.SpouseMiddleName = spouseMiddleNameText.Text;
                insSpouse.SpouseSuffix = (suffixSpouseComboBox.Text.Equals("") ? Convert.ToInt16(0) : suffix.SuffixID);
                insSpouse.SpouseNickname = spouseNicknameText.Text;
                insSpouse.SpouseMaidenName = spouseMaidenNameText.Text;
                insSpouse.SpouseCompanyID = (spouseCompanyIDText.Text.Equals("") ? 0 : Convert.ToInt64(spouseCompanyIDText.Text)); ;
                insSpouse.SpouseMobileNumber = (spouseMobileNoText.Text);
                insSpouse.SpouseOfficePhoneNumber = spouseOfficeNoText.Text;
                insSpouse.SpousePhoneNumber = spousePhoneNoText.Text;
                insSpouse.SpouseEmailAddress = spouseEmailAddressText.Text;
                insSpouse.SpouseEmailAddress2 = spouseEmailAddress1Text.Text;
                insSpouse.SpouseDateOfBirth = spousebirthDate;
                insSpouse.SpouseOccupation = spouseOccupationText.Text;

                return insSpouse;
            }
            catch (Exception ex)
            {
                return insSpouse;
            }
        }

        private InsertParentsMother Mother()
        {
            InsertParentsMother insertMother = new InsertParentsMother();
            try
            {
                Barangay barangayMother = (Barangay)motherBrgyCombo.SelectedItem;//Insert ClientParents

                insertMother.MotherLastName = motherLastNameText.Text;
                insertMother.MotherFirstName = motherFirstNameText.Text;
                insertMother.MotherMiddleName = motherMaidenNameText.Text;
                insertMother.MotherBarangayID = (motherBrgyCombo.Text.Equals("") ? 0 : barangayMother.BrgyID);
                insertMother.MotherHouseNo = motherHouseNoText.Text;
                insertMother.MotherStreet = motherStreetText.Text;
                insertMother.MotherPostalCode = (motherPostalCodeText.Text.Equals("") ? 0 : Convert.ToInt32(motherPostalCodeText.Text));
                insertMother.MotherSinceYear = (motherSinceYearCombo.Text.Equals("") ? 0 : Convert.ToInt32(motherSinceYearCombo.Text));

                return insertMother;
            }
            catch (Exception ex)
            {
                return insertMother;
            }

        }

        private InsertParentsFather Father()
        {
            InsertParentsFather insertFather = new InsertParentsFather();
            try
            {
                Barangay barangayFather = (Barangay)fatherBrgyCombo.SelectedItem;//Insert ClientParents

                insertFather.FatherLastName = fatherLastNameText.Text;
                insertFather.FatherFirstName = fatherFirstNameText.Text;
                insertFather.FatherMiddleName = fatherMiddleNameText.Text;
                insertFather.FatherBarangayID = (fatherBrgyCombo.Text.Equals("") ? 0 : barangayFather.BrgyID);
                insertFather.FatherHouseNo = fatherHouseNoText.Text;
                insertFather.FatherStreet = fatherStreetText.Text;
                insertFather.FatherPostalCode = (fatherPostalCodeText.Text.Equals("") ? 0 : Convert.ToInt32(fatherPostalCodeText.Text));
                insertFather.FatherSinceYear = (fatherSinceYearCombo.Text.Equals("") ? 0 : Convert.ToInt32(fatherSinceYearCombo.Text));
                insertFather.FatherParentType = 1;

                return insertFather;
            }
            catch (Exception)
            {
                return insertFather;
            }

        }

        private InsertEmployer Employer()
        {
            InsertEmployer insertEmployer = new InsertEmployer();
            try
            {
                Sectors sectorCompany = (Sectors)companySectorCombo.SelectedItem;
                string empFromDate = (dateOfEmpFromPicker.SelectedDate.Equals(null) ? "" : dateOfEmpFromPicker.SelectedDate.Value.ToString("yyyyMMdd"));//Client Company
                string empUntilDate = (dateOfEmpUntilPicker.SelectedDate.Equals(null) ? "" : dateOfEmpUntilPicker.SelectedDate.Value.ToString("yyyyMMdd"));
                Position position = (Position)companyPositionCombo.SelectedItem;
                OccupationStatuses empClientStatus = (OccupationStatuses)companyStatusCombo.SelectedItem;
                Barangay sectorBarangay = (Barangay)companyBrgyCombo.SelectedItem;//Insert ClientCompany

                insertEmployer.EmpCompanyID = (companyEmpCodeText.Text.Equals("") ? 0 : Convert.ToInt64(companyEmpCodeText.Text));
                insertEmployer.EmpFrom = empFromDate;
                insertEmployer.EmpUntil = empUntilDate;
                insertEmployer.EmpSectorID = (companySectorCombo.Text.Equals("") ? 0 : sectorCompany.SectorID);
                insertEmployer.EmpPositionID = (companyPositionCombo.Text.Equals("") ? 0 : position.OccupationID);
                insertEmployer.EmpStatusID = Convert.ToInt16(companyStatusCombo.Text.Equals("") ? 0 : empClientStatus.OccupationStatusID);
                insertEmployer.EmpBarangayID = (companyBrgyCombo.Text.Equals("") ? 0 : sectorBarangay.BrgyID);
                insertEmployer.EmpHouseNo = companyHouseNoText.Text;
                insertEmployer.EmpStreet = companyStreetText.Text;
                insertEmployer.EmpPostalCode = companyPostalCodeText.Text;
                insertEmployer.EmpTenureship = Convert.ToInt16(companyTenureshipText.Text.Equals("") ? 0 : Int32.Parse(companyTenureshipText.Text));
                insertEmployer.EmpType = true;

                return insertEmployer;
            }
            catch (Exception ex)
            {
                return insertEmployer;
            }

        }

        private InsertSelfEmployed selfEmployed()
        {
            InsertSelfEmployed insertSelfEmployed = new InsertSelfEmployed();
            try
            {
                Barangay selfEmpBarangay = (Barangay)selfEmployedBrgyCombo.SelectedItem;
                Sectors sectorSelfEmployed = (Sectors)selfEmployedSector.SelectedItem;
                Sectors lineOfBusiness = (Sectors)selfEmployedLineBusiness.SelectedItem;
                SubSectors subSectorSelfEmployed = (SubSectors)selfEmployedSubSector.SelectedItem;


                insertSelfEmployed.SelfEmpSectorID = (selfEmployedSector.Text.Equals("") ? 0 : sectorSelfEmployed.SectorID);
                insertSelfEmployed.SelfEmpSubSectorID = (selfEmployedSubSector.Text.Equals("") ? 0 : subSectorSelfEmployed.SubSectorID);
                insertSelfEmployed.SelfEmpOccupation = selfEmployedOccupationText.Text;
                insertSelfEmployed.SelfEmpBusinessName = selfEmployedBusinessNameText.Text;
                insertSelfEmployed.SelfEmpLineOfBusiness = (selfEmployedLineBusiness.Text.Equals("") ? 0 : lineOfBusiness.SectorID);
                insertSelfEmployed.SelfEmpBarangayID = (selfEmployedBrgyCombo.Text.Equals("") ? 0 : selfEmpBarangay.BrgyID);
                insertSelfEmployed.SelfEmpHouseNo = selfEmployedHouseNoText.Text;
                insertSelfEmployed.SelfEmpStreet = selfEmployedStreetText.Text;
                insertSelfEmployed.SelfEmpPostalCode = selfEmployedPostalCodeText.Text;
                insertSelfEmployed.SelfEmpType = false;

                return insertSelfEmployed;
            }
            catch (Exception ex)
            {
                return insertSelfEmployed;
            }

        }

        private InsertIncome income()
        {
            InsertIncome insertIncome = new InsertIncome();
            try
            {
                IncomeType salaryIncomeType = (IncomeType)salaryIcomeCombo.SelectedItem;
                IncomeType businessIncomeType = (IncomeType)businessIncomeCombo.SelectedItem;
                IncomeType spouseIncomeType = (IncomeType)spouseSalaryCombo.SelectedItem;
                IncomeType spouseBusinessIncomeType = (IncomeType)spouseBusinessIncomeCombo.SelectedItem;
                IncomeType otherIncomeType = (IncomeType)otherIncomeCombo.SelectedItem;


                insertIncome.AnnualSalaryIncome = (annualIncomeText.Text.Equals("") ? 0 : Convert.ToDecimal(annualIncomeText.Text));
                insertIncome.AnnualBusinessIncome = (annualBusinessIncomeText.Text.Equals("") ? 0 : Convert.ToDecimal(annualBusinessIncomeText.Text));
                insertIncome.SpouseAnnualSalary = (spouseAnnualSalaryText.Text.Equals("") ? 0 : Convert.ToDecimal(spouseAnnualSalaryText.Text));
                insertIncome.SpouseBusinessIncome = (spouseAnnualBusinessIncomeText.Text.Equals("") ? 0 : Convert.ToDecimal(spouseAnnualBusinessIncomeText.Text));
                insertIncome.OtherAnnualIncome = (otherAnnualIncomeText.Text.Equals("") ? 0 : Convert.ToDecimal(otherAnnualIncomeText.Text));
                insertIncome.SalaryIncomeType = salaryIncomeType.PaymentModeID;
                insertIncome.BusinessIncomeType = businessIncomeType.PaymentModeID;
                insertIncome.SpouseSalaryIncomeType = spouseIncomeType.PaymentModeID;
                insertIncome.SpouseBusinessIncomeType = spouseBusinessIncomeType.PaymentModeID;
                insertIncome.OtherIncomeType = otherIncomeType.PaymentModeID;

                return insertIncome;
            }
            catch (Exception ex)
            {
                return insertIncome;
            }

        }


        //Insert Client ID
        private void InsertClientID(String clientID, List<InsertClientID> InsertClientIDList)
        {
            InsertClientID insertClientID;

            foreach (var primaryID in DG_PrimaryID.ItemsSource)
            {
                insertClientID = (InsertClientID)primaryID;
                InsertClientIDList.Add(new InsertClientID()
                {
                    ID = insertClientID.ID,
                    ClientID = Convert.ToInt64(clientID),
                    IDTypeValue = insertClientID.IDTypeValue,
                    IDNumber = insertClientID.IDNumber,
                    IDType = false,
                });

            }

            string parsedIDs = new JavaScriptSerializer().Serialize(InsertClientIDList);

            CRUXLib.Response ResponseIDs = App.CRUXAPI.request("client/insertPrimaryID", parsedIDs);
        }

        private void InsertClientOtherID(String clientID, List<InsertClientOtherID> InsertClientOtherIDList)
        {
            foreach (var otherID in DG_OtherID.ItemsSource)
            {
                InsertClientOtherID insertClientID = (InsertClientOtherID)otherID;
                if (!String.IsNullOrWhiteSpace(insertClientID.OtherID.ToString()))
                {
                    DateTime IDDateIssuedResult;
                    string IDDateIssued = "";
                    if (DateTime.TryParseExact(insertClientID.IDDateIssued, "MMddyyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out IDDateIssuedResult))
                    {
                        DateTime IDDateExpiredResult;
                        string IDDateExpired = "";
                        if (DateTime.TryParseExact(insertClientID.IDDateExpired, "MMddyyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out IDDateExpiredResult))
                        {
                            IDDateExpired = IDDateExpiredResult.ToString("yyyy/MM/dd");
                            IDDateIssued = IDDateIssuedResult.ToString("yyyy/MM/dd");
                            InsertClientOtherIDList.Add(new InsertClientOtherID()
                            {
                                OtherID = insertClientID.OtherID,
                                ClientID = Convert.ToInt64(clientID),
                                OtherIDTypeValue = insertClientID.OtherIDTypeValue,
                                OtherIDNumber = insertClientID.OtherIDNumber,
                                IDDateIssued = IDDateIssued,
                                IDDateExpired = IDDateExpired,//.ToString("yyyyMMdd"),
                                OtherIDType = true,
                            });
                        }
                    }
                    //else
                    //{
                    //	MessageBox.Show("Date Expiry is not valid!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //	return;
                    //}
                    // DateTime iddate = insertClientID.IDDateIssued;

                }
                else
                {
                    MessageBox.Show("Date Issued is not valid!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

            }

            string parsedIDs = new JavaScriptSerializer().Serialize(InsertClientOtherIDList);

            CRUXLib.Response ResponseIDs = App.CRUXAPI.request("client/insertOtherID", parsedIDs);
        }

        private void InsertClientBanks(String clientID, List<InsertClientBank> InsertClientBanksList)
        {
            foreach (var bank in DG_Bank.ItemsSource)
            {
                InsertClientBank insertClientBank = (InsertClientBank)bank;
                InsertClientBanksList.Add(new InsertClientBank()
                {
                    BankID = insertClientBank.BankID,
                    ClientID = Convert.ToInt64(clientID),
                    BankNameID = insertClientBank.BankNameID,
                    BankNumber = insertClientBank.BankNumber,
                    BankAcctTypeID = insertClientBank.BankAcctTypeID
                });
            }

            string parsedIDs = new JavaScriptSerializer().Serialize(InsertClientBanksList);

            CRUXLib.Response ResponseIDs = App.CRUXAPI.request("client/insertBank", parsedIDs);
        }

        //Client Contact
        private void InsertClientContact(String clientID, List<InsertClientContact> InsertClientContactList)
        {
            foreach (var contact in DG_ContactType.ItemsSource)
            {
                InsertClientContact insertClientContact = (InsertClientContact)contact;
                InsertClientContactList.Add(new InsertClientContact()
                {
                    ClientID = Convert.ToInt64(clientID),
                    ContactTypeID = insertClientContact.ContactTypeID,
                    ContactNo = insertClientContact.ContactNo,
                    ContactID = insertClientContact.ContactID,
                });
            }

            string parsedContacts = new JavaScriptSerializer().Serialize(InsertClientContactList);

            CRUXLib.Response ResponseContacts = App.CRUXAPI.request("client/insertContacts", parsedContacts);
        }

        /// <summary>
        /// Client Assets
        /// </summary>
        private List<InsertClientAssets> InsertClientAssets(Int64 ClientID = 0)
        {
            List<InsertClientAssets> clientAssets = new List<Models.InsertClientAssets>();

            try
            {
                foreach (InsertClientAssets insertClientAsset in DG_Asset.ItemsSource)
                {
                    clientAssets.Add(new InsertClientAssets()
                    {
                        AssetID = insertClientAsset.AssetID,
                        AssetTypeID = insertClientAsset.AssetTypeID,
                        AssetDescription = insertClientAsset.AssetDescription,
                        ClientID = ClientID
                    });
                }

                return clientAssets;
            }
            catch (Exception e)
            {
                return clientAssets;
            }
        }

        /// <summary>
        /// Client Contacts
        /// </summary>
        /// <returns></returns>
        private List<InsertClientContact> InsertClientContacts(Int64 ClientID = 0)
        {
            List<InsertClientContact> clientContacts = new List<Models.InsertClientContact>();
            try
            {
                foreach (var contact in DG_ContactType.ItemsSource)
                {
                    InsertClientContact insertClientContact = (InsertClientContact)contact;
                    clientContacts.Add(new InsertClientContact()
                    {
                        ContactTypeID = insertClientContact.ContactTypeID,
                        ContactNo = insertClientContact.ContactNo,
                        ContactID = insertClientContact.ContactID,
                        ClientID = ClientID
                    });
                }

                return clientContacts;
            }
            catch (Exception ex)
            {
                return clientContacts;
            }
        }

        /// <summary>
        /// Client IDs
        /// </summary>
        /// <returns></returns>
        private List<InsertClientID> InsertClientID(Int64 ClientID = 0)
        {
            List<InsertClientID> clientIDs = new List<Models.InsertClientID>();

            try
            {
                InsertClientID insertClientID;

                foreach (var primaryID in DG_PrimaryID.ItemsSource)
                {
                    insertClientID = (InsertClientID)primaryID;
                    clientIDs.Add(new InsertClientID()
                    {
                        ID = insertClientID.ID,
                        IDTypeValue = insertClientID.IDTypeValue,
                        IDNumber = insertClientID.IDNumber,
                        IDType = false,
                        ClientID = ClientID
                    });
                }

                return clientIDs;

            }
            catch (Exception ex)
            {
                return clientIDs;
            }
        }

        /// <summary>
        /// Client Other IDs
        /// </summary>
        /// <returns></returns>
        private List<InsertClientOtherID> InsertClientOtherID(Int64 ClientID = 0)
        {
            List<InsertClientOtherID> clientOtherIDs = new List<Models.InsertClientOtherID>();

            try
            {
                foreach (var otherID in DG_OtherID.ItemsSource)
                {
                    InsertClientOtherID insertClientID = (InsertClientOtherID)otherID;
                    if (!String.IsNullOrWhiteSpace(insertClientID.OtherID.ToString()))
                    {
                        DateTime IDDateIssuedResult;
                        string IDDateIssued = "";
                        if (DateTime.TryParseExact(insertClientID.IDDateIssued, "MMddyyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out IDDateIssuedResult))
                        {
                            DateTime IDDateExpiredResult;
                            string IDDateExpired = "";
                            if (DateTime.TryParseExact(insertClientID.IDDateExpired, "MMddyyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out IDDateExpiredResult))
                            {
                                IDDateExpired = IDDateExpiredResult.ToString("yyyy/MM/dd");
                                IDDateIssued = IDDateIssuedResult.ToString("yyyy/MM/dd");
                                clientOtherIDs.Add(new InsertClientOtherID()
                                {
                                    OtherID = insertClientID.OtherID,
                                    OtherIDTypeValue = insertClientID.OtherIDTypeValue,
                                    OtherIDNumber = insertClientID.OtherIDNumber,
                                    IDDateIssued = IDDateIssued,
                                    IDDateExpired = IDDateExpired,//.ToString("yyyyMMdd"),
                                    OtherIDType = true,
                                    ClientID = ClientID
                                });
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Date Issued is not valid!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }

                return clientOtherIDs;
            }
            catch (Exception ex)
            {
                return clientOtherIDs;
            }
        }

        /// <summary>
        /// Client Bank
        /// </summary>
        private List<InsertClientBank> InsertClientBanks(Int64 ClientID = 0)
        {
            List<InsertClientBank> clientBanks = new List<InsertClientBank>();
            try
            {
                foreach (var bank in DG_Bank.ItemsSource)
                {
                    InsertClientBank insertClientBank = (InsertClientBank)bank;
                    clientBanks.Add(new InsertClientBank()
                    {
                        BankID = insertClientBank.BankID,
                        BankNameID = insertClientBank.BankNameID,
                        BankNumber = insertClientBank.BankNumber,
                        BankAcctTypeID = insertClientBank.BankAcctTypeID,
                        ClientID = ClientID
                    });
                }

                return clientBanks;
            }
            catch (Exception ex)
            {
                return clientBanks;
            }
        }

        /// <summary>
        /// Client Notifications
        /// </summary>
        /// <returns></returns>
        private List<Notification> InsertClientNotification(Int64 ClientID = 0)
        {
            List<Notification> clientNotifications = new List<Notification>();
            try
            {
                if (dataGridNotification.Items.Count > 0)
                {
                    foreach (var notification in dataGridNotification.ItemsSource)
                    {
                        Notification insertClientNotif = (Notification)notification;

                        clientNotifications.Add(new Notification()
                        {
                            NotificationIDs = insertClientNotif.NotificationIDs.Equals(null) ? 0 : insertClientNotif.NotificationIDs,
                            NotificationID = insertClientNotif.NotificationID,
                            BranchID = displayBranchID,
                            NotificationStatus = insertClientNotif.NotificationStatus,
                            TR_DATE = insertClientNotif.TR_DATE,
                            EncodedBy = insertClientNotif.EncodedBy,
                            Remarks = insertClientNotif.Remarks,
                            IsPrompt = insertClientNotif.IsPrompt,
                            ClientID = ClientID
                        });
                    }
                }

                return clientNotifications;
            }
            catch (Exception)
            {
                return clientNotifications;
            }

        }

        /// <summary>
        /// Client Dependents
        /// </summary>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        private List<InsertClientDependent> InsertClientDependents(Int64 ClientID = 0)
        {
            List<InsertClientDependent> clientDependents = new List<InsertClientDependent>();

            try
            {
                foreach (var dependent in DG_Dependent.ItemsSource)
                {
                    InsertClientDependent insertClientDependent = (InsertClientDependent)dependent;
                    DateTime birthdateResult;
                    string birthdate = "";
                    if (DateTime.TryParseExact(insertClientDependent.Birthdate, "MMddyyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out birthdateResult))
                    {
                        birthdate = birthdateResult.ToString("yyyy/MM/dd");
                        clientDependents.Add(new InsertClientDependent()
                        {
                            DependentID = insertClientDependent.DependentID,
                            DependentName = insertClientDependent.DependentName,
                            Relation = insertClientDependent.Relation,
                            Birthdate = birthdate,
                            ClientID = ClientID
                        });
                    }
                }

                return clientDependents;
            }
            catch (Exception)
            {
                return clientDependents;
            }

        }


        private List<InsertDOSRI> InsertClientDOSRIs(Int64 ClientID = 0)
        {
            List<InsertDOSRI> clientDOSRIs = new List<InsertDOSRI>();
            int seqNo = 0;
            try
            {
                foreach (InsertDOSRI dosri in DG_DOSRI.ItemsSource)
                {

                    clientDOSRIs.Add(new InsertDOSRI()
                    {
                        SeqNo = seqNo,
                        DosriID = dosri.DosriID,
                        AcctTypeID = dosri.AcctTypeID,
                        DOSRIBranchID = dosri.DOSRIBranchID,
                        AcctID = dosri.AcctID,
                        AcctName = dosri.AcctName,
                        ClientID = ClientID
                    });
                    seqNo += 1;
                }

                return clientDOSRIs;
            }
            catch (Exception)
            {
                return clientDOSRIs;
            }
        }


        private void InsertClientAssets(String clientID, List<InsertClientAssets> InsertClientAssetList)
        {

            foreach (var item in DG_Asset.ItemsSource)
            {
                InsertClientAssets insertClientAsset = (InsertClientAssets)item;

                InsertClientAssetList.Add(new InsertClientAssets()
                {
                    AssetID = insertClientAsset.AssetID,
                    ClientID = Convert.ToInt64(clientID),
                    AssetTypeID = insertClientAsset.AssetTypeID,
                    AssetDescription = insertClientAsset.AssetDescription
                });
            }

            string parsedAssets = new JavaScriptSerializer().Serialize(InsertClientAssetList);
            CRUXLib.Response ResponseAssets = App.CRUXAPI.request("client/insertClientAsset", parsedAssets);
        }

        private String InsertClientDependents(String clientID, List<InsertClientDependent> InsertClientDependentList)
        {
            foreach (var dependent in DG_Dependent.ItemsSource)
            {
                InsertClientDependent insertClientDependent = (InsertClientDependent)dependent;
                DateTime birthdateResult;
                string birthdate = "";
                if (DateTime.TryParseExact(insertClientDependent.Birthdate, "MMddyyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out birthdateResult))
                {
                    birthdate = birthdateResult.ToString("yyyy/MM/dd");
                    InsertClientDependentList.Add(new InsertClientDependent()
                    {
                        DependentID = insertClientDependent.DependentID,
                        ClientID = Convert.ToInt64(clientID),
                        DependentName = insertClientDependent.DependentName,
                        Relation = insertClientDependent.Relation,
                        Birthdate = birthdate
                    });
                }
            }

            /////Insert Client Dependent


            string parsedDependents = new JavaScriptSerializer().Serialize(InsertClientDependentList);
            CRUXLib.Response ResponseDependents = App.CRUXAPI.request("client/insertClientDependent", parsedDependents);
            return ResponseDependents.Status;
            //if(ResponseDependents.Status == "SUCCESS")
            //{
            //    MessageBox.Show("Client Was Successfully Added!", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
            //}
        }

        private String InsertClientNotification(String clientID, List<Notification> InsertClientNotifications)
        {
            if (dataGridNotification.Items.Count > 0)
            {

                foreach (var notification in dataGridNotification.ItemsSource)
                {
                    Notification insertClientNotif = (Notification)notification;

                    InsertClientNotifications.Add(new Notification()
                    {
                        NotificationIDs = insertClientNotif.NotificationIDs.Equals(null) ? 0 : insertClientNotif.NotificationIDs,
                        NotificationID = insertClientNotif.NotificationID,
                        BranchID = displayBranchID,
                        ClientID = Convert.ToInt64(clientID),
                        NotificationStatus = insertClientNotif.NotificationStatus,
                        TR_DATE = insertClientNotif.TR_DATE,
                        EncodedBy = insertClientNotif.EncodedBy,
                        Remarks = insertClientNotif.Remarks,
                        IsPrompt = insertClientNotif.IsPrompt
                    });
                }

                /////Insert Client Dependent
                string parsedDependents = new JavaScriptSerializer().Serialize(InsertClientNotifications);

                CRUXLib.Response ResponseDependents = App.CRUXAPI.request("client/insertNotification", parsedDependents);

                return ResponseDependents.Status;
                //if(ResponseDependents.Status == "SUCCESS")
                //{
                //    MessageBox.Show("Client Was Successfully Added!", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                //}
            }
            else
            {
                return "";
            }
        }

        private void NameValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[ A-Za-z0-9-._-ñÑúÉ]*$");

            if (!regex.IsMatch(e.Text))
            {
                MessageBox.Show("Invalid character.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                e.Handled = true;
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.,-]+");

            if (regex.IsMatch(e.Text))
            {
                MessageBox.Show("Invalid character.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                e.Handled = true;
            }
        }

        private void EmailValidationTextBox(object sender, TextCompositionEventArgs e)
        {

            if (!Regex.IsMatch(e.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
            {
                MessageBox.Show("Please input valid email address ", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void selfEmployedSector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Object selectedSector = new Object();

            selectedSector = selfEmployedSector.SelectedValue;

            loadSubSector(selectedSector);
        }

        private void EmailValidationTextBox(object sender, RoutedEventArgs e)
        {
            ((Control)sender).GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }

        private void companyClick(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = (sender as Button).Name.ToString();
                Members_List memberFind;

                if (name == btn_employercode.Name)
                {
                    memberFind = new Members_List(1, this);
                    memberFind.Owner = this;
                    memberFind.ShowDialog();
                }
                else if (name == btn_spouseCode.Name)
                {
                    memberFind = new Members_List(11, this);
                    memberFind.Owner = this;
                    memberFind.ShowDialog();
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files (*.jpg)|*.jpg|All Files(*.*)|*.*";
            open.FilterIndex = 1;
            open.FileName = "ClientPicture";

            if (open.ShowDialog() == true)
            {
                stream = File.Open(open.FileName, FileMode.OpenOrCreate);

                imgsrc.BeginInit();
                imgsrc.CacheOption = BitmapCacheOption.OnLoad;
                imgsrc.StreamSource = stream;
                imgsrc.EndInit();
                imgsrc.Freeze();
                clientImage.Source = imgsrc;

                client.ImageFileName = open.FileName;

            }



        }

        private void clientTypeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ClientTypes clientType = new JavaScriptSerializer().ConvertToType<ClientTypes>(clientTypeCombo.SelectedItem);

            //ClientType clientType = (ClientType)clientTypeCombo.SelectedItem;
            String parsed = new JavaScriptSerializer().Serialize(clientType);

            CRUXLib.Response Response = App.CRUXAPI.request("client/clientTypeSelected", parsed);
            if (Response.Status == "SUCCESS")
            {
                List<ClientTypes> listCompany = new JavaScriptSerializer().Deserialize<List<ClientTypes>>(Response.Content);
                var client = listCompany.FirstOrDefault(a => a.ClientTypeID == clientType.ClientTypeID).ClientTypeID;

                if (client == Convert.ToByte(clientTypes.SpecialDepositor) || client == Convert.ToByte(clientTypes.JuniorDepositor))
                {
                    principalButton.IsEnabled = true;
                    dateofPMES.IsEnabled = false;
                    principalText.Visibility = Visibility;
                }
                else if (client == Convert.ToByte(clientTypes.RegularMember) || client == Convert.ToByte(clientTypes.ProbitionaryMember) || client == Convert.ToByte(clientTypes.RegularMember) || client == Convert.ToByte(clientTypes.TemporaryAccount))
                {
                    if (getbarangay.Count > 0)
                    {
                        if (String.IsNullOrEmpty(getbarangay.First().ClientID.ToString()) && String.IsNullOrEmpty(getbarangay.First().PMESBatch))
                        {
                            dateofPMES.IsEnabled = true;
                            batchNoText.IsReadOnly = false;
                        }
                        else
                        {
                            dateofPMES.IsEnabled = false;
                            batchNoText.IsReadOnly = true;
                        }
                    }
                    else
                    {
                        dateofPMES.IsEnabled = true;
                        batchNoText.IsReadOnly = false;
                    }


                }
                else
                {
                    principalButton.IsEnabled = false;
                    principalText.Visibility = Visibility.Hidden;
                    dateofPMES.IsEnabled = false;

                }

            }

        }

        private void ITFAccountButton_Click(object sender, RoutedEventArgs e)
        {
            Int64 clientid = 0;
            Int64 barangayid = 0;
            if (getbarangay.Count != 0)
            {
                if (!String.IsNullOrWhiteSpace(getbarangay.FirstOrDefault().ClientID.ToString()))
                {
                    clientid = getbarangay.FirstOrDefault().ClientID;
                    barangayid = getbarangay.FirstOrDefault().BranchID;
                }
            }

            //if (displayClientID != 0)
            // {
            Members_Form member_form = new Members_Form(1, clientid, barangayid, this);

            //Members_Form member_form = new Members_Form(1, getbarangay.FirstOrDefault().ClientID, getbarangay.FirstOrDefault().BranchID,this);
            member_form.Owner = this;
            member_form.ShowDialog();
        }

        //private void BeneficiariesButton_Click(object sender, RoutedEventArgs e)
        //{
        //    if (displayClientID != 0)
        //    {
        //        Members_Form member_form = new Members_Form(2, getbarangay.FirstOrDefault().ClientID, getbarangay.FirstOrDefault().BranchID);
        //        member_form.Owner = this;
        //        member_form.ShowDialog();
        //    }

        //}

        private void principalButton_Click(object sender, RoutedEventArgs e)
        {
            var members_find = new Members_Find(22, this);
            members_find.Owner = this;
            members_find.ShowDialog();
            //var members_find = new Members_Find(22, this);
            //members_find.Owner = this;
            //members_find.ShowDialog();
        }

        private void incomeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string name = (sender as ComboBox).Name.ToString();

            if (salaryIcomeCombo.Name == name)
            {
                annualIncomeText.Text = computeSalary(incomeText.Text, salaryIcomeCombo.SelectedIndex).ToString();
                annualIncomeText.Text = string.Format("{0:#,##0.00}", double.Parse(annualIncomeText.Text));
            }
            else if (businessIncomeCombo.Name == name)
            {
                annualBusinessIncomeText.Text = computeSalary(businessIncomeText.Text, businessIncomeCombo.SelectedIndex).ToString();
                annualBusinessIncomeText.Text = string.Format("{0:#,##0.00}", double.Parse(annualBusinessIncomeText.Text));
            }
            else if (spouseSalaryCombo.Name == name)
            {
                spouseAnnualSalaryText.Text = computeSalary(spouseSalaryText.Text, spouseSalaryCombo.SelectedIndex).ToString();
                spouseAnnualSalaryText.Text = string.Format("{0:#,##0.00}", double.Parse(spouseAnnualSalaryText.Text));
            }
            else if (spouseBusinessIncomeCombo.Name == name)
            {
                spouseAnnualBusinessIncomeText.Text = computeSalary(spouseBusinessIncomeText.Text, spouseBusinessIncomeCombo.SelectedIndex).ToString();
                spouseAnnualBusinessIncomeText.Text = string.Format("{0:#,##0.00}", double.Parse(spouseAnnualBusinessIncomeText.Text));
            }
            else if (otherIncomeCombo.Name == name)
            {
                otherAnnualIncomeText.Text = computeSalary(otherIncomeText.Text, otherIncomeCombo.SelectedIndex).ToString();
                otherAnnualIncomeText.Text = string.Format("{0:#,##0.00}", double.Parse(otherAnnualIncomeText.Text));

            }

        }

        public Double computeSalary(String incomeText, Int32 index)
        {
            Double total = 0;
            if (index == 0)
            {
                total = Double.Parse(incomeText) * 52;
            }
            else if (index == 1)
            {
                total = Double.Parse(incomeText) * 12;
            }
            else if (index == 2)
            {
                total = Double.Parse(incomeText) * 4;
            }
            return total;
        }

        private void incomeSummaryText_LostFocus(object sender, RoutedEventArgs e)
        {


        }

        private void incomeText_TextChanged(object sender, TextChangedEventArgs e)
        {
            incomeText.Text = string.Format("{0:#,##0.00}", double.Parse(incomeText.Text));
            //incomeText.Text = incomeText.Text.ToString("0:###,###,##0");
        }

        private void incomeText_PreviewKeyDown(object sender, KeyEventArgs e)
        {

            if (!(e.Key != Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) && e.Key != Key.Decimal)
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.Key != Key.Decimal) && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        public string FirstCharToUpper(string input)
        {
            //if (String.IsNullOrEmpty(input))
            //    MessageBox.Show("Enter ");

            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input);
        }

        private void textBoxNames_LostFocus(object sender, RoutedEventArgs e)
        {
            string textBoxName = (sender as TextBox).Name.ToString();
            string textBoxValue = (sender as TextBox).Text;
            string startingString = textBoxValue.ToLower();
            char[] chars = new[] { ' ' };
            StringBuilder result = new StringBuilder(startingString.Length);
            bool makeUpper = true;
            foreach (var c in startingString)
            {
                if (makeUpper)
                {
                    result.Append(Char.ToUpper(c));
                    makeUpper = false;
                }
                else
                {
                    result.Append(c);
                }
                if (chars.Contains(c))
                {
                    makeUpper = true;
                }
            }

            if (lastNameText.Name == textBoxName)
            {
                lastNameText.Text = result.ToString();
            }
            else if (firstNameText.Name == textBoxName)
            {
                firstNameText.Text = result.ToString();
                //firstNameText.Text = FirstCharToUpper(firstNameText.Text);
            }
            else if (middleNameText.Name == textBoxName)
            {
                middleNameText.Text = result.ToString();
            }
            else if (nickNameText.Name == textBoxName)
            {
                nickNameText.Text = result.ToString();
            }
            else if (spouseLastNameText.Name == textBoxName)
            {
                spouseLastNameText.Text = result.ToString();
            }
            else if (spouseFirstNameText.Name == textBoxName)
            {
                spouseFirstNameText.Text = result.ToString();
            }
            else if (spouseMiddleNameText.Name == textBoxName)
            {
                spouseMiddleNameText.Text = result.ToString();
            }
            else if (suffixSpouseComboBox.Name == textBoxName)
            {
                suffixSpouseComboBox.Text = result.ToString();
            }
            else if (spouseMaidenNameText.Name == textBoxName)
            {
                spouseMaidenNameText.Text = result.ToString();
            }
            else if (fatherLastNameText.Name == textBoxName)
            {
                fatherLastNameText.Text = result.ToString();
            }
            else if (fatherFirstNameText.Name == textBoxName)
            {
                fatherFirstNameText.Text = result.ToString();
            }
            else if (fatherMiddleNameText.Name == textBoxName)
            {
                fatherMiddleNameText.Text = result.ToString();
            }
            else if (motherLastNameText.Name == textBoxName)
            {
                motherLastNameText.Text = result.ToString();
            }
            else if (motherFirstNameText.Name == textBoxName)
            {
                motherFirstNameText.Text = result.ToString();
            }
            else if (motherMaidenNameText.Name == textBoxName)
            {
                motherMaidenNameText.Text = result.ToString();
            }


        }

        public void Executed_Open(object sender, ExecutedRoutedEventArgs e)
        {
            Members_List newsub = new Members_List(3, this, mws);
            newsub.Owner = this;
            newsub.ShowDialog();
        }

        private void DatePicker_LostFocus(object sender, RoutedEventArgs e)
        {
            //dateExpiryDatePicker.
        }

        private void OthersTabItem_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            //loadReasons(); //added - 12-1-2016
            //loadRestrictions();
            //loadDosRi();
        }

        private void btn_new_Click(object sender, RoutedEventArgs e)
        {
            if (btn_new.IsEnabled == true)
            {

                option = true;
                MainGrid.IsEnabled = true;
                btn_save.IsEnabled = true;
                btn_save.Opacity = 100;
                btn_cancel.IsEnabled = true;
                Enable();
                dateOpenedPicker.SelectedDate = system.SystemDate;
                loadNationality();
                Members_New members_new = new Members_New(this);
                members_new.Owner = this;
                members_new.ShowDialog();

                /*if (DG_OtherID.Items.Count == 0)
				{
					otherid.Add(new InsertClientOtherID { OtherID = 1 });
					DG_OtherID.ItemsSource = otherid;
					DG_OtherID.SelectedIndex = otherid.Count - 1;

					DG_OtherID.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
					DG_OtherID.Focus();
					DG_OtherID.CurrentCell = new DataGridCellInfo(DG_OtherID.SelectedItem, DG_OtherID.Columns[0]);
					DG_OtherID.BeginEdit();
				}*/
                btn_find.IsEnabled = false;

                //DG_DOSRI.CanUserAddRows = true;

                //Added BY: Martin 07-07-2018
                insertDOSRIs.Add(new InsertDOSRI());

                DG_DOSRI.ItemsSource = insertDOSRIs;
                DG_DOSRI.IsEnabled = true;
                DG_DOSRI.IsReadOnly = false;

            }
        }

        private void btn_edit_Click(object sender, RoutedEventArgs e)
        {
            if (Lock() == "SUCCESS")
            {
                if (btn_edit.IsEnabled == true)
                {
                    //Lock();
                    option = false;

                    Enable();

                    btn_new.IsEnabled = false;
                    btn_find.IsEnabled = false;
                    btn_edit.IsEnabled = false;
                    titleCombo.Focus();

                    //if (DG_OtherID.Items.Count == 0)
                    //{
                    //	otherid.Add(new InsertClientOtherID { OtherID = 1 });
                    //	DG_OtherID.ItemsSource = otherid;
                    //	DG_OtherID.SelectedIndex = otherid.Count - 1;

                    //	DG_OtherID.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
                    //	DG_OtherID.Focus();
                    //	DG_OtherID.CurrentCell = new DataGridCellInfo(DG_OtherID.SelectedItem, DG_OtherID.Columns[0]);
                    //	DG_OtherID.BeginEdit();
                    //}

                    insertDOSRIs = new List<InsertDOSRI>(); // to do insert existing list 
                    insertDOSRIs.Add(new InsertDOSRI());
                    DG_DOSRI.ItemsSource = insertDOSRIs;
                    DG_DOSRI.IsEnabled = true;
                    DG_DOSRI.IsReadOnly = false;
                }
            }
            else
            {
                string TableName = "tblClient";
                LockRequest lockRequest = new LockRequest()
                {
                    ID = Convert.ToString(displayClientID),
                    TableName = "tblClient"
                };

                String parsedLock = new JavaScriptSerializer().Serialize(lockRequest);
                String parsed = displayClientID.ToString() + ":" + TableName;

                CRUXLib.Response Response = App.CRUXAPI.request("pmesattendance/loaduser", parsed);
                string user = "";
                if (Response.Status == "SUCCESS")
                {

                    List<userInfo> systems = new JavaScriptSerializer().Deserialize<List<userInfo>>(Response.Content);
                    foreach (var item in systems)
                    {
                        user = item.UserName;
                    }
                }
                MessageBox.Show("Client is currently modified by " + user, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            //MainGrid.IsEnabled = true;
            //var members_find = new Members_Find(21, this);
            //members_find.ShowDialog();
        }

        public String Lock()
        {
            String Status = "";

            LockRequest lockRequest = new LockRequest()
            {
                ID = Convert.ToString(displayClientID),
                TableName = "tblClient"
            };

            String parsedLock = new JavaScriptSerializer().Serialize(lockRequest);
            CRUXLib.Response Response = App.CRUXAPI.request("Lock/enable", parsedLock);

            if (Response.Status == "SUCCESS")
            {
                return Status = Response.Content;
            }
            else
            {
                return Status = Response.Content;
            }
        }

        public void Unlock()
        {
            LockRequest lockRequest = new LockRequest()
            {
                ID = Convert.ToString(displayClientID),
                TableName = "tblClient"
            };

            String parsedLock = new JavaScriptSerializer().Serialize(lockRequest);
            CRUXLib.Response Response = App.CRUXAPI.request("Lock/disable", parsedLock);

            if (Response.Status == "SUCCESS")
            {

            }
            else
            {

            }
        }

        public void Enable()
        {
            BasicInfoGrid.IsEnabled = true;
            basicGrid.IsEnabled = true;
            empGrid.IsEnabled = true;
            selfEmpGrid.IsEnabled = true;
            incomeGrid.IsEnabled = true;
            presentGroupBox.IsEnabled = true;
            homeGrid.IsEnabled = true;
            spouseGrid.IsEnabled = true;
            othersGrid.IsEnabled = true;
            AlertGrid.IsEnabled = true;
            ImageGrid.IsEnabled = true;
            SignatureGrid.IsEnabled = true;

            fatherGroupBox.IsEnabled = true;
            motherGroupBox.IsEnabled = true;

            DG_ContactType.IsEnabled = true;
            DG_PrimaryID.IsEnabled = true;
            DG_OtherID.IsEnabled = true;
            DG_Asset.IsEnabled = true;
            DG_Bank.IsEnabled = true;
            DG_Dependent.IsEnabled = true;
            DG_BOD.IsEnabled = true;
            dataGridNotification.IsReadOnly = false;

            addressCountryCombo.IsEnabled = true;
            addressCityCombo.IsEnabled = true;
            addressProvinceCombo.IsEnabled = true;
            addressBrgyCombo.IsEnabled = true;
            addressHouseNoText.IsReadOnly = false;
            addressLivingWithParentsCheckBox.IsEnabled = true;
            addressOwnerIfRentedText.IsReadOnly = false;
            addressOwnershipCombo.IsEnabled = true;
            //addressPostalCodeText.IsReadOnly = false;
            addressSinceDateCombo.IsEnabled = true;
            addressStreetText.IsReadOnly = false;

            homeCountryCombo.IsEnabled = true;
            homeCityCombo.IsEnabled = true;
            homeProvinceCombo.IsEnabled = true;
            homeBrgyCombo.IsEnabled = true;
            homeHouseNoText.IsReadOnly = false;
            homeLivingWithParentsCheckBox.IsEnabled = true;
            homeOwnerIfRentedText.IsReadOnly = false;
            homeOwnershipCombo.IsEnabled = true;
            //addressPostalCodeText.IsReadOnly = false;
            homeSinceDateCombo.IsEnabled = true;
            homeStreetText.IsReadOnly = false;


            MainGrid.IsEnabled = true;
            btn_cancel.IsEnabled = true;
            btn_save.IsEnabled = true;
            btn_save.Opacity = 100;

            OtherButtonsGrid.IsEnabled = true;
            btn_find.Focus();
        }

        public void Disable()
        {
            btn_find.Focus();
            BasicInfoGrid.IsEnabled = false;
            basicGrid.IsEnabled = false;
            empGrid.IsEnabled = false;
            selfEmpGrid.IsEnabled = false;
            incomeGrid.IsEnabled = false;
            //OtherButtonsGrid.IsEnabled = false;
            //presentGroupBox.IsEnabled = false;
            ImageGrid.IsEnabled = false;
            SignatureGrid.IsEnabled = false;

            addressCountryCombo.IsEnabled = false;
            addressCityCombo.IsEnabled = false;
            addressProvinceCombo.IsEnabled = false;
            addressBrgyCombo.IsEnabled = false;
            addressHouseNoText.IsReadOnly = true;
            addressLivingWithParentsCheckBox.IsEnabled = false;
            addressOwnerIfRentedText.IsReadOnly = true;
            addressOwnershipCombo.IsEnabled = false;
            addressPostalCodeText.IsReadOnly = true;
            addressSinceDateCombo.IsEnabled = false;
            addressStreetText.IsReadOnly = true;

            homeCountryCombo.IsEnabled = false;
            homeCityCombo.IsEnabled = false;
            homeProvinceCombo.IsEnabled = false;
            homeBrgyCombo.IsEnabled = false;
            homeHouseNoText.IsReadOnly = true;
            homeLivingWithParentsCheckBox.IsEnabled = false;
            homeOwnerIfRentedText.IsReadOnly = true;
            homeOwnershipCombo.IsEnabled = false;
            homePostalCodeText.IsReadOnly = true;
            homeSinceDateCombo.IsEnabled = false;
            homeStreetText.IsReadOnly = true;



            dateOfEmpFromPicker.IsEnabled = false;
            dateOfEmpUntilPicker.IsEnabled = false;
            companySectorCombo.IsEnabled = false;
            companyPositionCombo.IsEnabled = false;
            companyStatusCombo.IsEnabled = false;
            companyProvinceCombo.IsEnabled = false;



            //homeGrid.IsEnabled = false;
            spouseGrid.IsEnabled = false;
            othersGrid.IsEnabled = false;
            AlertGrid.IsEnabled = false;
            fatherGroupBox.IsEnabled = false;
            motherGroupBox.IsEnabled = false;

            DG_ContactType.IsEnabled = false;
            DG_PrimaryID.IsEnabled = false;
            DG_OtherID.IsEnabled = false;
            DG_Asset.IsEnabled = false;
            DG_Bank.IsEnabled = false;
            DG_Dependent.IsEnabled = false;
            DG_BOD.IsEnabled = false;
            dataGridNotification.IsReadOnly = true;

            //MainGrid.IsEnabled = false;
            // btn_cancel.IsEnabled = false;
            if (String.IsNullOrWhiteSpace(clientIDText.Text))
            {
                btn_edit.IsEnabled = false;
            }

            btn_save.IsEnabled = false;
            btn_save.Opacity = 50;
            btn_find.Focus();
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            Unlock();
            refreshMe();
            clientIDText.Text = "";
            txtAgeUponEntry.Text = "";
            recruitedByTextBox.Text = "";
            dateOfBirthPicker.Text = "";
            ageText.Text = "";
            getbarangay = new List<EditClient>();
            displayClientID = 0;
            OtherButtonsGrid.IsEnabled = false;
            Disable();

            btn_new.IsEnabled = true;
            btn_find.IsEnabled = true;
            clientSignature.Source = null;
            clientImage.Source = null;
            btn_find.Focus();

            //btn_edit.IsEnabled = false;
        }

        private void incomeText_SelectionChanged(object sender, RoutedEventArgs e)
        {
            string name = (sender as TextBox).Name.ToString();

            if (incomeText.Name == name)
            {
                if (incomeText.Text == null || incomeText.Text == "")
                {
                    salaryIcomeCombo.IsEnabled = false;
                }
                else
                    salaryIcomeCombo.IsEnabled = true;
            }
            else if (businessIncomeText.Name == name)
            {
                if (businessIncomeText.Text == null || businessIncomeText.Text == "")
                {
                    businessIncomeCombo.IsEnabled = false;
                }
                else
                    businessIncomeCombo.IsEnabled = true;
            }
            else if (spouseSalaryText.Name == name)
            {
                if (spouseSalaryText.Text == null || spouseSalaryText.Text == "")
                {
                    spouseSalaryCombo.IsEnabled = false;
                }
                else
                    spouseSalaryCombo.IsEnabled = true;
            }
            else if (spouseBusinessIncomeText.Name == name)
            {
                if (spouseBusinessIncomeText.Text == null || spouseBusinessIncomeText.Text == "")
                {
                    spouseBusinessIncomeCombo.IsEnabled = false;
                }
                else
                    spouseBusinessIncomeCombo.IsEnabled = true;
            }
            else if (otherIncomeText.Name == name)
            {
                if (otherIncomeText.Text == null || otherIncomeText.Text == "")
                {
                    otherIncomeCombo.IsEnabled = false;
                }
                else
                    otherIncomeCombo.IsEnabled = true;
            }
        }

        //private void Window_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.F7)
        //    {
        //        if (clientIDText.Text == null || clientIDText.Text == "")
        //        {

        //        }
        //        else
        //        {
        //            // SL_Details sldetailed = new SL_Details(Convert.ToInt64(clientIDText.Text));
        //            //sldetailed.Show();
        //            userInfo userinfo = new userInfo();
        //            userinfo.BranchID = getbarangay.FirstOrDefault().BranchID;
        //            userinfo.ClientID = displayClientID;
        //            String parsed = new JavaScriptSerializer().Serialize(userinfo);
        //            //App.CRUXAPI.openWindow("Client", "SL_Setup.exe", parsed);
        //        }
        //    }

        //}
        public void openModule()
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke
                (
                    DispatcherPriority.Background, new Action(() => Keyboard.Focus(btn_find))
                );
            }
            catch (Exception ex)
            {
            }
        }

        private void F7(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(clientIDText.Text))
            {
                // SL_Details sldetailed = new SL_Details(Convert.ToInt64(clientIDText.Text));
                //sldetailed.Show();
                userInfo userinfo = new userInfo();
                userinfo.BranchID = getbarangay.FirstOrDefault().BranchID;
                userinfo.ClientID = displayClientID;
                String parsed = new JavaScriptSerializer().Serialize(userinfo);
                //App.CRUXAPI.openWindow(new WindowInteropHelper(this).Handle, "Client", "SL_Setup.exe", parsed);
                App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Client", "SL_Setup.exe", this, "Members_Information", parsed);
                //App.CRUXAPI.closeCallBack += openModule;


            }
        }

        private void LoanApps(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(clientIDText.Text))
            {
                // SL_Details sldetailed = new SL_Details(Convert.ToInt64(clientIDText.Text));
                //sldetailed.Show();
                loanUserInfo userinfo = new loanUserInfo();
                userinfo.BranchID = getbarangay.FirstOrDefault().BranchID;
                userinfo.ClientID = displayClientID;
                userinfo.SLC = 0;
                userinfo.SLT = 0;
                userinfo.REF_NO = "";
                userinfo.APPLICATION_NO = "";
                String parsed = new JavaScriptSerializer().Serialize(userinfo);
                //App.CRUXAPI.openWindow(new WindowInteropHelper(this).Handle, "Loans", "LoansApplication.exe", parsed);
                App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Loans", "LoansApplication.exe", this, "Members_Information", parsed);
                //App.CRUXAPI.closeCallBack += openModule;
            }
        }

        private void LoanApproval(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(clientIDText.Text))
            {
                // SL_Details sldetailed = new SL_Details(Convert.ToInt64(clientIDText.Text));
                //sldetailed.Show();
                userInfo userinfo = new userInfo();
                userinfo.BranchID = getbarangay.FirstOrDefault().BranchID;
                userinfo.ClientID = displayClientID;
                String parsed = new JavaScriptSerializer().Serialize(userinfo);
                //App.CRUXAPI.openWindow(new WindowInteropHelper(this).Handle, "Loans", "LoansApproval.exe", parsed);
                App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Loans", "LoansApproval.exe", this, "Members_Information", parsed);
                //App.CRUXAPI.closeCallBack += openModule;
            }
        }

        private void shift_F8(object sender, RoutedEventArgs e)
        {
            userInfo userinfo = new userInfo();
            userinfo.BranchID = getbarangay.FirstOrDefault().BranchID;
            userinfo.ClientID = displayClientID;
            String parsed = new JavaScriptSerializer().Serialize(userinfo);
            //App.CRUXAPI.openWindow(new WindowInteropHelper(this).Handle, "Back Office", "SLAccountSetup.exe", parsed);
            App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Back Office", "SLAccountSetup.exe", this, "Members_Information", parsed);
            //App.CRUXAPI.closeCallBack += openModule;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Unlock();
            //Environment.Exit(Environment.ExitCode);
            this.Close();
        }

        private void IncomeLostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(incomeText.Text))
                {
                    incomeText.Text = string.Format("{0:#,##0.00}", double.Parse(incomeText.Text));
                }
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void incomeText_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(incomeText.Text))
                {
                    incomeText.Text = string.Format("{0:#,##0.00}", double.Parse(incomeText.Text));
                }
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }

            //Double val;
            //if (!String.IsNullOrWhiteSpace(incomeText.Text))
            //{
            //	if (!Double.TryParse(incomeText.Text, out val))
            //	{
            //		MessageBox.Show("Please input numeric character only.", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            //		incomeText.Text = "";
            //		incomeText.Focus();
            //	}
            //	else
            //	{

            //	}
            //}
        }

        private void businessIncomeText_LostFocus(object sender, RoutedEventArgs e)
        {
            Double val;
            if (!Double.TryParse(businessIncomeText.Text, out val))
            {
                MessageBox.Show("Please input numeric character only.", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                businessIncomeText.Text = "";
                businessIncomeText.Focus();
            }
            else
            {
                businessIncomeText.Text = string.Format("{0:#,##0.00}", double.Parse(businessIncomeText.Text));
            }
        }

        private void spouseSalaryText_LostFocus(object sender, RoutedEventArgs e)
        {
            Double val;
            if (!Double.TryParse(spouseSalaryText.Text, out val))
            {
                MessageBox.Show("Please input numeric character only.", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                spouseSalaryText.Text = "";
                spouseSalaryText.Focus();
            }
            else
            {
                spouseSalaryText.Text = string.Format("{0:#,##0.00}", double.Parse(spouseSalaryText.Text));
            }
        }

        private void spouseBusinessIncomeText_LostFocus(object sender, RoutedEventArgs e)
        {
            Double val;
            if (!Double.TryParse(spouseBusinessIncomeText.Text, out val))
            {
                MessageBox.Show("Please input numeric character only.", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                spouseBusinessIncomeText.Text = "";
                spouseBusinessIncomeText.Focus();
            }
            else
            {
                spouseBusinessIncomeText.Text = string.Format("{0:#,##0.00}", double.Parse(spouseBusinessIncomeText.Text));
            }
        }

        private void otherIncomeText_LostFocus(object sender, RoutedEventArgs e)
        {
            Double val;
            if (!Double.TryParse(otherIncomeText.Text, out val))
            {
                MessageBox.Show("Please input numeric character only.", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                otherIncomeText.Text = "";
                otherIncomeText.Focus();
            }
            else
            {
                otherIncomeText.Text = string.Format("{0:#,##0.00}", double.Parse(otherIncomeText.Text));
            }
        }

        private void DG_ContactType_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            var _emp = e.Row.Item as InsertClientContact;

            if (_emp.ContactTypeID == 3)
            {
                if (_emp.ContactNo.Count() > 11 || _emp.ContactNo.Count() < 11)
                {
                    MessageBox.Show("Mobile Number is Invalid", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                    _emp.ContactNo = "";

                }
                else if (_emp.ContactNo.Count() == 11)
                {
                    //Object fck = _emp.ContactNo;
                    _emp.ContactNo = String.Format("{0:(+63)###-#######}", double.Parse(_emp.ContactNo));
                }
            }
        }

        private void dosRiComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedDosri = dosRiComboBox.SelectedItem as DosRi;


            if (dosRiComboBox.SelectedItem != null)
            {
                if (selectedDosri.DosRiID == 4)
                {
                    relatedToTxt.Visibility = Visibility.Visible;
                    relatedlabel.Visibility = Visibility.Visible;



                }
                else
                {
                    relatedToTxt.Visibility = Visibility.Hidden;
                    relatedlabel.Visibility = Visibility.Hidden;
                    relatedToTxt.Text = "";
                }

            }
            else
            {
                relatedToTxt.Visibility = Visibility.Hidden;
                relatedlabel.Visibility = Visibility.Hidden;
                relatedToTxt.Text = "";
            }



        }

        private void dosRiComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                dosRiComboBox.SelectedIndex = -1;
                e.Handled = true;
            }

        }

        private void reasonsComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                reasonsComboBox.SelectedIndex = -1;
                e.Handled = true;
            }
        }

        private void suffixSpouseComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                suffixSpouseComboBox.SelectedIndex = -1;
                e.Handled = true;
            }
        }

        private void suffixComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                suffixComboBox.SelectedIndex = -1;
                e.Handled = true;
            }
        }

        private void reasonsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            if (reasonsComboBox.SelectedItem != null)
            {
                var reason = reasonsComboBox.SelectedItem as Reasons;
                if (reasonsComboBox.SelectedIndex != -1)
                {
                    clientStatusCombo.SelectedValue = 3;
                    dateClosedTxt.Text = system.SystemDate.ToString("MM/dd/yyyy");

                    if (reason.ReasonID == 2)
                    {
                        dateTransferredTxt.Visibility = Visibility.Visible;
                        dateTransferredLabel.Visibility = Visibility.Visible;
                        branchDestCombo.Visibility = Visibility.Visible;
                        destLabel.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        dateTransferredTxt.Visibility = Visibility.Hidden;
                        dateTransferredLabel.Visibility = Visibility.Hidden;
                        dateTransferredTxt.Text = "";
                        branchDestCombo.Visibility = Visibility.Hidden;
                        branchDestCombo.SelectedIndex = -1;
                        destLabel.Visibility = Visibility.Hidden;
                    }
                    if (reason.ReasonID == 6)
                    {
                        dateDeceasedTxt.Visibility = Visibility.Visible;
                        dateDeceasedLabel.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        dateDeceasedTxt.Visibility = Visibility.Hidden;
                        dateDeceasedLabel.Visibility = Visibility.Hidden;
                        dateDeceasedTxt.Text = "";
                    }
                }

            }
        }

        private void clientStatusCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedClientStatus = clientStatusCombo.SelectedItem as ClientStatuses;
            if (clientStatusCombo.SelectedItem != null)
            {
                if (selectedClientStatus.ClientStatusID == 3)
                {
                    if (reasonsComboBox.SelectedItem == null)
                    {
                        dateClosedTxt.Text = system.SystemDate.ToString("MM/dd/yyyy");
                        tabControl.SelectedIndex = 11;
                        othersTabControl.SelectedIndex = 2;
                        MessageBox.Show("Please provide reason for closing the account!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                        reasonsComboBox.Focus();
                    }
                }
                else
                {
                    dateClosedTxt.Text = "";
                }
            }


        }

        private void othersTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (othersTextBox.Text != "")
            {
                var test = string.Format("{0:N2}", Decimal.Parse(othersTextBox.Text));
                othersTextBox.Text = "";
                othersTextBox.Text = test;
            }

        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Unlock();
            if (clientIDText.Text != "" || lastNameText.Text != "")
            {

                refreshMe();
                clientIDText.Text = "";
                txtAgeUponEntry.Text = "";
                recruitedByTextBox.Text = "";
                dateOfBirthPicker.Text = "";
                ageText.Text = "";
                clientImage.Source = null;
                clientSignature.Source = null;
                getbarangay = new List<EditClient>();
                displayClientID = 0;
                //OtherButtonsGrid.IsEnabled = false;
                Disable();

                btn_new.IsEnabled = true;
                btn_find.IsEnabled = true;
                btn_find.Focus();
            }
            else
            {
                Unlock();
                this.Close();
            }

        }

        private void CommandBinding_Executed_1(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            if (displayBranchID != 0 && displayClientID != 0)
            {
                userInfo interest = new userInfo();

                interest.BranchID = displayBranchID;
                interest.ClientID = ClientID;
                String parsed = new JavaScriptSerializer().Serialize(interest);
                //App.CRUXAPI.openWindow(new WindowInteropHelper(this).Handle, "Client", "Statement.exe", parsed);
                App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Client", "Statement.exe", this, "Members_Information", parsed);
                //App.CRUXAPI.closeCallBack += openModule;
            }

        }

        //private void dateOfBirthPicker_LostFocus(object sender, RoutedEventArgs e)
        //{
        //    DateTime currentDate = DateTime.Parse(DateTime.Now.Date.ToShortDateString());
        //    //String SelectedDate = (dateOfBirthPicker.SelectedDate.Equals(null) ? "" : dateOfBirthPicker.SelectedDate.Value.ToString("yyyyMMdd"));

        //    DateTime selecteddate;
        //    string SelectedDate = "";
        //    if (DateTime.TryParseExact(dateOfBirthPicker.Text, "MM/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out selecteddate))
        //    {
        //        SelectedDate = selecteddate.ToString("yyyyMMdd");
        //    }
        //    else
        //    {
        //        return;
        //    }
        //    String CurrentDate = Convert.ToString(currentDate.ToString("yyyyMMdd"));
        //    Int64 getAge = 0;
        //    if (SelectedDate == "")
        //    {
        //        getAge = 0;
        //    }
        //    else
        //    {
        //        getAge = (Convert.ToInt64(CurrentDate) - Convert.ToInt64(SelectedDate)) / 10000;
        //    }


        //    ageText.Text = getAge.ToString();

        //}

        private void dateOfBirthPicker_TextChanged(object sender, TextChangedEventArgs e)
        {
            DateTime currentDate = DateTime.Parse(DateTime.Now.Date.ToShortDateString());
            //String SelectedDate = (dateOfBirthPicker.SelectedDate.Equals(null) ? "" : dateOfBirthPicker.SelectedDate.Value.ToString("yyyyMMdd"));

            DateTime selecteddate;
            string SelectedDate = "";
            if (DateTime.TryParseExact(dateOfBirthPicker.Text, "MM/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out selecteddate))
            {
                SelectedDate = selecteddate.ToString("yyyyMMdd");
            }
            else
            {
                return;
            }
            String CurrentDate = Convert.ToString(currentDate.ToString("yyyyMMdd"));
            Int64 getAge = 0;
            if (SelectedDate == "")
            {
                getAge = 0;
            }
            else
            {
                getAge = (Convert.ToInt64(CurrentDate) - Convert.ToInt64(SelectedDate)) / 10000;
            }


            ageText.Text = getAge.ToString();
        }

        private void CommandBinding_Executed_2(object sender, ExecutedRoutedEventArgs e)
        {
            if (displayClientID != 0)
            {
                ClientActivityParams userinfo = new ClientActivityParams();
                userinfo.BranchCode = getbarangay.FirstOrDefault().BranchID;
                userinfo.ClientID = displayClientID;
                userinfo.ModuleID = 12;
                String parsed = new JavaScriptSerializer().Serialize(userinfo);
                //App.CRUXAPI.openWindow(new WindowInteropHelper(this).Handle, "System", "ActivityLog.exe", parsed);
                App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "System", "AuditLog.exe", this, "Members_Information", parsed);
                //App.CRUXAPI.closeCallBack += openModule;
            }
        }

        private void Enlarge_Image(object sender, ExecutedRoutedEventArgs e)
        {
            if (tabControl1.Height != 600)
            {
                tabControl1.Height = 600;
                tabControl1.Width = 580;
                tabControl1.Margin = new Thickness(0, 0, 0, 0);

            }
            else
            {
                tabControl1.Height = 314;
                tabControl1.Width = 398;
                tabControl1.Margin = new Thickness(186, 242, 0, 0);
            }

        }

        private void Enlarge_Notification(object sender, ExecutedRoutedEventArgs e)
        {


            if (dataGridNotification.Height != 380)
            {
                dataGridNotification.Height = 380;
                dataGridNotification.Width = 1166;
                dataGridNotification.Margin = new Thickness(10, 0.4, 0, 3.8);


            }
            else
            {
                dataGridNotification.Height = 100;
                dataGridNotification.Width = 1166;
                dataGridNotification.Margin = new Thickness(10, 291.6, 0, 3.8);
            }

        }

        private void dataGridNotification_PreviewKeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Insert)
            {
                loadNotifications.Add(new Notification { NotificationID = 1, TR_DATE = (DateTime.Now.ToString("MM/dd/yyyy")), EncodedBy = App.CRUXAPI.User.Username });
                dataGridNotification.ItemsSource = loadNotifications;
                dataGridNotification.SelectedIndex = loadNotifications.Count - 1;
                dataGridNotification.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
                dataGridNotification.Focus();
                dataGridNotification.CurrentCell = new DataGridCellInfo(dataGridNotification.SelectedItem, dataGridNotification.Columns[0]);
                dataGridNotification.BeginEdit();
                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
            }
            if (e.Key == Key.Tab)
            {
                if (dataGridNotification.CurrentColumn.DisplayIndex == 4)
                {
                    e.Handled = true;
                }
            }
            if (e.Key == Key.Delete)
            {
                e.Handled = true;
            }
        }

        private void ShowList(object sender, ExecutedRoutedEventArgs e)
        {
            Int64 clientid = 0;
            Int64 branchid = 0;
            if (getbarangay.Count != 0)
            {
                if (!String.IsNullOrWhiteSpace(getbarangay.FirstOrDefault().ClientID.ToString()))
                {
                    clientid = getbarangay.FirstOrDefault().ClientID;
                    branchid = getbarangay.FirstOrDefault().BranchID;
                }
            }


            Benficiaries ben = new Benficiaries(clientid, branchid, !this.BasicInfoGrid.IsEnabled);
            ben.Owner = this;
            ben.ShowDialog();

        }

        private void ShowITFList(object sender, ExecutedRoutedEventArgs e)
        {
            Int64 clientid = 0;
            Int64 barangayid = 0;
            if (getbarangay.Count != 0)
            {
                if (!String.IsNullOrWhiteSpace(getbarangay.FirstOrDefault().ClientID.ToString()))
                {
                    clientid = getbarangay.FirstOrDefault().ClientID;
                    barangayid = getbarangay.FirstOrDefault().BranchID;
                }
            }

            //if (displayClientID != 0)
            // {
            Members_Form member_form = new Members_Form(1, clientid, barangayid, this);
            member_form.Owner = this;
            member_form.ShowDialog();
            // }
        }

        private void CoMakerWith(object sender, ExecutedRoutedEventArgs e)
        {
            if (displayClientID != 0)
            {
                Co_Maker member_form = new Co_Maker(getbarangay.FirstOrDefault().ClientID, getbarangay.FirstOrDefault().BranchID);
                member_form.Owner = this;
                member_form.ShowDialog();
            }
        }

        public ObservableCollection<InsertClientOtherID> otherid = new ObservableCollection<InsertClientOtherID>();

        private void DG_OtherID_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
            }
            if (e.Key == Key.Tab)
            {
                if (DG_OtherID.CurrentColumn.DisplayIndex == 3)
                {
                    e.Handled = true;
                }
            }
            if (e.Key == Key.Insert)
            {
                //DG_OtherID.Items

                otherid.Add(new Models.InsertClientOtherID());
                if (otherid.Count == 1)
                {
                    e.Handled = true;

                }
                else
                {
                    InsertClientOtherID current = (InsertClientOtherID)DG_OtherID.SelectedItem;

                    if ((current.OtherIDTypeValue == 0))
                    {

                        MessageBox.Show("Please select ID Type", "WARNING!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        DG_OtherID.Focus();
                        DG_OtherID.CurrentCell = new DataGridCellInfo(DG_OtherID.SelectedItem, DG_OtherID.Columns[0]);
                        DG_OtherID.BeginEdit();
                        return;


                    }

                    DateTime IDDateIssuedResult;
                    DateTime IDDateExpiredResult;
                    string IDDateExpired = "";
                    string IDDateIssued = "";
                    if (!DateTime.TryParseExact(current.IDDateIssued, "MMddyyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out IDDateIssuedResult))
                    {                   // e.Handled = true;
                        MessageBox.Show("Date Issued is not valid!", "WARNING!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        DG_OtherID.Focus();
                        DG_OtherID.CurrentCell = new DataGridCellInfo(DG_OtherID.SelectedItem, DG_OtherID.Columns[2]);
                        DG_OtherID.BeginEdit();
                        return;
                    }
                    else if (!DateTime.TryParseExact(current.IDDateExpired, "MMddyyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out IDDateExpiredResult))
                    {
                        // e.Handled = true;                   
                        MessageBox.Show("Date Expiry keydown is not valid!", "WARNING!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        DG_OtherID.Focus();
                        DG_OtherID.CurrentCell = new DataGridCellInfo(DG_OtherID.SelectedItem, DG_OtherID.Columns[3]);
                        DG_OtherID.BeginEdit();
                        return;
                    }
                    if (String.IsNullOrEmpty(current.OtherIDNumber))
                    {
                        MessageBox.Show("ID Number should not be empty", "WARNING!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        DG_OtherID.Focus();
                        DG_OtherID.CurrentCell = new DataGridCellInfo(DG_OtherID.SelectedItem, DG_OtherID.Columns[1]);
                        DG_OtherID.BeginEdit();
                        return;
                    }
                    e.Handled = true;
                    otherid.Add(new InsertClientOtherID { });
                    DG_OtherID.ItemsSource = otherid;
                    DG_OtherID.SelectedIndex = otherid.Count - 1;

                    DG_OtherID.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
                    DG_OtherID.Focus();
                    DG_OtherID.CurrentCell = new DataGridCellInfo(DG_OtherID.SelectedItem, DG_OtherID.Columns[0]);
                    DG_OtherID.BeginEdit();
                    //e.Handled = true;
                    // e.Handled = true;

                }







            }
        }

        //private void Grid_Loaded(object sender, RoutedEventArgs e)
        //{
        //	if (getbarangay.Count > 0)
        //	{
        //		if (getbarangay.First().ClientID > 0)
        //		{
        //			loadNotification();
        //			//// members_list.Close();
        //			foreach (var jefrey in loadNotifications)
        //			{
        //				if (jefrey.IsPrompt)
        //				{
        //					MessageBox.Show(jefrey.Remarks, "Alert!");
        //					lastNameText.Focus();
        //				}
        //			}
        //		}
        //	}
        //}

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            userid = App.CRUXAPI.Arguments;

            if (!String.IsNullOrEmpty(userid))
            {
                ClientParams user = new JavaScriptSerializer().Deserialize<ClientParams>(userid);
                loadclientName(user);
            }
        }

        private void companyEmpNameText_TextChanged(object sender, TextChangedEventArgs e)
        {
            dateOfEmpFromPicker.SelectedDate = null;

            dateOfEmpUntilPicker.SelectedDate = null;

            companySectorCombo.SelectedIndex = -1;
            companyPositionCombo.SelectedIndex = -1;
            companyStatusCombo.SelectedIndex = -1;
            companySectorCombo.SelectedIndex = -1;
            companyProvinceCombo.SelectedIndex = -1;
            companyCityCombo.SelectedIndex = -1;

            companyBrgyCombo.SelectedIndex = -1;
            companyHouseNoText.Text = "";
            companyStreetText.Text = "";
            companyPostalCodeText.Text = "";
            companyTenureshipText.Text = "";

            dateOfEmpFromPicker.IsEnabled = true;
            dateOfEmpUntilPicker.IsEnabled = true;
            companySectorCombo.IsEnabled = true;
            companyPositionCombo.IsEnabled = true;
            companyStatusCombo.IsEnabled = true;
            companyProvinceCombo.IsEnabled = true;

        }

        private void CommandBinding_Executed_3(object sender, ExecutedRoutedEventArgs e)
        {
            tabControl1.SelectedIndex = 0;
            e.Handled = true;
        }

        private void CommandBinding_Executed_4(object sender, ExecutedRoutedEventArgs e)
        {
            tabControl1.SelectedIndex = 1;
            e.Handled = true;
        }

        private void clientTypeCombo_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void clientIDText_Copy_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                //Disable();
                var members_find = new Members_Find(4, this);
                members_find.Owner = this;
                members_find.ShowDialog();
                e.Handled = true;
            }

        }

        private void CommandBinding_Executed_5(object sender, ExecutedRoutedEventArgs e)
        {
            dataGridNotification.Focus();
            dataGridNotification.CurrentCell = new DataGridCellInfo(dataGridNotification.Items[0], dataGridNotification.Columns[0]);
            dataGridNotification.BeginEdit();
        }

        private void AddRefresherIDButton_Click(object sender, RoutedEventArgs e)
        {
            ClientParams _client = new ClientParams();

            _client.ClientID = displayClientID.ToString();
            _client.BranchID = displayBranchID.ToString();

            String parsed = new JavaScriptSerializer().Serialize(_client);
            App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Client", "ClientAccountHistory.exe", this, "Members Information", parsed);
            //App.CRUXAPI.openWindow(new WindowInteropHelper(this).Handle, "Client", "ClientAccountHistory.exe", parsed);
            App.CRUXAPI.closeCallBack += openModule;
            LoadRefresherID(_client);
        }

        private void DG_Refresher_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                ClientParams _client = new ClientParams();
                if (DG_Refresher.SelectedItem == null) return;
                var selectedPerson = DG_Refresher.SelectedItem as ClientHistoryDetails;

                _client.ClientID = selectedPerson.PrevClientID.ToString();
                _client.BranchID = selectedPerson.BranchID.ToString();
                String parsed = new JavaScriptSerializer().Serialize(_client);
                App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Client", "Members Information.exe", this, "Members_Information", parsed);
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void ClientIDTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                var members_find = new Members_Find(23, this);
                members_find.Owner = this;
                members_find.ShowDialog();
                e.Handled = true;
            }
        }

        public System.Windows.Controls.DataGridCell GetDataGridCell(System.Windows.Controls.DataGridCellInfo cellInfo)
        {
            var cellContent = cellInfo.Column.GetCellContent(cellInfo.Item);

            if (cellContent != null)
                return ((System.Windows.Controls.DataGridCell)cellContent.Parent);

            return (null);
        }

        private void DG_DOSRI_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                int col = 0;
                int i = DG_DOSRI.CurrentColumn.DisplayIndex;
                var selectedDOSRI = DG_DOSRI.SelectedItem as Models.DOSRI.InsertDOSRI;

                if (!DG_DOSRI.IsReadOnly)
                {
                    if (e.Key == Key.Space || e.Key == Key.Insert || e.Key == Key.Enter)
                    {
                        if (DG_DOSRI.CurrentColumn.DisplayIndex == 0)
                        {

                            if (e.Key == Key.Space)
                            {
                                DOSRI _dosri = new DOSRI(this, dosRis);
                                _dosri.Owner = this;
                                _dosri.ShowDialog();
                                e.Handled = true;
                            }
                        }
                        else if (DG_DOSRI.CurrentColumn.DisplayIndex == 1)
                        {
                            if (e.Key == Key.Space)
                            {
                                DOSRIAcctType _dosri = new DOSRIAcctType(this, dosriAcctTypes, selectedDOSRI.DosriID);
                                _dosri.Owner = this;
                                _dosri.ShowDialog();
                                e.Handled = true;
                            }
                        }
                        else if (DG_DOSRI.CurrentColumn.DisplayIndex == 2)
                        {
                            if (e.Key == Key.Space)
                            {
                                if (selectedDOSRI.AcctTypeID == 1)
                                {
                                    Members_Find members_find = new Members_Find(23, this);
                                    members_find.Owner = this;
                                    members_find.ShowDialog();
                                }
                                else
                                {
                                    UserFind usersFind = new UserFind(this, defaultBranchID);
                                    usersFind.Owner = this;
                                    usersFind.ShowDialog();

                                }

                                e.Handled = true;
                            }
                            else if (e.Key == Key.Enter)
                            {
                                col += 1;
                                e.Handled = true;
                            }
                        }
                        else if (DG_DOSRI.CurrentColumn.DisplayIndex == 3)
                        {
                            if (e.Key == Key.Enter)
                            {
                                col = 0;
                                e.Handled = true;
                            }
                        }

                        if (e.Key == Key.Insert)
                        {
                            var index = DG_DOSRI.Items.IndexOf(DG_DOSRI.SelectedItem) + 1;

                            insertDOSRIs.Add(new InsertDOSRI());
                            DG_DOSRI.ItemsSource = insertDOSRIs;


                            DG_DOSRI.IsReadOnly = true;
                            DG_DOSRI.Items.Refresh();
                            DG_DOSRI.IsReadOnly = false;

                            MoveFocusToNewRow();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void MoveFocusToNewRow()
        {
            DG_DOSRI.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
            DG_DOSRI.SelectedIndex = DG_DOSRI.Items.IndexOf(DG_DOSRI.SelectedItem) + 1;
            DG_DOSRI.Focus();
            DG_DOSRI.CurrentCell = new DataGridCellInfo(DG_DOSRI.SelectedItem,
               DG_DOSRI.Columns[0]);
        }

        private void othersTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DOSRITab.IsSelected)
            {
                DG_DOSRI.Focus();
                e.Handled = true;
            }
        }
    }
    public class EyeCandy
    {
        #region Image dependency property

        /// <summary>
        /// An attached dependency property which provides an
        /// <see cref="ImageSource" /> for arbitrary WPF elements.
        /// </summary>
        public static readonly DependencyProperty ImageProperty;

        /// <summary>
        /// Gets the <see cref="ImageProperty"/> for a given
        /// <see cref="DependencyObject"/>, which provides an
        /// <see cref="ImageSource" /> for arbitrary WPF elements.
        /// </summary>
        public static ImageSource GetImage(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(ImageProperty);
        }

        /// <summary>
        /// Sets the attached <see cref="ImageProperty"/> for a given
        /// <see cref="DependencyObject"/>, which provides an
        /// <see cref="ImageSource" /> for arbitrary WPF elements.
        /// </summary>
        public static void SetImage(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(ImageProperty, value);
        }

        #endregion

        static EyeCandy()
        {
            //register attached dependency property
            var metadata = new FrameworkPropertyMetadata((ImageSource)null);
            ImageProperty = DependencyProperty.RegisterAttached("Image",
                                                                typeof(ImageSource),
                                                                typeof(EyeCandy), metadata);
        }
    }
}

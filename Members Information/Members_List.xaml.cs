﻿using Members_Information.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using Members_Information.Models.DOSRI;

namespace Members_Information
{
    /// <summary>
    /// Interaction logic for Members_List.xaml
    /// </summary>

    public partial class Members_List : Window
    {
        public Members_New mws;
        private InsertClient client;
        private int dataGridDoubleClick;
        public MainWindow mw;
        public Members_Find mf;

        //ADDED BY: MARTIN 06-08-2018
        int currentRow = 0;
        private ICollectionView MyData;
        string SearchText = string.Empty;
        String DefaultColumn = "";

        public Members_List(int option, MainWindow mww, Members_New mwss)
        {
            InitializeComponent();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.mw = mww;
            this.mws = mwss;
            OptionList(option);
            dataGridDoubleClick = option;

            client = new InsertClient();
            DG_List.DataContext = client;
        }

        public Members_List(int option, MainWindow mww, Members_New mwss, Members_Find mf)
        {
            InitializeComponent();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.mw = mww;
            this.mws = mwss;
            this.mf = mf;
            OptionList(option);
            dataGridDoubleClick = option;

            client = new InsertClient();
            DG_List.DataContext = client;


        }

        public Members_List(int option, MainWindow mww)
        {
            InitializeComponent();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.mw = mww;

            OptionList(option);
            dataGridDoubleClick = option;

            client = new InsertClient();
            DG_List.DataContext = client;


        }

        public int options = 0;
        private void OptionList(int option)
        {
            string CLIENTNAME = "";
            string CLIENTID = "";
            switch (option)
            {

                case 1:
                case 11:
                    options = 1;
                    Company();
                    loadCompanies();
                    break;
                case 2:
                case 21:
                case 22:
                case 23:
                    options = 2;
                    Member();
                    CLIENTID = this.mf.txt_batchnum.Text == "" ? "0" : this.mf.txt_batchnum.Text;
                    CLIENTNAME = this.mf.txt_venue.Text == "" ? "0" : this.mf.txt_venue.Text;
                    Boolean showClosed = this.mf.checkBox_showClosed.IsChecked.Value;
                    Boolean showAll = this.mf.checkBox_showAll.IsChecked.Value;
                    Int64 BranchID = Convert.ToInt64(this.mf.cmb_branchID.SelectedValue);
                    loadMembers(CLIENTID, CLIENTNAME, showClosed, showAll, BranchID);

                    break;
                //           case 22:
                //options = 2;
                //Member();
                //               loadRegularMembers();
                //               break;
                case 3:
                    options = 3;
                    Attendance();
                    string batchNum = this.mws.txt_Batchnum.Text == "" ? "0" : this.mws.txt_Batchnum.Text;
                    string clientName = this.mws.txt_Name.Text == "" ? "0" : this.mws.txt_Name.Text;

                    loadAttendance(batchNum, clientName);
                    break;
                case 4:
                    options = 4;
                    //Attendance();
                    KCASClient();
                    CLIENTID = this.mf.txt_batchnum.Text == "" ? "0" : this.mf.txt_batchnum.Text;
                    CLIENTNAME = this.mf.txt_venue.Text == "" ? "0" : this.mf.txt_venue.Text;
                    loadKCASClient(CLIENTID, CLIENTNAME);
                    break;
            }
        }

        public void Member()
        {
            Title = "Member List";
            DataGridTextColumn dataGridClientID = new DataGridTextColumn();
            dataGridClientID.Header = "Client ID";
            dataGridClientID.Binding = new Binding("ClientIDFormated");

            dataGridClientID.Width = new DataGridLength(20, DataGridLengthUnitType.Star);
            DG_List.Columns.Add(dataGridClientID);


            DataGridTextColumn dataGridClientName = new DataGridTextColumn();
            dataGridClientName.Header = "Client Name";
            dataGridClientName.Binding = new Binding("ClientName");
            dataGridClientName.Width = new DataGridLength(50, DataGridLengthUnitType.Star);
            DG_List.Columns.Add(dataGridClientName);

            DataGridTextColumn dataGridBranchName = new DataGridTextColumn();
            dataGridBranchName.Header = "Branch";
            dataGridBranchName.Binding = new Binding("BranchName");
            dataGridBranchName.Width = new DataGridLength(30, DataGridLengthUnitType.Star);
            DG_List.Columns.Add(dataGridBranchName);

            DataGridTextColumn dataGridClientType = new DataGridTextColumn();
            dataGridClientType.Header = "Client Type";
            dataGridClientType.Binding = new Binding("ClientType");
            dataGridClientType.Width = new DataGridLength(30, DataGridLengthUnitType.Star);
            DG_List.Columns.Add(dataGridClientType);

            DataGridTextColumn dataGridClientOldID = new DataGridTextColumn();
            dataGridClientOldID.Header = "Old Client ID";
            dataGridClientOldID.Binding = new Binding("OldClientID");
            dataGridClientOldID.Width = new DataGridLength(20, DataGridLengthUnitType.Star);
            DG_List.Columns.Add(dataGridClientOldID);
        }

        public void Company()
        {
            Title = "Company List";
            DataGridTextColumn dataGridCompanyID = new DataGridTextColumn();
            dataGridCompanyID.Header = "Company ID";
            dataGridCompanyID.Binding = new Binding("CompanyID");
            DG_List.Columns.Add(dataGridCompanyID);

            DataGridTextColumn dataGridCompanyName = new DataGridTextColumn();
            dataGridCompanyName.Header = "Company Name";
            dataGridCompanyName.Binding = new Binding("CompanyName");
            dataGridCompanyName.Width = new DataGridLength(50, DataGridLengthUnitType.Star);
            DG_List.Columns.Add(dataGridCompanyName);
        }

        public void Attendance()
        {
            Title = "Attendance List";
            DataGridTextColumn dataGridAttendanceID = new DataGridTextColumn();
            dataGridAttendanceID.Header = "";
            dataGridAttendanceID.Binding = new Binding("AttendanceID");
            dataGridAttendanceID.MaxWidth = 0;
            DG_List.Columns.Add(dataGridAttendanceID);

            DataGridTextColumn dataGridSuffix = new DataGridTextColumn();
            dataGridSuffix.Header = "";
            dataGridSuffix.Binding = new Binding("Suffix");
            dataGridSuffix.MaxWidth = 0;
            DG_List.Columns.Add(dataGridSuffix);

            DataGridTextColumn dataGridBatchNumber = new DataGridTextColumn();
            dataGridBatchNumber.Header = "BatchNumber";
            dataGridBatchNumber.Binding = new Binding("Batch");
            DG_List.Columns.Add(dataGridBatchNumber);

            DataGridTextColumn dataGridDateOfPMES = new DataGridTextColumn();
            dataGridDateOfPMES.Header = "Date of PMES";
            dataGridDateOfPMES.Binding = new Binding("DateofPMES");
            dataGridDateOfPMES.Binding.StringFormat = "MM-dd-yyyy";
            DG_List.Columns.Add(dataGridDateOfPMES);

            DataGridTextColumn dataGridName = new DataGridTextColumn();
            dataGridName.Header = "Name";
            dataGridName.Binding = new Binding("Name");
            dataGridName.Width = new DataGridLength(50, DataGridLengthUnitType.Star);
            DG_List.Columns.Add(dataGridName);
        }

        public void KCASClient()
        {
            Title = "KCAS Member's List";
            DataGridTextColumn dataGridClientID = new DataGridTextColumn();
            dataGridClientID.Header = "Client ID";
            dataGridClientID.Binding = new Binding("ClientIDFormated");

            dataGridClientID.Width = new DataGridLength(20, DataGridLengthUnitType.Star);
            DG_List.Columns.Add(dataGridClientID);


            DataGridTextColumn dataGridClientName = new DataGridTextColumn();
            dataGridClientName.Header = "Client Name";
            dataGridClientName.Binding = new Binding("ClientName");
            dataGridClientName.Width = new DataGridLength(50, DataGridLengthUnitType.Star);
            DG_List.Columns.Add(dataGridClientName);

            DataGridTextColumn dataGridBranchName = new DataGridTextColumn();
            dataGridBranchName.Header = "Birth Date";
            dataGridBranchName.Binding = new Binding("BranchName");
            dataGridBranchName.Width = new DataGridLength(18, DataGridLengthUnitType.Star);
            DG_List.Columns.Add(dataGridBranchName);

            //DataGridTextColumn dataGridClientType = new DataGridTextColumn();
            //dataGridClientType.Header = "Client Type";
            //dataGridClientType.Binding = new Binding("ClientType");
            //dataGridClientType.Width = new DataGridLength(30, DataGridLengthUnitType.Star);
            //DG_List.Columns.Add(dataGridClientType);


        }

        public void loadKCASClient(string batchNum, string clientName)
        {
            ClientSearch o2 = new ClientSearch();
            o2.ClientID = 0;
            o2.BatchNum = Convert.ToInt64(batchNum);
            o2.ClientName = clientName;

            String parsed = new JavaScriptSerializer().Serialize(o2);
            CRUXLib.Response Response = App.CRUXAPI.request("client/KCASClient", parsed);
            if (Response.Status == "SUCCESS")
            {
                List<ClientMembers> listAttendace = new JavaScriptSerializer().Deserialize<List<ClientMembers>>(Response.Content);
                //Console.WriteLine(Response.Content);

                DG_List.ItemsSource = listAttendace;
                MyData = CollectionViewSource.GetDefaultView(listAttendace);

            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        public void loadAttendance(string batchNum, string clientName)
        {
            ClientSearch o2 = new ClientSearch();
            o2.ClientID = 0;
            o2.BatchNum = Convert.ToInt64(batchNum);
            o2.ClientName = clientName;

            String parsed = new JavaScriptSerializer().Serialize(o2);
            CRUXLib.Response Response = App.CRUXAPI.request("client/attendance", parsed);
            if (Response.Status == "SUCCESS")
            {
                List<PMESAttendance> listAttendace = new JavaScriptSerializer().Deserialize<List<PMESAttendance>>(Response.Content);
                //Console.WriteLine(Response.Content);

                DG_List.ItemsSource = listAttendace;
                MyData = CollectionViewSource.GetDefaultView(listAttendace);

            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        public void loadMembers(string CLIENTID, string CLIENTNAME, Boolean ShowClosed, Boolean ShowAll, Int64 BranchID)
        {

            ClientSearch o2 = new ClientSearch();
            o2.ClientID = Convert.ToInt64(CLIENTID);
            o2.ClientName = CLIENTNAME;
            o2.ShowClosed = ShowClosed;
            o2.ShowAll = ShowAll;
            o2.BranchID = BranchID;
            String parsed = new JavaScriptSerializer().Serialize(o2);
            CRUXLib.Response Response = App.CRUXAPI.request("client/members", parsed);
            if (Response.Status == "SUCCESS")
            {
                List<ClientMembers> listMembers = new JavaScriptSerializer().Deserialize<List<ClientMembers>>(Response.Content);
                //Console.WriteLine(Response.Content);

                DG_List.ItemsSource = listMembers;
                MyData = CollectionViewSource.GetDefaultView(listMembers);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        public void loadRegularMembers()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("client/regularMembers", "");
            if (Response.Status == "SUCCESS")
            {
                List<ClientMembers> listMembers = new JavaScriptSerializer().Deserialize<List<ClientMembers>>(Response.Content);
                //Console.WriteLine(Response.Content);

                DG_List.ItemsSource = listMembers;
                MyData = CollectionViewSource.GetDefaultView(listMembers);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        public void loadCompanies()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("client/companies", "");
            if (Response.Status == "SUCCESS")
            {
                List<ClientCompanies> listCompanies = new JavaScriptSerializer().Deserialize<List<ClientCompanies>>(Response.Content);
                // Console.WriteLine(Response.Content);

                DG_List.ItemsSource = listCompanies;
                MyData = CollectionViewSource.GetDefaultView(listCompanies);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        private void DG_List_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                selectClient();
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }

        }

        private void DG_List_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    selectClient();
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }

        }

        /// <summary>
        /// For highlighting grid at the same time focusing on textbox
        /// </summary>
        /// Added By: Martin 06-08-2018
        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            TextBox t = sender as TextBox;
            SearchText = t.Text.ToString();
            MyData.Filter = FilterData;

            DG_List.SelectedIndex = 0;
        }

        /// <summary>
        /// Hot keys and shortcut keys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Up)
                {
                    if (currentRow > 0)
                    {
                        DG_List.Focus();

                        int previousIndex = DG_List.SelectedIndex - 1;
                        if (previousIndex < 0) return;
                        currentRow = previousIndex;
                        DG_List.SelectedIndex = previousIndex;
                        DG_List.ScrollIntoView(DG_List.Items[currentRow]);
                    }
                }

                else if (e.Key == Key.Down)
                {
                    if (currentRow < DG_List.Items.Count - 1)
                    {
                        DG_List.Focus();

                        int nextIndex = DG_List.SelectedIndex + 1;
                        if (nextIndex > DG_List.Items.Count - 1) return;
                        currentRow = nextIndex;
                        DG_List.SelectedIndex = nextIndex;
                        DG_List.ScrollIntoView(DG_List.Items[currentRow]);
                    }
                }
                else if (e.Key == Key.Escape)
                {
                    this.Close();
                    e.Handled = true;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void selectClient()
        {

            if (dataGridDoubleClick == 1 || dataGridDoubleClick == 11) // for company
            {
                Object company = DG_List.SelectedItem;
                ClientCompanies o2 = (ClientCompanies)company;

                String parsed = new JavaScriptSerializer().Serialize(o2);


                CRUXLib.Response Response = App.CRUXAPI.request("client/companySelected", parsed);
                if (Response.Status == "SUCCESS")
                {
                    List<ClientCompanies> listCompany = new JavaScriptSerializer().Deserialize<List<ClientCompanies>>(Response.Content);

                    if (dataGridDoubleClick == 1)
                    {

                        var compID = listCompany.FirstOrDefault(a => a.CompanyID == o2.CompanyID).CompanyID;
                        var compName = listCompany.FirstOrDefault(a => a.CompanyID == o2.CompanyID).CompanyName;
                        this.mw.SetCompanyIdName(compID, compName);
                    }
                    else
                    {
                        var companyID = listCompany.FirstOrDefault(a => a.CompanyID == o2.CompanyID).CompanyID;
                        var compName = listCompany.FirstOrDefault(a => a.CompanyID == o2.CompanyID).CompanyName;

                        this.mw.SetSpouseCompanyIdName(companyID, compName);
                    }

                    this.Close();
                }
                else
                {
                    MessageBox.Show(Response.Content);
                }
            }
            else if (dataGridDoubleClick == 2)
            {
                Object client = DG_List.SelectedItem;
                ClientMembers o2 = (ClientMembers)client;


                String parsed = new JavaScriptSerializer().Serialize(o2);

                CRUXLib.Response Response = App.CRUXAPI.request("client/selectedClient", parsed);
                if (Response.Status == "SUCCESS")
                {
                    List<EditClient> listCompany = new JavaScriptSerializer().Deserialize<List<EditClient>>(Response.Content);
                    //Console.WriteLine(Response.Content);
                    this.Close();
                    this.mw.DisplayEditClient(listCompany);
                    this.mw.btn_edit.IsEnabled = true;
                    this.mw.btn_edit.Opacity = 100;
                    mw.clientBeneficiariesList.Clear();
                }
                else
                {
                    MessageBox.Show(Response.Content);
                }
            }
            else if (dataGridDoubleClick == 21)
            {
                Object client = DG_List.SelectedItem;
                ClientMembers o2 = (ClientMembers)client;

                String parsed = new JavaScriptSerializer().Serialize(o2);

                CRUXLib.Response Response = App.CRUXAPI.request("client/selectedClient", parsed);
                if (Response.Status == "SUCCESS")
                {
                    List<EditClient> listCompany = new JavaScriptSerializer().Deserialize<List<EditClient>>(Response.Content);
                    //Console.WriteLine(Response.Content);
                    mw.clientBeneficiariesList.Clear();
                    this.mw.DisplayEditClient(listCompany);
                    this.Close();
                }
                else
                {
                    MessageBox.Show(Response.Content);
                }
            }
            else if (dataGridDoubleClick == 22)
            {
                Object client = DG_List.SelectedItem;
                ClientMembers o2 = (ClientMembers)client;

                String parsed = new JavaScriptSerializer().Serialize(o2);

                CRUXLib.Response Response = App.CRUXAPI.request("client/memberSelected", parsed);
                if (Response.Status == "SUCCESS")
                {
                    List<ClientMembers> listCompany = new JavaScriptSerializer().Deserialize<List<ClientMembers>>(Response.Content);


                    var principalID = listCompany.FirstOrDefault(a => a.ClientID == o2.ClientID).ClientID;
                    this.mw.SetPrincipalID(principalID);
                    this.Close();
                }
                else
                {
                    MessageBox.Show(Response.Content);
                }
            }
            else if (dataGridDoubleClick == 23) //for ClientID DOSRI
            {
                Object client = DG_List.SelectedItem;
                ClientMembers o2 = (ClientMembers)client;

                String parsed = new JavaScriptSerializer().Serialize(o2);

                CRUXLib.Response Response = App.CRUXAPI.request("client/ClientMemberDOSRI", parsed);
                if (Response.Status == "SUCCESS")
                {
                    ClientInfo clientInfo = new JavaScriptSerializer().Deserialize<ClientInfo>(Response.Content);

                    InsertDOSRI insertDosRi = (InsertDOSRI) this.mw.DG_DOSRI.SelectedItem;
                    insertDosRi.AcctIDFormat = clientInfo.ClientIDFormat;
                    insertDosRi.AcctName = clientInfo.Name;
                    insertDosRi.DOSRIBranchID = clientInfo.BranchID;
                    insertDosRi.AcctID = clientInfo.clientId;
                    //var principalID = client.FirstOrDefault(a => a.ClientID == o2.ClientID).ClientID;
                    //this.mw.SetDOSRI(clientInfo);
                    this.Close();
                }
                else
                {
                    MessageBox.Show(Response.Content);
                }
            }
            else if (dataGridDoubleClick == 3)
            {
                Object client = DG_List.SelectedItem;
                PMESAttendance pmes = (PMESAttendance)client;
                String parsed = new JavaScriptSerializer().Serialize(pmes);

                CRUXLib.Response Response = App.CRUXAPI.request("client/selectedPMESAttendant", parsed);
                if (Response.Status == "SUCCESS")
                {
                    List<InsertPMESAttendant> listClient = new JavaScriptSerializer().Deserialize<List<InsertPMESAttendant>>(Response.Content);


                    this.mw.DisplayClient(listClient);
                    this.Close();
                }
                else
                {
                    MessageBox.Show(Response.Content);
                }

            }
            else if (dataGridDoubleClick == 4)
            {
                Object client = DG_List.SelectedItem;
                ClientMembers o2 = (ClientMembers)client;
                MessageBox.Show("Please double check client information.\nClient ID: " + o2.ClientIDFormated + "\nClient Name: " + o2.ClientName + ".", "Warning", MessageBoxButton.OK, MessageBoxImage.Information);
                mw.clientIDText.Text = o2.ClientIDFormated;
                this.Close();
            }

        }

        private bool FilterData(object item)
        {
            if (options == 2)
            {
                var value = (Models.ClientMembers)item;

                if (String.IsNullOrEmpty(value.ClientIDFormated) || String.IsNullOrEmpty(value.BranchName) || String.IsNullOrEmpty(value.ClientType) ||
                    String.IsNullOrEmpty(value.ClientName))
                    return false;

                else
                    return Convert.ToString(value.ClientIDFormated).Contains(SearchText) || value.BranchName.ToLower().StartsWith(SearchText.ToLower()) ||
                         value.ClientType.ToLower().StartsWith(SearchText.ToLower()) || value.ClientName.ToLower().Contains(SearchText.ToLower());

            }
            else if (options == 1)
            {
                var company = (Models.ClientCompanies)item;

                if (String.IsNullOrEmpty(company.CompanyName) || company.CompanyID == 0)
                    return false;

                else
                    return Convert.ToString(company.CompanyID).Contains(SearchText) || company.CompanyName.ToLower().StartsWith(SearchText.ToLower());
            }

            return false;

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                txt_search.Focus();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Members_Information.Models;
using System.Web.Script.Serialization;
using System.Windows.Interop;


namespace Members_Information
{
    /// <summary>
    /// Interaction logic for Co_Maker.xaml
    /// </summary>
    public partial class Co_Maker : Window
    {
        public Co_Maker( Int64 ClientID, Int64 BranchID)
        {
            InitializeComponent();

            ClientSearch o2 = new ClientSearch();
            o2.ClientID = ClientID;
            o2.BranchID = BranchID;
            String parsed = new JavaScriptSerializer().Serialize(o2);
            CRUXLib.Response Response = App.CRUXAPI.request("client/CoMaker", parsed);
            if (Response.Status == "SUCCESS")
            {
                List<CoMakerWith> listAttendace = new JavaScriptSerializer().Deserialize<List<CoMakerWith>>(Response.Content);
                Console.WriteLine(Response.Content);

                dataGrid.ItemsSource = listAttendace;
                


            }
            else
            {
                MessageBox.Show(Response.Content);
            }




        }

        public void comakerDoubleClick(object sender, MouseButtonEventArgs e)
        {
            CoMakerWith comakerwith = (CoMakerWith)dataGrid.SelectedItem;
            
            loanUserInfo userinfo = new loanUserInfo();
            userinfo.BranchID = comakerwith.BranchID;
            userinfo.ClientID = comakerwith.noformatClientID;
            userinfo.SLC = comakerwith.SLC;
            userinfo.SLT = comakerwith.SLT;
            userinfo.REF_NO = comakerwith.AcctNo;
			userinfo.APPLICATION_NO = "";
			String parsed = new JavaScriptSerializer().Serialize(userinfo);
			//App.CRUXAPI.openWindow(new WindowInteropHelper(this).Handle, "Loans", "LoansApplication.exe", parsed);
			App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Loans", "LoansApplication.exe", this, "Members_Information", parsed);
		}

        private void dataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            dataGrid.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }

        private void dataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                CoMakerWith comakerwith = (CoMakerWith)dataGrid.SelectedItem;

                loanUserInfo userinfo = new loanUserInfo();
                userinfo.BranchID = comakerwith.BranchID;
                userinfo.ClientID = comakerwith.noformatClientID;
                userinfo.SLC = comakerwith.SLC;
                userinfo.SLT = comakerwith.SLT;
                userinfo.REF_NO = comakerwith.AcctNo;
				userinfo.APPLICATION_NO = "";
				String parsed = new JavaScriptSerializer().Serialize(userinfo);
				//App.CRUXAPI.openWindow(new WindowInteropHelper(this).Handle, "Loans", "LoansApplication.exe", parsed);
				App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Loans", "LoansApplication.exe", this, "Members_Information", parsed);
				e.Handled = true;
            }
			if (e.Key==Key.Escape)
			{
				this.Close();
			}
        }
    }
}

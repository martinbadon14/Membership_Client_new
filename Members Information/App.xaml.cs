﻿using CRUXLib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Members_Information
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    /// test
    public partial class App : Application
    {
        public static string[] args;
        public static API CRUXAPI = new API();
        protected override void OnStartup(StartupEventArgs e)
        {
            App.args = e.Args;
            List<String> parameters = App.args.ToList<String>();
            try
            {
                CRUXAPI.loadParams(parameters);
            }

            catch (Exception ex)
            {
                //base.OnStartup(e);
                //MainWindow mw = new MainWindow(0);
                //mw.Show();
            }
        }
    }
}

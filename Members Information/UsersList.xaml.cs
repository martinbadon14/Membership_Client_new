﻿using Members_Information.Models;
using Members_Information.Models.DOSRI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Members_Information
{
    /// <summary>
    /// Interaction logic for UsersList.xaml
    /// </summary>
    public partial class UsersList : Window
    {
        MainWindow mw;
        String DefaultColumn = "";
        int currentRow = 0, currentColumn = 1;
        string SearchText = string.Empty;
        private ICollectionView MyData;
        List<ClientInfo> userInfos;

        public UsersList(MainWindow mww, Int64 BranchID, String id, String UserName)
        {
            InitializeComponent();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            userInfos = new List<ClientInfo>();
            FillDataGrid(BranchID, id, UserName);
            this.mw = mww;
            this.DataContext = userInfos;
            SearchTextBox.Focus();
        }

        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Up)
                {
                    if (currentRow > 0)
                    {
                        try
                        {
                            int previousIndex = DG_Users.SelectedIndex - 1;
                            if (previousIndex < 0) return;
                            DG_Users.SelectedIndex = previousIndex;
                            DG_Users.ScrollIntoView(DG_Users.Items[currentRow]);

                        }
                        catch (Exception ex)
                        {
                            e.Handled = true;
                        }
                    }

                }
                else if (e.Key == Key.Down)
                {
                    if (currentRow < DG_Users.Items.Count - 1)
                    {
                        try
                        {
                            int nextIndex = DG_Users.SelectedIndex + 1;
                            if (nextIndex > DG_Users.Items.Count - 1) return;
                            DG_Users.SelectedIndex = nextIndex;
                            DG_Users.ScrollIntoView(DG_Users.Items[currentRow]);
                        }
                        catch (Exception ex)
                        {
                            e.Handled = true;
                        }

                    } // end if (this.SelectedOverride > 0)

                } // end else if (e.Key == Key.Down)

                else if (e.Key == Key.Escape)
                {
                    this.Close();
                    e.Handled = true;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void SelectUser()
        {
            if (DG_Users.SelectedItem == null) return;
            var selectedUser = DG_Users.SelectedItem as ClientInfo;

            InsertDOSRI insertDosRi = (InsertDOSRI)this.mw.DG_DOSRI.SelectedItem;
            insertDosRi.AcctIDFormat = selectedUser.UserIDFormat;
            insertDosRi.AcctName = selectedUser.Name;
            insertDosRi.DOSRIBranchID = selectedUser.BranchID;
            insertDosRi.AcctID = selectedUser.clientId;
            this.Close();
        }

        private void FillDataGrid(Int64 BranchID, String Id, String Name)
        {
            ClientDosRiParams dm = new ClientDosRiParams();
            dm.BranchID = BranchID;
            dm.id = Id;
            dm.name = Name;

            string parsed = "";
            parsed = new JavaScriptSerializer().Serialize(dm);

            CRUXLib.Response Response = App.CRUXAPI.request("client/UsersAccount", parsed);

            if (Response.Status == "SUCCESS")
            {
                userInfos = new JavaScriptSerializer().Deserialize<List<ClientInfo>>(Response.Content);
                DG_Users.ItemsSource = userInfos;

                if (userInfos.Count != 0)
                {
                    DG_Users.SelectedItem = userInfos.FirstOrDefault();
                }

                MyData = CollectionViewSource.GetDefaultView(userInfos);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        private void DG_Users_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                SelectUser();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void DG_Users_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (DG_Users.Items != null)
                    {
                        SelectUser();
                        e.Handled = true;
                    }
                }

            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SearchTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (DG_Users.Items != null)
                    {
                        SelectUser();
                        e.Handled = true;
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {

            }
            catch (Exception)
            {
                e.Handled = true;
                throw;
            }
        }

        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;
            if (cell.Column.DisplayIndex != this.currentColumn)
            {
                cell.IsSelected = false;
            }


            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }

    }
}

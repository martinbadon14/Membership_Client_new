﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class Restrictions
    {
        public Int16 RestrictionID { get; set; }
        public String RestrictionDesc { get; set; }
    }
}

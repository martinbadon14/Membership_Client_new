﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class Barangay
    {
        public Int64 BrgyID { get; set; }
        public Int64 CityID { get; set; }
        public String BarangayDesc { get; set; }
    }
}

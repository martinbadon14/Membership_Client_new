﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class Reasons
    {
        public Int16 ReasonID { get; set; }
        public String ReasonDesc { get; set; }
    }
}

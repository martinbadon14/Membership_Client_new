﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class AccountTypes
    {
        public Int64 AccountTypeID { get; set; }
        public String AccountType { get; set; }
    }
}

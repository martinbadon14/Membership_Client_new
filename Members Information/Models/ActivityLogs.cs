﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class ActivityLogs
    {
        public String dateTimeStamp { get; set; }
        public String Description { get; set; }
        public String UserName { get; set; }
        public String Overrideby { get; set; }
        public String ClientID { get; set; }
        public String ClientName { get; set; }
        public String Field { get; set; }
        public String Info { get; set; }
        public String FromData { get; set; }
        public String ToData { get; set; }
    }
}

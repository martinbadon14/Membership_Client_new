﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class ClientStatuses
    {
        public Int64 ClientStatusID { get; set; }
        public String ClientStatus { get; set; }
    }
}

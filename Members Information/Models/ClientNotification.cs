﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class ClientNotification
    {
        public Int64 NotificationID { get; set; }
        public Int64 NotificationIDs { get; set; }
        public string AssetDescription { get; set; }
        public Int64 ClientID { get; set; }
        public Int64 AssetID { get; set; }
    }

}

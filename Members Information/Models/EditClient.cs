﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Members_Information.Models
{
    public class EditClient : INotifyPropertyChanged
    {

        public Int64 ClientID { get; set; }
        public byte ClientChkID { get; set; }
        public Int64 BranchID { get; set; }
        public Int64 TitleID { get; set; }
		private String firstname;
		public String FirstName
		{
			get { return firstname; }
			set { if (value != firstname) { firstname = value; NotifyPropertyChanged("FirstName"); } }
		}
        public String LastName { get; set; }
        public String MiddleName { get; set; }
        public String Suffix { get; set; }
        public String NickName { get; set; }
        public String DateOpened { get; set; }
        public Byte AccountTypeID { get; set; }
        public Int64 ClientTypeID { get; set; }
        public Int64 ClientStatusID { get; set; }
		public Int16 SuffixID { get; set; }

		public String DateOfBirth { get; set; }
        public Byte GenderID { get; set; }
        public Byte CivilStatusID { get; set; }
        public Int64 EducAttainedID { get; set; }

        public Int64 MunicipalityBirthID { get; set; }
        public Int32 CountryID { get; set; }
        public Int64 ProvinceID { get; set; }

        public Int64 Nationality { get; set; }
        public Int64 ReligionID { get; set; }
        public Int64 BloodTypeID { get; set; }

        public String Height { get; set; }
        public String HeightFT { get; set; }
        public String HeightIN { get; set; }

        public String Weight { get; set; }
        public Byte NoofChildren { get; set; }
        public Byte NoofDependent { get; set; }
        public String DateOfPMES { get; set; }

        public String BatchNumber { get; set; }
        public String PMESBatch { get; set; }
        public String BranchAbbv { get; set; }
        public String PMESDate { get; set; }
        public String BatchNo { get; set; }


        public String DateClosed { get; set; }
        public Int16 ReasonID { get; set; }
        public String ReasonExp { get; set; }
        public Int64 RecruitedBy { get; set; }
        public Int16 DosriID { get; set; }
        public String RelatedTo { get; set; }
        public Int16 RestrictionID { get; set; }
		public Int64 PrincipalID { get; set; }

		public String DateDeceased { get; set; }
        public String DateTransferred { get; set; }
        public Int64 BranchCode { get; set; }

		public Int64 SchoolID { get; set; }
		public Int64 SectionID { get; set; }
		public String OldPSBN { get; set; }

		public List<InsertClientAddress> ClientPresentAddressList { get; set; }
        public List<InsertClientAddress> ClientHomeAddressList { get; set; }
        public List<InsertClientAddressHistory> ClientAddressList { get; set; }
        public List<InsertClientSpouse> ClientSpouseList { get; set; }
        public List<InsertClientParents> ClientMotherParentList { get; set; }
        public List<InsertClientParents> ClientFatherParentList { get; set; }
        public List<InsertClientIncome> ClientIncomeList { get; set; }

        public List<InsertClientContact> ClientContactList { get; set; }
        public List<InsertClientID> ClientIDList { get; set; }
        public List<InsertClientOtherID> ClientOtherIDList { get; set; }
        public List<InsertClientAssets> ClientAssetsList { get; set; }
        public List<InsertClientDependent> ClientDependentsList { get; set; }
        public List<InsertClientBank> ClientBanksList { get; set; }

        public List<InsertClientCompany> ClientCompanyList { get; set; }
		public List<InsertClientCompany> ClientCompanyHistoryList { get; set; }

		//Address
		public Int32 PCountryID { get { if (ClientPresentAddressList.Count > 0) { return ClientPresentAddressList.First().PCountryID; } else return 0; } set { if (value != ClientPresentAddressList.First().PCountryID) { ClientPresentAddressList.First().PCountryID = value; NotifyPropertyChanged("PCountryID"); } } }
        public Int64 PProvinceID
        {
            get
            {
                if (ClientPresentAddressList.Count > 0)
                {
                    return ClientPresentAddressList.FirstOrDefault().PProvinceID;
                }
                else return 0;
            }
            set
            {
               if (value != (ClientPresentAddressList.FirstOrDefault() == null ? 0 : ClientPresentAddressList.FirstOrDefault().PProvinceID))
                {
                    if (ClientPresentAddressList.Count == 0)
                    {
                        ClientPresentAddressList.Add(new InsertClientAddress() { PProvinceID = value }
                        );
                    }

                    ClientPresentAddressList.First().PProvinceID = value;


                }
                NotifyPropertyChanged("PProvinceID");
            }
        }
        public Int64 PMunicipalityID { get { if (ClientPresentAddressList.Count > 0) { return ClientPresentAddressList.First().PMunicipalityID; } else return 0; }
            set {
                if (value != (ClientPresentAddressList.FirstOrDefault() == null ? 0 : ClientPresentAddressList.FirstOrDefault().PMunicipalityID))
                {
                    if (ClientPresentAddressList.Count == 0)
                    {
                        ClientPresentAddressList.Add(new InsertClientAddress() { PMunicipalityID = value }
                        );
                    }

                    ClientPresentAddressList.First().PMunicipalityID = value;


                }
                NotifyPropertyChanged("PMunicipalityID");
            } }

      

        public Int64 PBarangayID
        {
            get { if (ClientPresentAddressList.Count > 0) { return ClientPresentAddressList.First().PBarangayID; } else return 0; }
            set
            {
                if (value != (ClientPresentAddressList.FirstOrDefault() == null ? 0 : ClientPresentAddressList.FirstOrDefault().PBarangayID))
                {
                    if (ClientPresentAddressList.Count == 0)
                    {
                        ClientPresentAddressList.Add(new InsertClientAddress() { PBarangayID = value }
                        );
                    }

                    ClientPresentAddressList.First().PBarangayID = value;


                }
                NotifyPropertyChanged("PBarangayID");
            }
        }
        public string HouseNo { get { if (ClientPresentAddressList.Count > 0) { return ClientPresentAddressList.First().HouseNo; } else return ""; }
            set {
                if (value != (ClientPresentAddressList.FirstOrDefault() == null ? "" : ClientPresentAddressList.FirstOrDefault().HouseNo))
                {
                    if (ClientPresentAddressList.Count == 0)
                    {
                        ClientPresentAddressList.Add(new InsertClientAddress() { HouseNo = value }
                        );
                    }

                    ClientPresentAddressList.First().HouseNo = value;


                }
                NotifyPropertyChanged("HouseNo");
            } }
        public string Street { get { if (ClientPresentAddressList.Count > 0) { return ClientPresentAddressList.First().Street; } else return ""; }
            set
            {
                if (value != (ClientPresentAddressList.FirstOrDefault() == null ? "" : ClientPresentAddressList.FirstOrDefault().Street))
                {
                    if (ClientPresentAddressList.Count == 0)
                    {
                        ClientPresentAddressList.Add(new InsertClientAddress() { Street = value }
                        );
                    }

                    ClientPresentAddressList.First().Street = value;


                }
                NotifyPropertyChanged("Street");
            } }
        public int PostalCode { get { if (ClientPresentAddressList.Count > 0) { return ClientPresentAddressList.First().PostalCode; } else return 0; } set { if (value != ClientPresentAddressList.First().PostalCode) { ClientPresentAddressList.First().PostalCode = value; NotifyPropertyChanged("PostalCode"); } } }
        public int SinceDate { get { if (ClientPresentAddressList.Count > 0) { return ClientPresentAddressList.First().SinceDate; } else return 0; } set { if (value != ClientPresentAddressList.First().SinceDate) { ClientPresentAddressList.First().SinceDate = value; NotifyPropertyChanged("SinceDate"); } } }
        public Boolean LivingwithParents { get { if (ClientPresentAddressList.Count > 0) { return ClientPresentAddressList.First().LivingwithParents; } else return Convert.ToBoolean(null); } set { if (value != ClientPresentAddressList.First().LivingwithParents) { ClientPresentAddressList.First().LivingwithParents = value; NotifyPropertyChanged("LivingwithParents"); } } }
        public int Owned { get { if (ClientPresentAddressList.Count > 0) { return ClientPresentAddressList.First().Owned; } else return 0; } set { if (value != ClientPresentAddressList.First().Owned) { ClientPresentAddressList.First().Owned = value; NotifyPropertyChanged("Owned"); } } }
        public string Owner { get { if (ClientPresentAddressList.Count > 0) { return ClientPresentAddressList.First().Owner; } else return ""; } set { if (value != ClientPresentAddressList.First().Owner) { ClientPresentAddressList.First().Owner = value; NotifyPropertyChanged("Owner"); } } }

        public Int32 HomeCountryID { get { if (ClientHomeAddressList.Count > 0) { return ClientHomeAddressList.First().HomeCountryID; } else return 0; }
            set
            {
                if (value != (ClientHomeAddressList.FirstOrDefault() == null ? 0 : ClientHomeAddressList.FirstOrDefault().HomeCountryID))
                {
                    if (ClientHomeAddressList.Count == 0)
                    {
                        ClientHomeAddressList.Add(new InsertClientAddress() { HomeCountryID = value }
                        );
                    }

                    ClientHomeAddressList.First().HomeCountryID = value;


                }
                NotifyPropertyChanged("HomeCountryID");
            } }
        public Int64 HomeProvinceID { get { if (ClientHomeAddressList.Count > 0) { return ClientHomeAddressList.First().HomeProvinceID; } else return 0; }
            set {
                if (value != (ClientHomeAddressList.FirstOrDefault() == null ? 0 : ClientHomeAddressList.FirstOrDefault().HomeProvinceID))
                {
                    if (ClientHomeAddressList.Count == 0)
                    {
                        ClientHomeAddressList.Add(new InsertClientAddress() { HomeProvinceID = value }
                        );
                    }

                    ClientHomeAddressList.First().HomeProvinceID = value;


                }
                NotifyPropertyChanged("HomeProvinceID");
            } }
        public Int64 HomeMunicipalityID { get { if (ClientHomeAddressList.Count > 0) { return ClientHomeAddressList.First().HomeMunicipalityID; } else return 0; }
            set
            {
                if (value != (ClientHomeAddressList.FirstOrDefault() == null ? 0 : ClientHomeAddressList.FirstOrDefault().HomeMunicipalityID))
                {
                    if (ClientHomeAddressList.Count == 0)
                    {
                        ClientHomeAddressList.Add(new InsertClientAddress() { HomeMunicipalityID = value }
                        );
                    }

                    ClientHomeAddressList.First().HomeMunicipalityID = value;


                }
                NotifyPropertyChanged("HomeMunicipalityID");
            } }
        public Int64 HomeBarangayID { get { if (ClientHomeAddressList.Count > 0) { return ClientHomeAddressList.First().HomeBarangayID; } else return 0; }
            set {
                if (value != (ClientHomeAddressList.FirstOrDefault() == null ? 0 : ClientHomeAddressList.FirstOrDefault().HomeBarangayID))
                {
                    if (ClientHomeAddressList.Count == 0)
                    {
                        ClientHomeAddressList.Add(new InsertClientAddress() { HomeBarangayID = value }
                        );
                    }

                    ClientHomeAddressList.First().HomeBarangayID = value;


                }
                NotifyPropertyChanged("HomeBarangayID");
            } }
        public string HomeHouseNo { get { if (ClientHomeAddressList.Count > 0) { return ClientHomeAddressList.First().HomeHouseNo; } else return ""; }
            set {
                if (value != (ClientHomeAddressList.FirstOrDefault() == null ? "" : ClientHomeAddressList.FirstOrDefault().HomeHouseNo))
                {
                    if (ClientHomeAddressList.Count == 0)
                    {
                        ClientHomeAddressList.Add(new InsertClientAddress() { HomeHouseNo = value }
                        );
                    }

                    ClientHomeAddressList.First().HomeHouseNo = value;


                }
                NotifyPropertyChanged("HomeHouseNo");
            } }
        public string HomeStreet { get { if (ClientHomeAddressList.Count > 0) { return ClientHomeAddressList.First().HomeStreet; } else return ""; }
            set
            {
                if (value != (ClientHomeAddressList.FirstOrDefault() == null ? "" : ClientHomeAddressList.FirstOrDefault().HomeStreet))
                {
                    if (ClientHomeAddressList.Count == 0)
                    {
                        ClientHomeAddressList.Add(new InsertClientAddress() { HomeStreet = value }
                        );
                    }

                    ClientHomeAddressList.First().HomeStreet = value;


                }
                NotifyPropertyChanged("HomeStreet");
            } }
        public int HomePostalCode { get { if (ClientHomeAddressList.Count > 0) { return ClientHomeAddressList.First().HomePostalCode; } else return 0; } set { if (value != ClientHomeAddressList.First().HomePostalCode) { ClientHomeAddressList.First().HomePostalCode = value; NotifyPropertyChanged("HomePostalCode"); } } }
        public int HomeSinceDate { get { if (ClientHomeAddressList.Count > 0) { return ClientHomeAddressList.First().HomeSinceDate; } else return 0; } set { if (value != ClientHomeAddressList.First().HomeSinceDate) { ClientHomeAddressList.First().HomeSinceDate = value; NotifyPropertyChanged("HomeSinceDate"); } } }
        public Boolean HomeLivingwithParents { get { if (ClientHomeAddressList.Count > 0) { return ClientHomeAddressList.First().HomeLivingwithParents; } else return Convert.ToBoolean(null); } set { if (value != ClientHomeAddressList.First().HomeLivingwithParents) { ClientHomeAddressList.First().HomeLivingwithParents = value; NotifyPropertyChanged("HomeLivingwithParents"); } } }
        public int HomeOwned { get { if (ClientHomeAddressList.Count > 0) { return ClientHomeAddressList.First().HomeOwned; } else return 0; } set { if (value != ClientHomeAddressList.First().HomeOwned) { ClientHomeAddressList.First().HomeOwned = value; NotifyPropertyChanged("HomeOwned"); } } }
        public string HomeOwner { get { if (ClientHomeAddressList.Count > 0) { return ClientHomeAddressList.First().HomeOwner; } else return ""; } set { if (value != ClientHomeAddressList.First().HomeOwner) { ClientHomeAddressList.First().HomeOwner = value; NotifyPropertyChanged("HomeOwner"); } } }


        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }


        //Spouse

        public string SpouseLastName
        {
            get
            {
                if (ClientSpouseList.Count() > 0)
                {
                    return ClientSpouseList.First().SpouseLastName;
                }
                else
                    return "";
            }
            set {
                if (value != (ClientSpouseList.FirstOrDefault() == null ? "" : ClientSpouseList.FirstOrDefault().SpouseLastName))
                {
                    if (ClientSpouseList.Count == 0)
                    {
                        ClientSpouseList.Add(new InsertClientSpouse() { SpouseLastName = value }
                        );
                    }

                    ClientSpouseList.First().SpouseLastName = value;


                }
                NotifyPropertyChanged("SpouseLastName");
            }
        }

        public string SpouseFirstName
        {
            get
            {
                if (ClientSpouseList.Count() > 0)
                {
                    return ClientSpouseList.First().SpouseFirstName;
                }
                else
                    return "";
            }
            set {
                if (value != (ClientSpouseList.FirstOrDefault() == null ? "" : ClientSpouseList.FirstOrDefault().SpouseFirstName))
                {
                    if (ClientSpouseList.Count == 0)
                    {
                        ClientSpouseList.Add(new InsertClientSpouse() { SpouseFirstName = value }
                        );
                    }

                    ClientSpouseList.First().SpouseFirstName = value;


                }
                NotifyPropertyChanged("SpouseFirstName");
            }
        }

        public string SpouseMiddleName
        {
            get
            {
                if (ClientSpouseList.Count() > 0)
                {
                    return ClientSpouseList.First().SpouseMiddleName;
                }
                else return "";
            }
            set
            {
                if (value != (ClientSpouseList.FirstOrDefault() == null ? "" : ClientSpouseList.FirstOrDefault().SpouseMiddleName))
                {
                    if (ClientSpouseList.Count == 0)
                    {
                        ClientSpouseList.Add(new InsertClientSpouse() { SpouseMiddleName = value }
                        );
                    }

                    ClientSpouseList.First().SpouseMiddleName = value;


                }
                NotifyPropertyChanged("SpouseMiddleName");
            }
        }
        public Int16 SpouseSuffix { get { if (ClientSpouseList.Count() > 0) { return ClientSpouseList.First().SpouseSuffix; } else return 0; }
            set
            {
                if (value != (ClientSpouseList.FirstOrDefault() == null ? 0 : ClientSpouseList.FirstOrDefault().SpouseSuffix))
                {
                    if (ClientSpouseList.Count == 0)
                    {
                        ClientSpouseList.Add(new InsertClientSpouse() { SpouseSuffix = value }
                        );
                    }

                    ClientSpouseList.First().SpouseSuffix = value;


                }
                NotifyPropertyChanged("SpouseSuffix");
            } }
        public string SpouseNickname { get { if (ClientSpouseList.Count() > 0) { return ClientSpouseList.First().SpouseNickname; } else return ""; }
            set {
                if (value != (ClientSpouseList.FirstOrDefault() == null ? "" : ClientSpouseList.FirstOrDefault().SpouseNickname))
                {
                    if (ClientSpouseList.Count == 0)
                    {
                        ClientSpouseList.Add(new InsertClientSpouse() { SpouseNickname = value }
                        );
                    }

                    ClientSpouseList.First().SpouseNickname = value;


                }
                NotifyPropertyChanged("SpouseNickname");
            } }
        public string SpouseMaidenName { get { if (ClientSpouseList.Count() > 0) { return ClientSpouseList.First().SpouseMaidenName; } else return ""; }
            set {
                if (value != (ClientSpouseList.FirstOrDefault() == null ? "" : ClientSpouseList.FirstOrDefault().SpouseMaidenName))
                {
                    if (ClientSpouseList.Count == 0)
                    {
                        ClientSpouseList.Add(new InsertClientSpouse() { SpouseMaidenName = value }
                        );
                    }

                    ClientSpouseList.First().SpouseMaidenName = value;


                }
                NotifyPropertyChanged("SpouseMaidenName");
            } }
        public Int64 SpouseCompanyID { get { if (ClientSpouseList.Count() > 0) { return ClientSpouseList.First().SpouseCompanyID; } else return 0; }
            set {
                if (value != (ClientSpouseList.FirstOrDefault() == null ? 0 : ClientSpouseList.FirstOrDefault().SpouseCompanyID))
                {
                    if (ClientSpouseList.Count == 0)
                    {
                        ClientSpouseList.Add(new InsertClientSpouse() { SpouseCompanyID = value }
                        );
                    }

                    ClientSpouseList.First().SpouseCompanyID = value;


                }
                NotifyPropertyChanged("SpouseCompanyID");
            } }
        public String SpouseCompanyName { get { if (ClientSpouseList.Count() > 0) { return ClientSpouseList.First().SpouseCompanyName; } else return ""; } set { if (value != ClientSpouseList.First().SpouseCompanyName) { ClientSpouseList.First().SpouseCompanyName = value; NotifyPropertyChanged("SpouseCompanyName"); } } }
        public string SpouseMobileNumber { get { if (ClientSpouseList.Count() > 0) { return ClientSpouseList.First().SpouseMobileNumber; } else return ""; } set { if (value != ClientSpouseList.First().SpouseMobileNumber) { ClientSpouseList.First().SpouseMobileNumber = value; NotifyPropertyChanged("SpouseMobileNumber"); } } }
        public string SpousePhoneNumber { get { if (ClientSpouseList.Count() > 0) { return ClientSpouseList.First().SpousePhoneNumber; } else return ""; } set { if (value != ClientSpouseList.First().SpousePhoneNumber) { ClientSpouseList.First().SpousePhoneNumber = value; NotifyPropertyChanged("SpousePhoneNumber"); } } }
        public string SpouseOfficePhoneNumber { get { if (ClientSpouseList.Count() > 0) { return ClientSpouseList.First().SpouseOfficePhoneNumber; } else return ""; } set { if (value != ClientSpouseList.First().SpouseOfficePhoneNumber) { ClientSpouseList.First().SpouseOfficePhoneNumber = value; NotifyPropertyChanged("SpouseOfficePhoneNumber"); } } }
        public string SpouseEmailAddress { get { if (ClientSpouseList.Count() > 0) { return ClientSpouseList.First().SpouseEmailAddress; } else return ""; } set { if (value != ClientSpouseList.First().SpouseEmailAddress) { ClientSpouseList.First().SpouseEmailAddress = value; NotifyPropertyChanged("SpouseEmailAddress"); } } }
        public string SpouseEmailAddress2 { get { if (ClientSpouseList.Count() > 0) { return ClientSpouseList.First().SpouseEmailAddress2; } else return ""; } set { if (value != ClientSpouseList.First().SpouseEmailAddress2) { ClientSpouseList.First().SpouseEmailAddress2 = value; NotifyPropertyChanged("SpouseEmailAddress2"); } } }
        public String SpouseDateOfBirth { get { if (ClientSpouseList.Count() > 0) { return ClientSpouseList.First().SpouseDateOfBirth; } else return ""; } set { if (value != ClientSpouseList.First().SpouseDateOfBirth) { ClientSpouseList.First().SpouseDateOfBirth = value; NotifyPropertyChanged("SpouseDateOfBirth"); } } }
        public String SpouseOccupation { get { if (ClientSpouseList.Count() > 0) { return ClientSpouseList.First().SpouseOccupation; } else return ""; } set { if (value != ClientSpouseList.First().SpouseOccupation) { ClientSpouseList.First().SpouseOccupation = value; NotifyPropertyChanged("SpouseOccupation"); } } }

        //Parents
        public string MotherLastName { get { if (ClientMotherParentList.Count > 0) { return ClientMotherParentList.First().MotherLastName; } else return ""; }
            set
            {
                if (value != (ClientMotherParentList.FirstOrDefault() == null ? "" : ClientMotherParentList.FirstOrDefault().MotherLastName))
                {
                    if (ClientMotherParentList.Count == 0)
                    {
                        ClientMotherParentList.Add(new InsertClientParents() { MotherLastName = value }
                        );
                    }

                    ClientMotherParentList.First().MotherLastName = value;


                }
                NotifyPropertyChanged("MotherLastName");
            }
        }
        public string MotherFirstName { get { if (ClientMotherParentList.Count > 0) { return ClientMotherParentList.First().MotherFirstName; } else return ""; }
            set
            {
                if (value != (ClientMotherParentList.FirstOrDefault() == null ? "" : ClientMotherParentList.FirstOrDefault().MotherFirstName))
                {
                    if (ClientMotherParentList.Count == 0)
                    {
                        ClientMotherParentList.Add(new InsertClientParents() { MotherFirstName = value }
                        );
                    }

                    ClientMotherParentList.First().MotherFirstName = value;


                }
                NotifyPropertyChanged("MotherFirstName");
            }
        }
        public string MotherMiddleName { get { if (ClientMotherParentList.Count > 0) { return ClientMotherParentList.First().MotherMiddleName; } else return ""; }
            set {
                if (value != (ClientMotherParentList.FirstOrDefault() == null ? "" : ClientMotherParentList.FirstOrDefault().MotherMiddleName))
                {
                    if (ClientMotherParentList.Count == 0)
                    {
                        ClientMotherParentList.Add(new InsertClientParents() { MotherMiddleName = value }
                        );
                    }

                    ClientMotherParentList.First().MotherMiddleName = value;


                }
                NotifyPropertyChanged("MotherMiddleName");
            }
        }
        public Int32 MotherCountryID { get { if (ClientMotherParentList.Count > 0) { return ClientMotherParentList.First().MotherCountryID; } else return 0; }
            set {
                if (value != (ClientMotherParentList.FirstOrDefault() == null ? 0 : ClientMotherParentList.FirstOrDefault().MotherCountryID))
                {
                    if (ClientMotherParentList.Count == 0)
                    {
                        ClientMotherParentList.Add(new InsertClientParents() { MotherCountryID = value }
                        );
                    }

                    ClientMotherParentList.First().MotherCountryID = value;


                }
                NotifyPropertyChanged("MotherCountryID");
            } }
        public Int64 MotherProvinceID { get { if (ClientMotherParentList.Count > 0) { return ClientMotherParentList.First().MotherProvinceID; } else return 0; }
            set {
                if (value != (ClientMotherParentList.FirstOrDefault() == null ? 0 : ClientMotherParentList.FirstOrDefault().MotherProvinceID))
                {
                    if (ClientMotherParentList.Count == 0)
                    {
                        ClientMotherParentList.Add(new InsertClientParents() { MotherProvinceID = value }
                        );
                    }

                    ClientMotherParentList.First().MotherProvinceID = value;


                }
                NotifyPropertyChanged("MotherProvinceID");
            } }
        public Int64 MotherCityID { get { if (ClientMotherParentList.Count > 0) { return ClientMotherParentList.First().MotherCityID; } else return 0; }
            set {
                if (value != (ClientMotherParentList.FirstOrDefault() == null ? 0 : ClientMotherParentList.FirstOrDefault().MotherCityID))
                {
                    if (ClientMotherParentList.Count == 0)
                    {
                        ClientMotherParentList.Add(new InsertClientParents() { MotherCityID = value }
                        );
                    }

                    ClientMotherParentList.First().MotherCityID = value;


                }
                NotifyPropertyChanged("MotherCityID");
            } }
        public Int64 MotherBarangayID { get { if (ClientMotherParentList.Count > 0) { return ClientMotherParentList.First().MotherBarangayID; } else return 0; }
            set {
                if (value != (ClientMotherParentList.FirstOrDefault() == null ? 0 : ClientMotherParentList.FirstOrDefault().MotherBarangayID))
                {
                    if (ClientMotherParentList.Count == 0)
                    {
                        ClientMotherParentList.Add(new InsertClientParents() { MotherBarangayID = value }
                        );
                    }

                    ClientMotherParentList.First().MotherBarangayID = value;


                }
                NotifyPropertyChanged("MotherBarangayID");
            } }
        public string MotherHouseNo { get { if (ClientMotherParentList.Count > 0) { return ClientMotherParentList.First().MotherHouseNo; } else return ""; }
            set {
                if (value != (ClientMotherParentList.FirstOrDefault() == null ? "" : ClientMotherParentList.FirstOrDefault().MotherHouseNo))
                {
                    if (ClientMotherParentList.Count == 0)
                    {
                        ClientMotherParentList.Add(new InsertClientParents() { MotherHouseNo = value }
                        );
                    }

                    ClientMotherParentList.First().MotherHouseNo = value;


                }
                NotifyPropertyChanged("MotherHouseNo");
            } }
        public string MotherStreet { get { if (ClientMotherParentList.Count > 0) { return ClientMotherParentList.First().MotherStreet; } else return ""; }
            set {
                if (value != (ClientMotherParentList.FirstOrDefault() == null ? "" : ClientMotherParentList.FirstOrDefault().MotherStreet))
                {
                    if (ClientMotherParentList.Count == 0)
                    {
                        ClientMotherParentList.Add(new InsertClientParents() { MotherStreet = value }
                        );
                    }

                    ClientMotherParentList.First().MotherStreet = value;


                }
                NotifyPropertyChanged("MotherStreet");
            } }
        public int MotherPostalCode { get { if (ClientMotherParentList.Count > 0) { return ClientMotherParentList.First().MotherPostalCode; } else return 0; } set { if (value != ClientMotherParentList.First().MotherPostalCode) { ClientMotherParentList.First().MotherPostalCode = value; NotifyPropertyChanged("MotherPostalCode"); } } }
        public Int64 MotherSinceYear { get { if (ClientMotherParentList.Count > 0) { return ClientMotherParentList.First().MotherSinceYear; } else return 0; } set { if (value != ClientMotherParentList.First().MotherSinceYear) { ClientMotherParentList.First().MotherSinceYear = value; NotifyPropertyChanged("MotherSinceYear"); } } }


        public string FatherLastName
        {
            get
            {
                if (ClientFatherParentList.Count > 0)
                { return ClientFatherParentList.First().FatherLastName;
                } else return "";
            }
            set
            {
                if (value != (ClientFatherParentList.FirstOrDefault() == null ? "" : ClientFatherParentList.FirstOrDefault().FatherLastName))
                {
                    if (ClientFatherParentList.Count == 0)
                    {
                        ClientFatherParentList.Add(new InsertClientParents() { FatherLastName = value }
                        );
                    }

                    ClientFatherParentList.First().FatherLastName = value;


                }
                NotifyPropertyChanged("FatherLastName");
            }
        }
        public string FatherFirstName { get { if (ClientFatherParentList.Count > 0) { return ClientFatherParentList.First().FatherFirstName; } else return ""; } set
            { if (value != (ClientFatherParentList.FirstOrDefault() == null ? "" : ClientFatherParentList.FirstOrDefault().FatherFirstName))
                {
                    if (ClientFatherParentList.Count==0)
                    {
                        ClientFatherParentList.Add(new InsertClientParents() { FatherFirstName = value}
                        );
                    }
                    
                        ClientFatherParentList.First().FatherFirstName = value;
                        
                    
                }
                NotifyPropertyChanged("FatherFirstName");
            }
        }
        public string FatherMiddleName
        {
            get { if (ClientFatherParentList.Count > 0)
                { return ClientFatherParentList.First().FatherMiddleName;
                } else return "";
            }
            set
            {
                if (value != (ClientFatherParentList.FirstOrDefault() == null ? "" : ClientFatherParentList.FirstOrDefault().FatherMiddleName))
                {
                    if (ClientFatherParentList.Count == 0)
                    {
                        ClientFatherParentList.Add(new InsertClientParents() { FatherMiddleName = value }
                        );
                    }

                    ClientFatherParentList.First().FatherMiddleName = value;


                }
                NotifyPropertyChanged("FatherMiddleName");
            }
        }
        public Int32 FatherCountryID { get { if (ClientFatherParentList.Count > 0) { return ClientFatherParentList.First().FatherCountryID; } else return 0; }
            set {
                if (value != (ClientFatherParentList.FirstOrDefault() == null ? 0 : ClientFatherParentList.FirstOrDefault().FatherCountryID))
                {
                    if (ClientFatherParentList.Count == 0)
                    {
                        ClientFatherParentList.Add(new InsertClientParents() { FatherCountryID = value }
                        );
                    }

                    ClientFatherParentList.First().FatherCountryID = value;


                }
                NotifyPropertyChanged("FatherCountryID");
            } }
        public Int64 FatherProvinceID { get { if (ClientFatherParentList.Count > 0) { return ClientFatherParentList.First().FatherProvinceID; } else return 0; }
            set {
                if (value != (ClientFatherParentList.FirstOrDefault() == null ? 0 : ClientFatherParentList.FirstOrDefault().FatherProvinceID))
                {
                    if (ClientFatherParentList.Count == 0)
                    {
                        ClientFatherParentList.Add(new InsertClientParents() { FatherProvinceID = value }
                        );
                    }

                    ClientFatherParentList.First().FatherProvinceID = value;


                }
                NotifyPropertyChanged("FatherProvinceID");
            } }
        public Int64 FatherCityID { get { if (ClientFatherParentList.Count > 0) { return ClientFatherParentList.First().FatherCityID; } else return 0; }
            set {
                if (value != (ClientFatherParentList.FirstOrDefault() == null ? 0 : ClientFatherParentList.FirstOrDefault().FatherCityID))
                {
                    if (ClientFatherParentList.Count == 0)
                    {
                        ClientFatherParentList.Add(new InsertClientParents() { FatherCityID = value }
                        );
                    }

                    ClientFatherParentList.First().FatherCityID = value;


                }
                NotifyPropertyChanged("FatherCityID");
            } }
        public Int64 FatherBarangayID { get { if (ClientFatherParentList.Count > 0) { return ClientFatherParentList.First().FatherBarangayID; } else return 0; }
            set
            {
                if (value != (ClientFatherParentList.FirstOrDefault() == null ? 0 : ClientFatherParentList.FirstOrDefault().FatherBarangayID))
                {
                    if (ClientFatherParentList.Count == 0)
                    {
                        ClientFatherParentList.Add(new InsertClientParents() { FatherBarangayID = value }
                        );
                    }

                    ClientFatherParentList.First().FatherBarangayID = value;


                }
                NotifyPropertyChanged("FatherBarangayID");
            } }
        public string FatherHouseNo
        {
            get
            { if (ClientFatherParentList.Count > 0)
                { return ClientFatherParentList.First().FatherHouseNo;
                } else return "";
            }
            set {
                if (value != (ClientFatherParentList.FirstOrDefault() == null ? "" : ClientFatherParentList.FirstOrDefault().FatherHouseNo))
                {
                    if (ClientFatherParentList.Count == 0)
                    {
                        ClientFatherParentList.Add(new InsertClientParents() { FatherHouseNo = value }
                        );
                    }

                    ClientFatherParentList.First().FatherHouseNo = value;


                }
                NotifyPropertyChanged("FatherHouseNo");
            }
        }
        public string FatherStreet { get { if (ClientFatherParentList.Count > 0) { return ClientFatherParentList.First().FatherStreet; } else return ""; }
            set {
                if (value != (ClientFatherParentList.FirstOrDefault() == null ? "" : ClientFatherParentList.FirstOrDefault().FatherStreet))
                {
                    if (ClientFatherParentList.Count == 0)
                    {
                        ClientFatherParentList.Add(new InsertClientParents() { FatherStreet = value }
                        );
                    }

                    ClientFatherParentList.First().FatherStreet = value;


                }
                NotifyPropertyChanged("FatherStreet");
            }
        }
        public int FatherPostalCode { get { if (ClientFatherParentList.Count > 0) { return ClientFatherParentList.First().FatherPostalCode; } else return 0; } set { } }
        public Int64 FatherSinceYear { get { if (ClientFatherParentList.Count > 0) { return ClientFatherParentList.First().FatherSinceYear; } else return 0; } set { } }



        //Client Company
        public Int64 EmpCompanyID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpCompanyID; } else return 0; }
            set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? 0 : ClientCompanyList.FirstOrDefault().EmpCompanyID))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { EmpCompanyID = value }
                        );
                    }

                    ClientCompanyList.First().EmpCompanyID = value;


                }
                NotifyPropertyChanged("EmpCompanyID");
            } }
        public String EmpCompanyName { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpCompanyName; } else return ""; }
            set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? "" : ClientCompanyList.FirstOrDefault().EmpCompanyName))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { EmpCompanyName = value }
                        );
                    }

                    ClientCompanyList.First().EmpCompanyName = value;


                }
                NotifyPropertyChanged("EmpCompanyName");
            } }
        public String EmpFrom { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpFrom; } else return ""; } set { } }
        public String EmpUntil { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpUntil; } else return ""; } set { } }
        public Int64 EmpSectorID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpSectorID; } else return 0; } set { } }
        public Int32 EmpPositionID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpPositionID; } else return 0; } set { } }
        public Int16 EmpStatusID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpStatusID; } else return 0; } set { } }
        public Int32 EmpCountryID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpCountryID; } else return 0; } set { } }
        public Int64 EmpProvinceID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpProvinceID; } else return 0; }
            set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? 0 : ClientCompanyList.FirstOrDefault().EmpProvinceID))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { EmpProvinceID = value }
                        );
                    }

                    ClientCompanyList.First().EmpProvinceID = value;


                }
                NotifyPropertyChanged("EmpProvinceID");
            } }
        public Int64 EmpMunicipalityID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpMunicipalityID; } else return 0; }
            set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? 0 : ClientCompanyList.FirstOrDefault().EmpMunicipalityID))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { EmpMunicipalityID = value }
                        );
                    }

                    ClientCompanyList.First().EmpMunicipalityID = value;


                }
                NotifyPropertyChanged("EmpMunicipalityID");
            } }
        public Int64 EmpBarangayID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpBarangayID; } else return 0; }
            set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? 0 : ClientCompanyList.FirstOrDefault().EmpBarangayID))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { EmpBarangayID = value }
                        );
                    }

                    ClientCompanyList.First().EmpBarangayID = value;


                }
                NotifyPropertyChanged("EmpBarangayID");
            } }
        //public string EmpBuildingNo { get; set; }
        public string EmpHouseNo { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpHouseNo; } else return ""; }
            set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? "" : ClientCompanyList.FirstOrDefault().EmpHouseNo))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { EmpHouseNo = value }
                        );
                    }

                    ClientCompanyList.First().EmpHouseNo = value;


                }
                NotifyPropertyChanged("EmpHouseNo");
            }
        }
        public string EmpStreet { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpStreet; } else return ""; }
            set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? "" : ClientCompanyList.FirstOrDefault().EmpStreet))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { EmpStreet = value }
                        );
                    }

                    ClientCompanyList.First().EmpStreet = value;


                }
                NotifyPropertyChanged("EmpStreet");
            }
        }
        public string EmpPostalCode { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpPostalCode; } else return ""; }
            set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? "" : ClientCompanyList.FirstOrDefault().EmpPostalCode))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { EmpPostalCode = value }
                        );
                    }

                    ClientCompanyList.First().EmpPostalCode = value;


                }
                NotifyPropertyChanged("EmpPostalCode");
            }
        }
        public Int16 EmpTenureship { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().EmpTenureship; } else return 0; }
            set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? 0 : ClientCompanyList.FirstOrDefault().EmpTenureship))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { EmpTenureship = value }
                        );
                    }

                    ClientCompanyList.First().EmpTenureship = value;


                }
                NotifyPropertyChanged("EmpTenureship");
            }
        }

        public Int64 SelfEmpSectorID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().SelfEmpSectorID; } else return 0; } set { } }
        public Int64 SelfEmpSubSectorID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().SelfEmpSubSectorID; } else return 0; } set { } }
        public string SelfEmpOccupation { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().SelfEmpOccupation; } else return ""; } set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? "" : ClientCompanyList.FirstOrDefault().SelfEmpOccupation))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { SelfEmpOccupation = value }
                        );
                    }

                    ClientCompanyList.First().SelfEmpOccupation = value;


                }
                NotifyPropertyChanged("SelfEmpOccupation");
            } }
        public string SelfEmpBusinessName { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().SelfEmpBusinessName; } else return ""; } set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? "" : ClientCompanyList.FirstOrDefault().SelfEmpBusinessName))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { SelfEmpBusinessName = value }
                        );
                    }

                    ClientCompanyList.First().SelfEmpBusinessName = value;


                }
                NotifyPropertyChanged("SelfEmpBusinessName");
            } }
        public Int64 SelfEmpLineOfBusiness { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().SelfEmpLineOfBusiness; } else return 0; } set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? 0: ClientCompanyList.FirstOrDefault().SelfEmpLineOfBusiness))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { SelfEmpLineOfBusiness = value }
                        );
                    }

                    ClientCompanyList.First().SelfEmpLineOfBusiness = value;


                }
                NotifyPropertyChanged("SelfEmpLineOfBusiness");

            } }
        public Int32 SelfEmpCountryID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().SelfEmpCountryID; } else return 0; }
            set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? 0 : ClientCompanyList.FirstOrDefault().SelfEmpCountryID))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { SelfEmpCountryID = value }
                        );
                    }

                    ClientCompanyList.First().SelfEmpCountryID = value;


                }
                NotifyPropertyChanged("SelfEmpCountryID");
            } }
        public Int64 SelfEmpProvinceID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().SelfEmpProvinceID; } else return 0; }
            set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? 0 : ClientCompanyList.FirstOrDefault().SelfEmpProvinceID))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { SelfEmpProvinceID = value }
                        );
                    }

                    ClientCompanyList.First().SelfEmpProvinceID = value;


                }
                NotifyPropertyChanged("SelfEmpProvinceID");
            } }
        public Int64 SelfEmpMunicipalityID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().SelfEmpMunicipalityID; } else return 0; }
            set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? 0 : ClientCompanyList.FirstOrDefault().SelfEmpMunicipalityID))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { SelfEmpMunicipalityID = value }
                        );
                    }

                    ClientCompanyList.First().SelfEmpMunicipalityID = value;


                }
                NotifyPropertyChanged("SelfEmpMunicipalityID");
            } }
        public Int64 SelfEmpBarangayID { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().SelfEmpBarangayID; } else return 0; }
            set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? 0 : ClientCompanyList.FirstOrDefault().SelfEmpBarangayID))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { SelfEmpBarangayID = value }
                        );
                    }

                    ClientCompanyList.First().SelfEmpBarangayID = value;


                }
                NotifyPropertyChanged("SelfEmpBarangayID");
            } }
        public string SelfEmpHouseNo { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().SelfEmpHouseNo; } else return ""; } set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? "" : ClientCompanyList.FirstOrDefault().SelfEmpHouseNo))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { SelfEmpHouseNo = value }
                        );
                    }

                    ClientCompanyList.First().SelfEmpHouseNo = value;


                }
                NotifyPropertyChanged("SelfEmpHouseNo");
            } }
        public string SelfEmpStreet { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().SelfEmpStreet; } else return ""; } set {
                if (value != (ClientCompanyList.FirstOrDefault() == null ? "" : ClientCompanyList.FirstOrDefault().SelfEmpStreet))
                {
                    if (ClientCompanyList.Count == 0)
                    {
                        ClientCompanyList.Add(new InsertClientCompany() { SelfEmpStreet = value }
                        );
                    }

                    ClientCompanyList.First().SelfEmpStreet = value;


                }
                NotifyPropertyChanged("SelfEmpStreet");
            } }
        public String SelfEmpPostalCode { get { if (ClientCompanyList.Count > 0) { return ClientCompanyList.First().SelfEmpPostalCode; } else return ""; } set { } }

    }
    public class Notification
    {
		public Int64 ClientID { get; set; }
		public Int64 BranchID { get; set; }
		public Int64 NotificationIDs { get; set; }
		public Int64 NotificationID { get; set; }
		public String NotificationStatus { get; set; }
		public String TR_DATE { get; set; }
		public Int64 UserID { get; set; }
		public String EncodedBy { get; set; }
		public String Remarks { get; set; }
		public Boolean IsPrompt { get; set; }
		public String TextDecoration { get; set; }

	}
    public class notificationStatus
    {
        public Int64 NotificationID { get; set; }
        public String NotificationStatus { get; set; }

    }
}

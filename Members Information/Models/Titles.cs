﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class Titles : INotifyPropertyChanged
    {
        public Int64 titleID;
        public Int64 TitleID { get { return titleID; } set { if (value != titleID) { titleID = value; NotifyPropertyChanged("TitleID"); } } }
        public String Title { get; set; }


        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
   

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
	class AlertLevelHistory
	{
		public String AlertLevelID { get; set; }
		public String AlertLevelDesc { get; set; }
		public String UserName { get; set; }
		public String DateTimeStamp { get; set; }
		public String ClientID { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class SystemDateClass
    {
        public DateTime SystemDate { get; set; }
        public String UserName { get; set; }
    }
}

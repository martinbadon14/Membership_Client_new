﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class SubSectors
    {
        public Int64 SubSectorID { get; set; }
        public string SubSector { get; set; }
    }
}

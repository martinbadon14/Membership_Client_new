﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class IDSecondary
    {
        public Int16 IDTypeValue { get; set; }
        public string IDTypeDesc { get; set; }
    }
}

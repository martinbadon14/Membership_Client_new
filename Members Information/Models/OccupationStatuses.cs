﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class OccupationStatuses
    {
        public Int16 OccupationStatusID { get; set; }
        public String OccupationStatus { get; set; }
    }
}

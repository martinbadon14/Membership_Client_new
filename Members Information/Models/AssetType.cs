﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class AssetType
    {
        public Int16 AssetTypeID { get; set; }
        public String AssetTypeDesc { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class Suffix
    {
        public Int16 SuffixID { get; set; }
        public String SuffixDesc { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class ContactTypes
    {
        public int ContactTypeID { get; set; }
        public string ContactType { get; set; }
    }
}

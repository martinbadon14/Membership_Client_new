﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class ClientMembers
    {
        public string ClientType { get; set; }
        public Int64 ClientID { get; set; }
        public string TitleName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
		public string Suffix { get; set; }
		public string ClientIDFormated { get; set; }
        public string ClientName { get { return string.Format("{0}, {1} {2} {3}", LastName, FirstName, MiddleName, Suffix); } }
        public string BranchName { get; set; }
        public string RowColor { get; set; }
		public Int64 BranchID { get; set; }
		public string SelectRowColor { get; set; }
		public String OldClientID { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class ClientSearch
    {
        public Int64 BatchNum { get; set; }
        public Int64 ClientID { get; set; }
        public string ClientName { get; set; }
        public Int64  BranchID { get; set; }
        public Boolean ShowAll { get; set; }
        public Boolean ShowClosed { get; set; }


		public Int32 ModuleID { get; set; }
		public Int32 TR_Code { get; set; }
		public Int32 TR_CTLNO { get; set; }
		public Int32 TR_Date { get; set; }

	}

    public class ClientActivityParams
    {
        public Int64 ClientID { get; set; }
        public Int64 BranchCode { get; set; }
        public Int32 ModuleID { get; set; }
    }
}

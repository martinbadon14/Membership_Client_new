﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class Education
    {
        
        public Int64 EducAttainmentID { get; set; }
        public String EducationalAttainment { get; set; }
    }
}

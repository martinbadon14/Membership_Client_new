﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class ProvinceAndCityCombo
    {
        public Int64 ProvinceID { get; set; }
        public Int64 PProvinceID { get; set; }
        public Int64 HomeProvinceID { get; set; }
        public Int64 MotherProvinceID { get; set; }
        public Int64 FatherProvinceID { get; set; }
        public Int64 EmpProvinceID { get; set; }
        public Int64 EmpSelfProvinceID  { get; set; }

        public Int64 CityID { get; set; }
        public Int64 PCityID { get; set; }
        public Int64 HomeCityID { get; set; }
        public Int64 SpouseCityID { get; set; }
        public Int64 MotherCityID { get; set; }
        public Int64 FatherCityID { get; set; }
        public Int64 EmpCityID { get; set; }
        public Int64 EmpSelfCityID { get; set; }
    }
}

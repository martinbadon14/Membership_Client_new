﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class Countries
    {
        public Int64 CountryID { get; set; }
        public String CountryDescription { get; set; }
    }
}

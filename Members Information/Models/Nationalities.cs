﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class Nationalities
    {
        public Int64 NationalityID { get; set; }
        public String Nationality { get; set; }
    }
}

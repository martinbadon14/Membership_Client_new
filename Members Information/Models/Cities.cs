﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class Cities
    {
        public Int64 CityID { get; set; }
        public Int64 ProvinceID { get; set; }
        public String City { get; set; }
        public String ZipCode { get; set; }
    }
}

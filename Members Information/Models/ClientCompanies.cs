﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class ClientCompanies
    {
        public Int64  CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
    }
}

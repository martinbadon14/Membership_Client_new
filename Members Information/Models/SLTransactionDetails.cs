﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class SLTransactionDetails
    {
        public String Ref_No { get; set; }
        public Byte SLC_Code { get; set; }
        public Byte SLT_Code { get; set; }
        public Byte SLE_Code { get; set; }
        public String Description { get; set; }
        public String SLDate { get; set; }
        public String Amt { get; set; }
        public String Balance { get; set; }
        public String ClearedBalance { get; set; }
    }
    class SLDetails
    {
        public string TransTypeCode { get; set; }
        public Int32 DocNo { get; set; }
        public String TransactionDate { get; set; }    
        public Decimal Debit { get; set; }
        public Decimal Credit { get; set; }
        public Decimal Balance { get; set; }
        public Decimal ClearedBalance { get; set; }
        public Decimal UnclearedAmt { get; set; }
        public Int32 CTLNo { get; set; }      
    }
}

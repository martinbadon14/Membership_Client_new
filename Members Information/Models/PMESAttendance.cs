﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class PMESAttendance
    {
        
        public String AttendanceID { get; set; }
        public String LName { get; set; }
        public String FName { get; set; }
        public String MName { get; set; }
        public string Name { get { return string.Format("{0}, {1} {2}", LName, FName, MName); } }
        public String Suffix { get; set; }
        public String Address { get; set; }
        public DateTime Birthday { get; set; }
        public String MobileNumber { get; set; }
        public String Amount { get; set; }
        public String PMESBatch { get; set; }
        public DateTime DateofPMES { get; set; }
        public String BranchAbbv { get; set; }
        public String PMESDate { get; set; }
        public Int64 BranchID { get; set; }
        public String Batch { get { return string.Format("{0} {1} - {2}", PMESDate, BranchAbbv, PMESBatch); } }

    }
}

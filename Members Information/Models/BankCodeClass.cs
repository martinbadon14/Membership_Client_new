﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
	class BankCodeClass
	{
		public Int64 BankID { get; set; }
		public String BankDesc { get; set; }
		public Int16 BranchCode { get; set; }
		public String BankDesc2 { get; set; }
		public String AccountNo { get; set; }
		public String COAID { get; set; }
	}
}

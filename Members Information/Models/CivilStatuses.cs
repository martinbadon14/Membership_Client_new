﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class CivilStatuses
    {
        public Int64 CivilStatusID { get; set; }
        public String CivilStatus { get; set; }
    }
}

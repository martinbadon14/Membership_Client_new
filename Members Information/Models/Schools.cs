﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
	class Schools
	{
		public Int64 SchoolID { get; set; }
		public String SchoolDesc { get; set; }
	}

	class Sections
	{
		public Int64 SectionID { get; set; }
		public String SectionDesc { get; set; }
	}
}

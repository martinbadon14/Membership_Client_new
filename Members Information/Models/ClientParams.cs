﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class ClientParams
    {
        public String BranchID { get; set; }
        public String ClientID { get; set; }
    }
}

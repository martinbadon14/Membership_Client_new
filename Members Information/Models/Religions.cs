﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class Religions
    {
        public Int64 ReligionID { get; set; }
        public String Religion { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class BloodTypes
    {
        public Int64 BloodTypeID { get; set; }
        public String BloodType { get; set; }
    }
}

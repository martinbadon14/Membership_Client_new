﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class InsertPMESAttendant
    {
        public String AttendanceID { get; set; }
        public String LastName { get; set; }
        public String FirstName { get; set; }
        public String MiddleName { get; set; }
        public string Name { get { return string.Format("{0} {1} {2}", LastName, FirstName, MiddleName); } }
        public Int16 SuffixID { get; set; }
        public String BatchNo { get; set; }
        public String PMESBatch { get; set; }
        public String BranchAbbv { get; set; }
        public String PMESDate { get; set; }
        public Int64 BranchID { get; set; }
        public String Batch { get { return string.Format("{0} {1} - {2}", PMESDate, BranchAbbv, PMESBatch); } }
        public String BirthDate { get; set; }
        public String DateOfPMES { get; set; }
		public String SystemDate { get; set; }
	}
}

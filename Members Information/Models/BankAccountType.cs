﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class BankAccountType
    {
        public Int16 BankAcctTypeID { get; set; }
        public String BankAcctTypeDesc { get; set; }
    }
}

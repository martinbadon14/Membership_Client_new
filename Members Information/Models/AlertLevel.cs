﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
	class AlertLevel
	{
		public Int16 AlertLevelID { get; set; }
		public String AlertLevelDesc { get; set; }
	}
}

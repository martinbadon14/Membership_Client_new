﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class InsertClientDocuments
    {
        public Int64 ClientDocumentID { get; set; }
        public string DocumentType { get; set; }
        public string DocumentName { get; set; }
        public DateTime DateFiled { get; set; }
        public string  Status { get; set; }
        public String DocumentPath { get; set; }
        public Byte[] DocumentData { get; set; }
        public Int64 ClientID { get; set; }
    }
}

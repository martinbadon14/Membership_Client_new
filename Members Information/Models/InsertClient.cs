﻿using Members_Information.Models.DOSRI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class InsertClient : INotifyPropertyChanged
    {
        private Int64 branchID;
        public Int64 BranchID { get { return branchID; } set { if (value != branchID) { branchID = value; NotifyPropertyChanged("BranchID"); } } }

        private Int64 titleID;
        public Int64 TitleID { get { return titleID; } set { if (value != titleID) { titleID = value; NotifyPropertyChanged("TitleID"); } } }

        private String firstName;
        public String FirstName { get { return firstName; } set { if (value != firstName) { firstName = value; NotifyPropertyChanged("FirstName"); } } }

        private String lastName;
        public String LastName { get { return lastName; } set { if (value != lastName) { lastName = value; NotifyPropertyChanged("LastName"); } } }

        private String middleName;
        public String MiddleName { get { return middleName; } set { if (value != middleName) { middleName = value; NotifyPropertyChanged("MiddleName"); } } }

        private Int16 suffixID;
        public Int16 SuffixID { get { return suffixID; } set { if (value != suffixID) { suffixID = value; NotifyPropertyChanged("SuffixID"); } } }

        private String nickName;
        public String NickName { get { return nickName; } set { if (value != nickName) { nickName = value; NotifyPropertyChanged("NickName"); } } }

        private String dateOpened;
        public String DateOpened { get { return dateOpened; } set { if (value != dateOpened) { dateOpened = value; NotifyPropertyChanged("DateOpened"); } } }

        private Int64 accountTypeID;
        public Int64 AccountTypeID { get { return accountTypeID; } set { if (value != accountTypeID) { accountTypeID = value; NotifyPropertyChanged("AccountTypeID"); } } }

        private Byte clientTypeID;
        public Byte ClientTypeID { get { return clientTypeID; } set { if (value != clientTypeID) { clientTypeID = value; NotifyPropertyChanged("ClientTypeID"); } } }

        private Int64 clientStatusID;
        public Int64 ClientStatusID { get { return clientStatusID; } set { if (value != clientStatusID) { clientStatusID = value; NotifyPropertyChanged("ClientStatusID"); } } }

        private String dateOfBirth;
        public String DateOfBirth { get { return dateOfBirth; } set { if (value != dateOfBirth) { dateOfBirth = value; NotifyPropertyChanged("DateOfBirth"); } } }

        private Int64 genderID;
        public Int64 GenderID { get { return genderID; } set { if (value != genderID) { genderID = value; NotifyPropertyChanged("GenderID"); } } }

        private Int64 civilStatusID;
        public Int64 CivilStatusID { get { return civilStatusID; } set { if (value != civilStatusID) { civilStatusID = value; NotifyPropertyChanged("CivilStatusID"); } } }

        private Int64 educAttainedID;
        public Int64 EducAttainedID { get { return educAttainedID; } set { if (value != educAttainedID) { educAttainedID = value; NotifyPropertyChanged("EducAttainedID"); } } }

        private Int64 municipalityBirthID;
        public Int64 MunicipalityBirthID { get { return municipalityBirthID; } set { if (value != municipalityBirthID) { municipalityBirthID = value; NotifyPropertyChanged("MunicipalityBirthID"); } } }

        private Int64 nationality;
        public Int64 Nationality { get { return nationality; } set { if (value != nationality) { nationality = value; NotifyPropertyChanged("Nationality"); } } }

        private Int64 religionID;
        public Int64 ReligionID { get { return religionID; } set { if (value != religionID) { religionID = value; NotifyPropertyChanged("ReligionID"); } } }

        private Int64 bloodTypeID;
        public Int64 BloodTypeID { get { return bloodTypeID; } set { if (value != bloodTypeID) { bloodTypeID = value; NotifyPropertyChanged("BloodTypeID"); } } }

        private String height;
        public String Height { get { return height; } set { if (value != height) { height = value; NotifyPropertyChanged("Height"); } } }

        private String weight;
        public String Weight { get { return weight; } set { if (value != weight) { weight = value; NotifyPropertyChanged("Weight"); } } }

        private Int64 noOfChildren;
        public Int64 NoofChildren { get { return noOfChildren; } set { if (value != noOfChildren) { noOfChildren = value; NotifyPropertyChanged("NoofChildren"); } } }

        private Int64 noofDependent;
        public Int64 NoofDependent { get { return noofDependent; } set { if (value != noofDependent) { noofDependent = value; NotifyPropertyChanged("NoofDependent"); } } }

        private String batchNo;
        public String BatchNo { get { return batchNo; } set { if (value != batchNo) { batchNo = value; NotifyPropertyChanged("BatchNo"); } } }

        private Int64 principalID;
        public Int64 PrincipalID { get { return principalID; } set { if (value != principalID) { principalID = value; NotifyPropertyChanged("PrincipalID"); } } }

        private String attendaceID;
        public String AttendanceID { get { return attendaceID; } set { if (value != attendaceID) { attendaceID = value; NotifyPropertyChanged("AttendanceID"); } } }

        private Int64 pmesBatchNumber;
        public Int64 PMESBatchNumber { get { return pmesBatchNumber; } set { if (value != pmesBatchNumber) { pmesBatchNumber = value; NotifyPropertyChanged("PMESBatchNumber"); } } }


        private String dateOfPMES;
        public String DateOfPMES { get { return dateOfPMES; } set { if (value != dateOfPMES) { dateOfPMES = value; NotifyPropertyChanged("DateOfPMES"); } } }

        public String ImageFileName { get; set; }

        public String dateClosed;
        public String DateClosed { get { return dateClosed; } set { if (value != dateClosed) { dateClosed = value; NotifyPropertyChanged("DateClosed"); } } }

        public Int16 reasonid;
        public Int16 ReasonID { get { return reasonid; } set { if (value != reasonid) { reasonid = value; NotifyPropertyChanged("ReasonID"); } } }
        public String ReasonExp { get; set; }

        public Int64 recruitedby;
        public Int64 RecruitedBy { get { return recruitedby; } set { if (value != recruitedby) { recruitedby = value; NotifyPropertyChanged("RecruitedBy"); } } }

        public Int16 dosriid;
        public Int16 DosriID { get { return dosriid; } set { if (value != dosriid) { dosriid = value; NotifyPropertyChanged("DosriID"); } } }

        public Int16 restrictionid;
        public Int16 RestrictionID { get { return restrictionid; } set { if (value != restrictionid) { restrictionid = value; NotifyPropertyChanged("RestrictionID"); } } }

        public String relatedto;
        public String RelatedTo { get { return relatedto; } set { if (value != relatedto) { relatedto = value; NotifyPropertyChanged("RelatedTo"); } } }

        public String dateDeceased;
        public String DateDeceased { get { return dateDeceased; } set { if (value != dateDeceased) { dateDeceased = value; NotifyPropertyChanged("DateDeceased"); } } }

        public String dateTransferred;
        public String DateTransferred { get { return dateTransferred; } set { if (value != dateTransferred) { dateTransferred = value; NotifyPropertyChanged("DateTransferred"); } } }

        private Int64 branchTransferredID;
        public Int64 BranchCode { get { return branchTransferredID; } set { if (value != branchTransferredID) { branchTransferredID = value; NotifyPropertyChanged("BranchCode"); } } }

        private Int64 schoolid;
        public Int64 SchoolID
        {
            get { return schoolid; }
            set
            {
                if (value != schoolid)
                { schoolid = value; NotifyPropertyChanged("SchoolID"); }
            }
        }
        private Int64 sectionid;
        public Int64 SectionID
        {
            get { return sectionid; }
            set
            {
                if (value != sectionid)
                { sectionid = value; NotifyPropertyChanged("SectionID"); }
            }
        }

        public Int64 ClientID { get; set; }
        public Byte ClientChkID { get; set; }
        public Boolean OptionInsertOrEdit { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        //To be refactored for optimization
        public InsertPresent Present { get; set; }
        public InsertHome Home { get; set; }
        public InsertSpouse Spouse { get; set; }
        public InsertParentsMother Mother { get; set; }
        public InsertParentsFather Father { get; set; }
        public InsertEmployer Employer { get; set; }
        public InsertSelfEmployed Self { get; set; }
        public InsertIncome Income { get; set; }

        //ADDED BY: MARTIN BADON 06-06-2018
        public List<InsertClientAssets> ClientAssets { get; set; }
        public List<InsertClientContact> ClientContacts { get; set; }
        public List<InsertClientID>  ClientIDs{ get; set; }
        public List<InsertClientOtherID> ClientOtherIDs { get; set; }
        public List<InsertClientBank> ClientBanks { get; set; }
        public List<InsertClientDependent> ClientDependents { get; set; }
        public List<Notification> ClientNotifications { get; set; }

        //ADDED BY: MARTIN 07-07-2018
        public List<InsertDOSRI> ClientDOSRIs { get; set; }
        //END
    }

    public class InsertPresent
    {
        public Int64 BarangayID { get; set; }
        public string HouseNo { get; set; }
        public string Street { get; set; }
        public int PostalCode { get; set; }
        public int SinceDate { get; set; }
        public Boolean LivingwithParents { get; set; }
        public int Owned { get; set; }
        public string Owner { get; set; }
        public string AddressType { get; set; }
        public string AddressStatus { get; set; }
        public DateTime DateTimeStamp { get; set; }
    }
    public class InsertHome
    {
        public Int64 HomeBarangayID { get; set; }
        public string HomeHouseNo { get; set; }
        public string HomeStreet { get; set; }
        public int HomePostalCode { get; set; }
        public int HomeSinceDate { get; set; }
        public Boolean HomeLivingwithParents { get; set; }
        public int HomeOwned { get; set; }
        public string HomeOwner { get; set; }
        public string HomeAddressType { get; set; }
        public string HomeAddressStatus { get; set; }
    }

    public class InsertSpouse : INotifyPropertyChanged
    {
        public string SpouseLastName { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseMiddleName { get; set; }

        private Int16 spousesuffixID;
        public Int16 SpouseSuffix { get { return spousesuffixID; } set { if (value != spousesuffixID) { spousesuffixID = value; NotifyPropertyChanged("spousesuffixID"); } } }

        //public string SpouseSuffix { get; set; }
        public string SpouseNickname { get; set; }
        public string SpouseMaidenName { get; set; }
        public Int64 SpouseCompanyID { get; set; }
        public string SpouseCompanyName { get; set; } //for Display purposes
        public String SpouseMobileNumber { get; set; }
        public string SpousePhoneNumber { get; set; }
        public string SpouseOfficePhoneNumber { get; set; }
        public string SpouseEmailAddress { get; set; }
        public string SpouseEmailAddress2 { get; set; }
        public String SpouseDateOfBirth { get; set; }
        public String SpouseOccupation { get; set; }



        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }

    public class InsertParentsMother
    {
        public Int64 ParentsID { get; set; }
        public string MotherLastName { get; set; }
        public string MotherFirstName { get; set; }
        public string MotherMiddleName { get; set; }
        public Int64 MotherBarangayID { get; set; }
        public string MotherHouseNo { get; set; }
        public string MotherStreet { get; set; }
        public int MotherPostalCode { get; set; }
        public Int64 MotherSinceYear { get; set; }
        public Byte ParentType { get; set; }

    }
    public class InsertParentsFather
    {
        public string FatherLastName { get; set; }
        public string FatherFirstName { get; set; }
        public string FatherMiddleName { get; set; }
        public Int64 FatherBarangayID { get; set; }
        public string FatherHouseNo { get; set; }
        public string FatherStreet { get; set; }
        public int FatherPostalCode { get; set; }
        public Int64 FatherSinceYear { get; set; }
        public Byte FatherParentType { get; set; }

    }

    public class InsertEmployer
    {
        public Int64 EmpID { get; set; }
        public Int64 EmpCompanyID { get; set; }
        public String EmpCompanyName { get; set; }
        public String EmpFrom { get; set; }
        public String EmpUntil { get; set; }
        public Int64 EmpSectorID { get; set; }
        public Int64 EmpPositionID { get; set; }
        public Int16 EmpStatusID { get; set; }
        public Int64 EmpBarangayID { get; set; }
        public string EmpBuildingNo { get; set; }
        public string EmpHouseNo { get; set; }
        public string EmpStreet { get; set; }
        public string EmpPostalCode { get; set; }
        public Int16 EmpTenureship { get; set; }
        public Boolean EmpType { get; set; }
    }

    public class InsertSelfEmployed
    {
        public Int64 SelfEmpSectorID { get; set; }
        public Int64 SelfEmpSubSectorID { get; set; }
        public string SelfEmpOccupation { get; set; }
        public string SelfEmpBusinessName { get; set; }
        public Int64 SelfEmpLineOfBusiness { get; set; }
        public Int64 SelfEmpBarangayID { get; set; }
        public string SelfEmpHouseNo { get; set; }
        public string SelfEmpStreet { get; set; }
        public String SelfEmpPostalCode { get; set; }
        public Boolean SelfEmpType { get; set; }
    }

    public class InsertIncome
    {
        public decimal? AnnualSalaryIncome { get; set; }
        public Int16 SalaryIncomeType { get; set; }
        public decimal? AnnualBusinessIncome { get; set; }
        public Int16 BusinessIncomeType { get; set; }
        public decimal? SpouseAnnualSalary { get; set; }
        public Int16 SpouseSalaryIncomeType { get; set; }
        public decimal? SpouseBusinessIncome { get; set; }
        public Int16 SpouseBusinessIncomeType { get; set; }
        public decimal? OtherAnnualIncome { get; set; }
        public Int16 OtherIncomeType { get; set; }
    }
    public class InsertClientContact : INotifyPropertyChanged
    {
        public int ContactTypeID { get; set; }
        public String contactno;
        public String ContactNo
        {
            get
            {
                return contactno;
            }
            set
            {
                if (value != contactno)
                {
                    contactno = value;
                    NotifyPropertyChanged("ContactNo");
                }
            }
        }
        public Int64 ClientID { get; set; }
        public Int64 ContactID { get; set; }


        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }

    public class InsertClientOtherID : INotifyPropertyChanged
    {
        public Int16 otheridtypevalue;
        public Int16 OtherIDTypeValue
        {
            get
            {
                return otheridtypevalue;
            }
            set
            {
                if (value != otheridtypevalue)
                {
                    otheridtypevalue = value;
                    NotifyPropertyChanged("OtherIDTypeValue");
                }
            }
        }
        public String otheridnumber;
        public String OtherIDNumber
        {
            get
            {
                return otheridnumber;
            }
            set
            {
                if (value != otheridnumber)
                {
                    otheridnumber = value;
                    NotifyPropertyChanged("OtherIDNumber");
                }
            }
        }


        public String idDateIssued;
        public String IDDateIssued
        {
            get
            {
                return idDateIssued;
            }
            set
            {
                if (value != idDateIssued)
                {
                    idDateIssued = value;
                    NotifyPropertyChanged("IDDateIssued");
                }
            }
        }

        public String idDateExpired;
        public String IDDateExpired
        {
            get
            {
                return idDateExpired;
            }
            set
            {
                if (value != idDateExpired)
                {
                    idDateExpired = value;
                    NotifyPropertyChanged("IDDateExpired");
                }
            }
        }

        public bool OtherIDType { get; set; }
        public Int64 ClientID { get; set; }
        public Int64 OtherID { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }

    public class InsertClientID //: IDataErrorInfo
    {
        public Int16 IDTypeValue { get; set; }
        public string IDNumber { get; set; }
        public bool IDType { get; set; }
        public Int64 ClientID { get; set; }
        public Int64 ID { get; set; }
        //public string Error
        //{
        //    get
        //    {
        //        return this[IDNumber];
        //        //throw new NotImplementedException();
        //    }
        //}

        //public string this[string columnName]
        //{
        //    get
        //    {
        //        string errorMessage = null;
        //        switch (columnName)
        //        {
        //            case "IDNumber":
        //                if (String.IsNullOrWhiteSpace(IDNumber))
        //                {
        //                    errorMessage = "IDNumber is required.";
        //                }
        //                break;
        //        }
        //        return errorMessage;
        //    }
        //}
    }

    public class InsertClientAssets
    {
        public Int16 AssetTypeID { get; set; }
        public string AssetDescription { get; set; }
        public Int64 ClientID { get; set; }
        public Int64 AssetID { get; set; }
    }

    public class InsertClientCompany
    {
        public Int64 EmpID { get; set; }
        public Int64 EmpCompanyID { get; set; }
        public String EmpCompanyName { get; set; }
        public String EmpFrom { get; set; }
        public String EmpUntil { get; set; }
        public Int64 EmpSectorID { get; set; }
        public Int32 EmpPositionID { get; set; }
        public Int16 EmpStatusID { get; set; }
        public Int32 EmpCountryID { get; set; }
        public Int64 EmpProvinceID { get; set; }
        public Int64 EmpMunicipalityID { get; set; }
        public Int64 EmpBarangayID { get; set; }
        public string EmpBuildingNo { get; set; }
        public string EmpHouseNo { get; set; }
        public string EmpStreet { get; set; }
        public string EmpPostalCode { get; set; }
        public Int16 EmpTenureship { get; set; }
        public byte EmpType { get; set; }

        public Int64 SelfEmpSectorID { get; set; }
        public Int64 SelfEmpSubSectorID { get; set; }
        public string SelfEmpOccupation { get; set; }
        public string SelfEmpBusinessName { get; set; }
        public Int64 SelfEmpLineOfBusiness { get; set; }
        public Int32 SelfEmpCountryID { get; set; }
        public Int64 SelfEmpProvinceID { get; set; }
        public Int64 SelfEmpMunicipalityID { get; set; }
        public Int64 SelfEmpBarangayID { get; set; }
        public string SelfEmpHouseNo { get; set; }
        public string SelfEmpStreet { get; set; }
        public String SelfEmpPostalCode { get; set; }
    }

    public class InsertClientDependent
    {
        public Int64 DependentID { get; set; }
        public String DependentName { get; set; }
        public String Birthdate { get; set; }
        public String Relation { get; set; }
        public Int64 ClientID { get; set; }
    }

    public class InsertNotification
    {
        public Int64 ClientID { get; set; }
        public Int64 BranchID { get; set; }
        public Int64 NotificationIDs { get; set; }
        public Int64 NotificationID { get; set; }
        public String NotificationStatus { get; set; }
        public String TR_DATE { get; set; }
        public Int64 UserID { get; set; }
        public String EncodedBy { get; set; }
        public String Remarks { get; set; }
        public Boolean IsPrompt { get; set; }
        public String TextDecoration { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class IncomeType
    {
        public Int16 PaymentModeID { get; set; }
        public String PaymentModeDesc { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class userInfo
    {
        public Int64 ClientID { get; set; }
        public Int64 BranchID { get; set; }
		public Int64 ID { get; set; }
		public string UserName { get; set; }
	}

    class loanUserInfo
    {
        public Int64 ClientID { get; set; }
        public Int64 BranchID { get; set; }
        public Byte SLC { get; set; }
        public Byte SLT { get; set; }
        public String REF_NO { get; set; }
		public String APPLICATION_NO { get; set; }
	}

	
}

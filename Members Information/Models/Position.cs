﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class Position
    {
        public Int64 OccupationID { get; set; }
        public String Occupation { get; set; }
        public Int64 Code { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class Genders
    {
        public Int64 GenderID { get; set; }
        public String Gender { get; set; }
    }
}

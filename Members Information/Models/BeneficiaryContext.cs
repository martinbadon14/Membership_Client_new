﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class BeneficiaryContext : INotifyPropertyChanged
    {
        Boolean _EnableEdit;
        Int64 _ClientID;
        Int64 _BranchID;
        ObservableCollection<Relation> _Relations;
        List<ClientBeneficiaries> _Beneficiaries;

        public Boolean EnableEdit
        {
            get
            {
                return _EnableEdit;
            }
            set
            {
                _EnableEdit = value;
                OnPropertyChanged("EnableEdit");
            }
        }
        public Int64 ClientID
        {
            get
            {
                return _ClientID;
            }
            set
            {
                _ClientID = value;
                OnPropertyChanged("ClientID");
            }
        }
        
        public Int64 BranchID
        {
            get
            {
                return _BranchID;
            }
            set
            {
                _BranchID = value;
                OnPropertyChanged("BranchID");
            }
        }
        public List<ClientBeneficiaries> Beneficiaries
        {
            get { return _Beneficiaries; }
            set
            {
                _Beneficiaries = value;
                OnPropertyChanged("Beneficiaries");
            }
        }

        public ObservableCollection<Relation> Relations
        {
            get { return _Relations; }
            set
            {
                _Relations = value;
                OnPropertyChanged("Relations");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}

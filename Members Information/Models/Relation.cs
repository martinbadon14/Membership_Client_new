﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class Relation
    {
        public Int32 RelationID { get; set; }
        public String RelationDesc { get; set; }
    }
}

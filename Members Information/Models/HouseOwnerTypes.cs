﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class HouseOwnerTypes
    {
        public Byte HouseOwnerTypeID { get; set; }
        public String HouseOwnerType { get; set; }
    }
}

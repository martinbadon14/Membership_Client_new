﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class ClientBeneficiaries
    {
        public String dob;

        public List<Relation> Relations { get; set; }
        public Int64 BeneficiaryID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String MiddleName { get; set; }
        public String DateOfBirth {
            get
            {
                return this.DateFormat(dob);
            }
            set {
                dob = value;
            }

        }
        public Int32 RelationID { get; set; }
        public String Occupation { get; set; }
        public Int64 ClientID { get; set; }
        public Int64 BranchID { get; set; }
        public String datetimeAdded { get; set; }
        public String datetimeModified { get; set; }

        private String DateFormat(String Date)
        {
            if (Date == null)
            {
                Date = "";
            }

            String temp = Regex.Replace(Date, "[^0-9.]", "");
            if (temp.Length >= 8)
            {
                temp = temp.Substring(0, 8);
            }
            else
            {
                int underscore = 8 - temp.Length;

                while (underscore > 0)
                {
                    temp += "_";
                    underscore--;
                }
            }
            temp = temp.Insert(2, "/");
            temp = temp.Insert(5, "/");

            return temp;
        }
    }
}

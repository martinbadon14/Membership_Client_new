﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class ClientTypes
    {
        public Byte ClientTypeID { get; set; }
        public String ClientType { get; set; }
    }
}

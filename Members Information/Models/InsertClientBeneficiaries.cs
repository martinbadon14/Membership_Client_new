﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Members_Information.Models
{
    public class InsertClientBeneficiaries : INotifyPropertyChanged
    {
        public Int64 beneficiaryid;
        public Int64 BeneficiaryID {
            get {
                return beneficiaryid;
            }
            set
            {
                if (value != beneficiaryid)
                {
                    beneficiaryid = value;
                    NotifyPropertyChanged("BeneficiaryID");
                }
            }
        }
        public String firstname;
        public String FirstName {
            get
            {
                return firstname;
            }
            set
            {
                if (value != firstname)
                {
                    firstname = value;
                    NotifyPropertyChanged("FirstName");
                }
            }
        }
        public String lastname;
        public String LastName
        {
            get
            {
                return lastname;
            }
            set
            {
                if (value != lastname)
                {
                    lastname = value;
                    NotifyPropertyChanged("LastName");
                }
            }
        }
        public String middlename;
        public String MiddleName {

            get
            {
                return middlename;
            }
            set
            {
                if (value != middlename)
                {
                    middlename = value;
                    NotifyPropertyChanged("MiddleName");
                }
            }

        }
        //public DateTime DateOfBirth { get; set; }

        public String dateOfBirth;
        public String DateOfBirth
        {
            get
            {
                return dateOfBirth;
            }
            set
            {
                if (value != dateOfBirth)
                {
                    dateOfBirth = value;
                    NotifyPropertyChanged("DateOfBirth");
                }
            }

        }
        public Int32 relation;
        public Int32 RelationID
		{
            get
            {
                return relation;
            }
            set
            {
                if (value != relation)
                {
                    relation = value;
                    NotifyPropertyChanged("RelationID");
                }
            }
        }
        public String occupation;
        public String Occupation {
            get
            {
                return occupation;
            }
            set
            {
                if (value != occupation)
                {
                    occupation = value;
                    NotifyPropertyChanged("Occupation");
                }
            }
        }
        public Int64 clientid;
        public Int64 ClientID {
            get
            {
                return clientid;
            }
            set
            {
                if (value != clientid)
                {
                    clientid = value;
                    NotifyPropertyChanged("ClientID");
                }
            }
        }
        public Int64 branchid;
        public Int64 BranchID {
            get
            {
                return branchid;
            }
            set
            {
                if (value != branchid)
                {
                    branchid = value;
                    NotifyPropertyChanged("BranchID");
                }
            }
        }

		public String datetimeadded;
		public String datetimeAdded
		{
			get
			{
				return datetimeadded;
			}
			set
			{
				if (value != datetimeadded)
				{
					datetimeadded = value;
					NotifyPropertyChanged("datetimeAdded");
				}
			}
		}

		public String datetimemodified;
		public String datetimeModified
		{
			get
			{
				return datetimemodified;
			}
			set
			{
				if (value != datetimemodified)
				{
					datetimemodified = value;
					NotifyPropertyChanged("datetimeModified");
				}
			}
		}
		public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}

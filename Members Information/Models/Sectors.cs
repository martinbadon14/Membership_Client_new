﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class Sectors
    {
        public Int64 SectorID { get; set; }
        public string Sector { get; set; }
    }
}

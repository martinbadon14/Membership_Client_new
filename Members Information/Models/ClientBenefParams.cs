﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class ClientBenefParams
    {
        public Int64 ClientID { get; set; }
        public List<InsertClientBeneficiaries> beneficiaries { get; set; }
    }
}

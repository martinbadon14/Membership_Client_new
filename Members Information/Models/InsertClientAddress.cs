﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    public class InsertClientAddress
    {
        private int boolValue;

        //Present
        public Int64 PMunicipalityID { get; set; }
        public Int32 PCountryID { get; set; }
        public Int64 PProvinceID { get; set; }


        public Int64 PBarangayID { get; set; }
        public string HouseNo { get; set; }
        public string Street { get; set; }
        public int PostalCode { get; set; }
        public int SinceDate { get; set; }
        public Boolean LivingwithParents { get; set; }
        public int Owned { get; set; }
        public string Owner { get; set; }
        public string AddressType { get; set; }
        public Boolean AddressStatus { get; set; }
        public DateTime DateTimeStamp { get; set; }

        //Home
        public Int64 HomeMunicipalityID { get; set; }
        public Int32 HomeCountryID { get; set; }
        public Int64 HomeProvinceID { get; set; }
        public Int64 HomeBarangayID { get; set; }
        public string HomeHouseNo { get; set; }
        public string HomeStreet { get; set; }
        public int HomePostalCode { get; set; }
        public int HomeSinceDate { get; set; }
        public Boolean HomeLivingwithParents { get; set; }
        public int HomeOwned { get; set; }
        public string HomeOwner { get; set; }
        public string HomeAddressType { get; set; }
        public string HomeAddressStatus { get; set; }

        public Int64 ClientID { get; set; }
    }

    public class InsertClientAddressHistory
    {
        public string HouseNo { get; set; }
        public string Street { get; set; }
        public int SinceDate { get; set; }
        public String HouseOwnerType { get; set; }
        public String Owner { get; set; }
        public String Barangay { get; set; }
        public String City { get; set; }
        public String Province { get; set; }
        public String Address { get { return string.Format("{0} {1}, {2}, {3}, {3}", HouseNo, Street, Barangay,City, Province); } }
        public Int64 ClientID { get; set; }
    }

    public class InsertClientSpouse
    {
        //Client Spouse
        public string SpouseLastName { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseMiddleName { get; set; }
        public Int16 SpouseSuffix { get; set; }
        public string SpouseNickname { get; set; }
        public string SpouseMaidenName { get; set; }
        public Int64 SpouseCompanyID { get; set; }
        public String SpouseCompanyName { get; set; }
        public string SpouseMobileNumber { get; set; }
        public string SpousePhoneNumber { get; set; }
        public string SpouseOfficePhoneNumber { get; set; }
        public string SpouseEmailAddress { get; set; }
        public string SpouseEmailAddress2 { get; set; }
        public String SpouseDateOfBirth { get; set; }
        public String SpouseOccupation { get; set; }
        public Int64 ClientID { get; set; }
    }

    //Client Parents
    public class InsertClientParents
    {
        public Int64 ParentsID { get; set; }
        public string MotherLastName { get; set; }
        public string MotherFirstName { get; set; }
        public string MotherMiddleName { get; set; }
        public Int32 MotherCountryID { get; set; }
        public Int64 MotherProvinceID { get; set; }
        public Int64 MotherCityID { get; set; }
        public Int64 MotherBarangayID { get; set; }
        public string MotherHouseNo { get; set; }
        public string MotherStreet { get; set; }
        public int MotherPostalCode { get; set; }
        public Int64 MotherSinceYear { get; set; }
        public Byte ParentType { get; set; }

        public string FatherLastName { get; set; }
        public string FatherFirstName { get; set; }
        public string FatherMiddleName { get; set; }
        public Int32 FatherCountryID { get; set; }
        public Int64 FatherProvinceID { get; set; }
        public Int64 FatherCityID { get; set; }
        public Int64 FatherBarangayID { get; set; }
        public string FatherHouseNo { get; set; }
        public string FatherStreet { get; set; }
        public int FatherPostalCode { get; set; }
        public Int64 FatherSinceYear { get; set; }
        public Byte FatherParentType { get; set; }
    }

    public class InsertClientIncome
    {
        //Client Income
        public decimal? AnnualSalaryIncome { get; set; }
        public Int16 SalaryIncomeType { get; set; }
        public decimal? AnnualBusinessIncome { get; set; }
        public Int16 BusinessIncomeType { get; set; }
        public decimal? SpouseAnnualSalary { get; set; }
        public Int16 SpouseSalaryIncomeType { get; set; }
        public decimal? SpouseBusinessIncome { get; set; }
        public Int16 SpouseBusinessIncomeType { get; set; }
        public decimal? OtherAnnualIncome { get; set; }
        public Int16 OtherIncomeType { get; set; }
    }

    public class InsertClientBank
    {
        public Int64 BankID { get; set; }
		public Int64 BankNameID { get; set; }
		public String BankNumber { get; set; }
        public Int16 BankAcctTypeID { get; set; }
        public Int64 ClientID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models.Client_History
{
    public class ClientHistoryDetails : INotifyPropertyChanged
    {
        String _BranchName;
        public String BranchName
        {
            get { return _BranchName; }
            set
            {
                _BranchName = value;
                OnPropertyChanged("BranchName");
            }
        }

        Int16 _BranchID;
        public Int16 BranchID
        {
            get { return _BranchID; }
            set
            {
                _BranchID = value;
                OnPropertyChanged("BranchID");
            }
        }

        Int64 _PrevClientID;
        public Int64 PrevClientID
        {
            get { return _PrevClientID; }
            set
            {
                _PrevClientID = value;
                OnPropertyChanged("PrevClientID");
            }
        }

        Int64 _CurrentClientID;
        public Int64 CurrentClientID
        {
            get { return _CurrentClientID; }
            set
            {
                _CurrentClientID = value;
                OnPropertyChanged("CurrentClientID");
            }
        }

        String _DateOpened;
        public String DateOpened
        {
            get { return _DateOpened; }
            set
            {
                _DateOpened = value;
                OnPropertyChanged("DateOpened");
            }
        }

        String _DateClosed;
        public String DateClosed
        {
            get { return _DateClosed; }
            set
            {
                _DateClosed = value;
                OnPropertyChanged("DateClosed");
            }
        }

        String _ClientName;
        public String ClientName
        {
            get { return _ClientName; }
            set
            {
                _ClientName = value;
                OnPropertyChanged("ClientName");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}

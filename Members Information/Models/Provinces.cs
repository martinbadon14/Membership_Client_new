﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class Provinces
    {
        public Int64 ProvinceID { get; set; }
        public Int64 CountryID { get; set; }
        public String Province { get; set; }
    }
}

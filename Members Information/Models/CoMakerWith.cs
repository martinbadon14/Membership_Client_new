﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models
{
    class CoMakerWith
    {
        public String ClientID { get; set; }
        public Int64 BranchID { get; set; }
        public Int64 noformatClientID { get; set; }
        public String ClientName { get; set; }
        public String LoanType { get; set; }
        public String AcctNo { get; set; }
        public Decimal Balance { get; set; }
        public String Status { get; set; }
        public String Pastdue { get; set; }
        public byte SLC { get; set; }
        public byte SLT { get; set; }
		public string RowColor { get; set; }
		public string SelectRowColor { get; set; }
	}
}

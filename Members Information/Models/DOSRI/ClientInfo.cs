﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models.DOSRI
{
    public class ClientInfo
    {
        public Int64 BranchID { get; set; }
        public String BranchIDForm { get; set; }
        public Int64 clientId { get; set; }
        public String ClientIDForm { get; set; }
        public Byte clientCheckID { get; set; }
        public String ClientIDFormat { get { return string.Format("{0}-{1}-{2}", BranchIDForm, ClientIDForm, clientCheckID); } }
        public String UserIDFormat { get { return string.Format("{0}-{1}", BranchIDForm, ClientIDForm); } }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public string suffix { get; set; }
        public String Name { get { return string.Format("{0}, {1} {2} {3}", lastName, firstName, middleName, suffix); } }
        public String clientType { get; set; }
        public String status { get; set; }
        public String RowColor { get; set; }

        public String DateClosed { get; set; }
        public String DateOpened { get; set; }
        public Byte AccountStatusID { get; set; }
    }

    public class ClientDosRiParams
    {
        public Int64 BranchID { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public String ClientStatusID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models.DOSRI
{
    public class InsertDOSRI : INotifyPropertyChanged
    {
        Int64 _BranchCode;
        public Int64 BranchCode
        {
            get { return _BranchCode; }
            set
            {
                if (value != _BranchCode)
                {
                    _BranchCode = value; NotifyPropertyChanged("BranchCode");
                }
            }
        }

        Int16 _AcctTypeID;
        public Int16 AcctTypeID
        {
            get { return _AcctTypeID; }
            set
            {
                if (value != _AcctTypeID)
                {
                    _AcctTypeID = value; NotifyPropertyChanged("AcctTypeID");
                }
            }
        }

        String _AcctTypeDesc;
        public String AcctTypeDesc
        {
            get { return _AcctTypeDesc; }
            set
            {
                if (value != _AcctTypeDesc)
                {
                    _AcctTypeDesc = value;
                    NotifyPropertyChanged("AcctTypeDesc");
                }
            }
        }

        Int64 _AcctID;
        public Int64 AcctID
        {
            get { return _AcctID; }
            set
            {
                if (value != _AcctID)
                {
                    _AcctID = value;
                    NotifyPropertyChanged("AcctID");
                }
            }
        }

        String _AcctIDFormat;
        public String AcctIDFormat
        {
            get { return _AcctIDFormat; }
            set
            {
                if (value != _AcctIDFormat)
                {
                    _AcctIDFormat = value;
                    NotifyPropertyChanged("AcctIDFormat");
                }
            }
        }

        String _AcctName;
        public String AcctName
        {
            get { return _AcctName; }
            set
            {
                if (value != _AcctName)
                {
                    _AcctName = value;
                    NotifyPropertyChanged("AcctName");
                }
            }
        }

        Int16 _DosriID;
        public Int16 DosriID
        {
            get { return _DosriID; }
            set
            {
                if (value != _DosriID)
                {
                    _DosriID = value;
                    NotifyPropertyChanged("DosriID");
                }
            }
        }

        String _DosriDesc;
        public String DosriDesc
        {
            get { return _DosriDesc; }
            set
            {
                if (value != _DosriDesc)
                {
                    _DosriDesc = value;
                    NotifyPropertyChanged("DosriDesc");
                }
            }
        }

        Int64 _ClientID;
        public Int64 ClientID
        {
            get { return _ClientID; }
            set
            {
                if (value != _ClientID)
                {
                    _ClientID = value;
                    NotifyPropertyChanged("ClientID");
                }
            }
        }

        Int64 _DOSRIBranchID;
        public Int64 DOSRIBranchID
        {
            get { return _DOSRIBranchID; }
            set
            {
                if (value != _DOSRIBranchID)
                {
                    _DOSRIBranchID = value; NotifyPropertyChanged("DOSRIBranchID");
                }
            }
        }

        Int32 _SeqNo;
        public Int32 SeqNo
        {
            get { return _SeqNo; }
            set
            {
                if (value != _SeqNo)
                {
                    _SeqNo = value;
                    NotifyPropertyChanged("SeqNo");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}

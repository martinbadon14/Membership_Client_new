﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Members_Information.Models.DOSRI
{
    public class DosRiAcctType
    {
        public Int16 AcctTypesID { get; set; }
        public String AcctTypeDesc { get; set; }
    }
}

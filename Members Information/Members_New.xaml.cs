﻿using Members_Information.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace Members_Information
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Members_New : Window
    {
        public MainWindow mw;
        public Members_New(MainWindow mww)
        {
            InitializeComponent();
            this.mw = mww;
            txt_Batchnum.Focus();
        }

        private void btn_ok_Click(object sender, RoutedEventArgs e)
        {

            if (txt_Batchnum.Text!=""||txt_Name.Text!="")
            {
                Members_List newsub = new Members_List(3, this.mw, this);
                newsub.Owner = this;
                newsub.ShowDialog();
                this.Close();
            }else
            {
                MessageBox.Show("Please input character","Warning");
            }
            
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
		private void txt_batchnum_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			if (!Regex.IsMatch(e.Text, @"^[0-9]+$"))
			{
				e.Handled = true;
			}
		}
		private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txt_Batchnum.Text != "" || txt_Name.Text != "")
                {
                    Members_List newsub = new Members_List(3, this.mw, this);
                    newsub.Owner = this;
                    newsub.ShowDialog();
                    e.Handled = true;
					this.Close();
                   
                }
                else
                {
                    MessageBox.Show("Please input some character", "Warning");
                }
            }
            if (e.Key == Key.Escape)
            {
                this.Close();
                e.Handled = true;
            }
        }
    }
}

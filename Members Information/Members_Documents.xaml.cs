﻿using Members_Information.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Members_Information
{
    /// <summary>
    /// Interaction logic for Members_Documents.xaml
    /// </summary>
    public partial class Members_Documents : Window
    {
        OpenFileDialog open;
        List<InsertClientDocuments> InsertClientDocumentsList;

        public Members_Documents()
        {
            InitializeComponent();
            open = new OpenFileDialog();
            InsertClientDocumentsList = new List<InsertClientDocuments>();
            //clientDocuments = new InsertClientDocuments();

            List<InsertClientDocuments> clientDocumentsList = new List<InsertClientDocuments>();
            DG_Documents.ItemsSource = clientDocumentsList;
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            open.Multiselect = true;
            open.InitialDirectory = @"C:\";
            open.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            open.FilterIndex = 1;

            if (open.ShowDialog() == true)
            {
                Stream stream = File.Open(open.FileName, FileMode.OpenOrCreate);

                foreach (var fileName in open.FileNames)
                {
                    var name = System.IO.Path.GetFileNameWithoutExtension(fileName);
                    var type = System.IO.Path.GetExtension(fileName);

                    InsertClientDocumentsList.Add(new InsertClientDocuments()
                    {
                        DocumentName = name,
                        DocumentType = type,
                    });
                }
                DG_Documents.ItemsSource = InsertClientDocumentsList;
            }

        }
    }
}

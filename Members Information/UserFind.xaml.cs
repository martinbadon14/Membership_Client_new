﻿using Members_Information.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Members_Information
{
    /// <summary>
    /// Interaction logic for UserFind.xaml
    /// </summary>
    public partial class UserFind : Window
    {
        MainWindow mww;
        UserSearchDataCon DataCon = new UserSearchDataCon();

        public UserFind(MainWindow mw, Int64 branchID = 0)
        {
            InitializeComponent();
            mww = mw;
            PopulateBranch();
            this.DataCon.BranchID = branchID;
            this.DataCon.UserName = App.CRUXAPI.getUserParameter("ClientName");
            this.DataContext = DataCon;

            if (!String.IsNullOrEmpty(this.DataCon.UserName))
            {
                txt_userName.Focus();
                txt_userName.SelectAll();
            }
            else
            {
                txt_userID.Focus();
            }

        }

        private void PopulateBranch()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("client/allbranches", "");
            if (Response.Status == "SUCCESS")
            {
                List<Branches> listSuffix = new JavaScriptSerializer().Deserialize<List<Branches>>(Response.Content);
                cmb_branchID.ItemsSource = listSuffix;
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            App.CRUXAPI.setUserParameter("ClientName", txt_userName.Text);
        }

        private void txt_userName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (!String.IsNullOrEmpty(this.DataCon.UserName))
                    {
                        DisplaySelectedUsers();
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(this.DataCon.UserName))
                {
                    txt_userName.Focus();
                    txt_userName.SelectAll();
                }
                else
                {
                    txt_userID.Focus();
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DisplaySelectedUsers();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void DisplaySelectedUsers()
        {
            if (txt_userID.Text != "" || txt_userName.Text != "")
            {
                UsersList user = new UsersList(this.mww, this.DataCon.BranchID, this.DataCon.UserID.ToString(), this.DataCon.UserName);
                user.Owner = this.mww;
                this.Close();
                user.ShowDialog();
            }
            else
            {
                MessageBox.Show("Please enter characters.", "Warning");
            }
        }


        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public class UserSearchDataCon : INotifyPropertyChanged
        {
            Int64 _BranchID;
            public Int64 BranchID
            {
                get { return _BranchID; }
                set
                {
                    _BranchID = value;
                    OnPropertyChanged("BranchID");
                }
            }


            Int64 _UserID;
            public Int64 UserID
            {
                get { return _UserID; }
                set
                {
                    _UserID = value;
                    OnPropertyChanged("UserID");
                }
            }

            String _UserName;
            public String UserName
            {
                get { return _UserName; }
                set
                {
                    _UserName = value;
                    OnPropertyChanged("UserName");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }

        }

        private void txt_userID_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (this.DataCon.UserID != 0)
                    {
                        DisplaySelectedUsers();
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }
    }
}
